'use strict';
/*jshint expr: true, indent:4*/

/**
 * Properties and settings of the OAuth2 authorization server
 */
exports.properties = {
    //
    // OAuth2orize endpoints
    //
    username: 'test',
    email: 'test@test.com',
    password: 'test',
    login: '/api/session',
    clientId: 'trustedClient',
    clientSecret: 'ssh-otherpassword',
    token: '/oauth2/token',
    authorization: '/oauth2/authorize',
    userinfo: '/api2/me',
    register: '/api/user',
    clientinfo: '/api2/clientinfo',
    logout: '/logout',
    primus: '/primus/?EIO=2&transport=polling&b64=1',
    untrustedClientId: 'xyz123',
    //
    // CAS endpoints
    //
    redirect: 'http://localhost:9000/callback/', // this represents an endpoint on the client
    casClientId: 'cas456',
    casClientSecret: 'ssh-othersecret',
    casLogin: '/cas/login',
    casValidate: '/cas/validate',
    casServiceValidate: '/cas/serviceValidate',
    casLogout: '/cas/logout',
    casOAuthAuthorization: '/cas/oauth2.0/authorize',
    casOAuthToken: '/cas/oauth2.0/accessToken',
    casOAuthProfile: '/cas/oauth2.0/profile',

    //
    // Users
    // 

    users: {
        simple: {
            provider: 'local',
            displayName: 'Yasmine',
            email: 'yasmine@yappn.com',
            username: 'Yasmine',
            password: 'testuserpass',
            role: 'user',
            gender: 'female',
            lang: 'en',
            birthdate: new Date('1986-02-22')
        },
        admin: {
            provider: 'local',
            displayName: 'ThePrimeMedian',
            email: 'api@yappn.com',
            username: 'jkevinburton',
            password: 'testadminpass',
            role: 'admin',
            gender: 'male',
            lang: 'en',
            birthdate: new Date('1979-12-31')
        }
    }
};