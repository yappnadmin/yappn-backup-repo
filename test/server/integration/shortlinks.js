'use strict';
/*jshint expr: true, indent:4*/

var expect = require('chai').expect,
    helper = require('../common').request;

before(function (done) {
    helper.waitForServerReady(done);
});

/**
 * Integration test for detecting the user's language. 
 * There is no security to this endpoint. Simple, straight to the point.
 */
describe('Shortlink API', function () {
    // set the time ot to be 20 seconds
    this.timeout(20000);


    it('should not let me create a new shortlink because originalUrl is missing.', function(done) {
        helper.createShortlink({
            originalUrl: ''
        }, function(err, response /*, body */) {
            //console.log(body);
            expect(err).to.not.exist;
            expect(response.statusCode).to.equal(400);
            done();
        });
    });

    it('should let me create a new shortlink because originalUrl is available.', function(done) {
        helper.createShortlink({
            originalUrl: 'https://www.vevo.com/watch/john-legend/all-of-me/USSM21302088'
        }, function(err, response, body) {
            expect(err).to.not.exist;
            expect(response.statusCode).to.equal(201);
            expect(body.data.url).to.exist;
            done();
        });
    });

});