'use strict';
/*jshint expr: true, indent: 4*/

var expect = require('chai').expect,
    requestCore = require('../common/request-core');

var models = require(requestCore.rootPath + '/lib/config/models'),
    Discussion = models.model('Discussion'),
    Room = models.model('Room'),
    Post = models.model('Comment');

describe('Yappn Forums - Discussions', function() {
    var discussion;

    this.timeout(20000);

    before(function(done) {

        requestCore.waitForServerReady(function() {
            Discussion.remove().exec(function() {
                Room.remove().exec(function() {
                    Post.remove().exec(function() {
                        requestCore.login({}, function() {
                            done();
                        });
                    });
                });
            });
        });

    });

    it('should begin with no discussions.', function(done) {
        Discussion.find({}, function(err, discussions) {
            expect(discussions.length).to.equal(0);
            done();
        });
    });

    it('should stop me from posting as my post contains too much profanity', function(done) {
        requestCore.request.post(requestCore.serverAddress('/api/forums/discussions'), requestCore.addCsrfHeader({
            json: {
                displayName: 'A new and exciting discussion',
                content: 'Holy fuck that cunt.',
                lang: 'en'
            }
        }), function (err, resp) {
            expect(resp.statusCode).to.equal(417);
            done();
        });
    });

    it('should allow me to create a new discussion', function(done) {
        requestCore.request.post(requestCore.serverAddress('/api/forums/discussions'), requestCore.addCsrfHeader({
            json: {
                displayName: 'A new and exciting discussion',
                content: 'Holy geez louise!',
                lang: 'en'
            }
        }), function (err, resp, body) {
            expect(resp.statusCode).to.equal(201);
            expect(body.message).to.equal('addons.forums.discussions.created');
            discussion = body.data.discussion;
            done();
        });
    });

    it('should allow me to create a new discussion with 2 tags', function(done) {
        requestCore.request.post(requestCore.serverAddress('/api/forums/discussions'), requestCore.addCsrfHeader({
            json: {
                displayName: 'Music is in everything',
                content: 'Good time jamaican music and soul.',
                lang: 'en',
                tags: ['bob','marley']
            }
        }), function (err, resp, body) {
            expect(resp.statusCode).to.equal(201);
            expect(body.message).to.equal('addons.forums.discussions.created');
            discussion = body.data.discussion;
            done();
        });
    });

    it('should allow me to create a new discussion with 1 more tag', function(done) {
        requestCore.request.post(requestCore.serverAddress('/api/forums/discussions'), requestCore.addCsrfHeader({
            json: {
                displayName: 'No woman no cry.',
                content: 'Awesome music by Bob Marley.',
                lang: 'en',
                tags: ['bob']
            }
        }), function (err, resp, body) {
            expect(resp.statusCode).to.equal(201);
            expect(body.message).to.equal('addons.forums.discussions.created');
            done();
        });
    });

    it('should not let me post a comment because of my bad mouth!', function(done) {
        requestCore.request.post(requestCore.serverAddress('/api/forums/comments'), requestCore.addCsrfHeader({
            json: {
                content: 'I really like this fucking shit. Like a dick.',
                lang: 'en',
                reference: 'discussions',
                referenceTo: discussion._id
            }
        }), function (err, resp) {
            expect(resp.statusCode).to.equal(417);
            done();
        });
    });

    it('should let me post a comment', function(done) {
        requestCore.request.post(requestCore.serverAddress('/api/forums/comments'), requestCore.addCsrfHeader({
            json: {
                content: 'I really like this stuff. Like a boss.',
                lang: 'en',
                reference: 'discussions',
                referenceTo: discussion._id
            }
        }), function (err, resp, body) {
            //console.log(body);
            expect(resp.statusCode).to.equal(201);
            expect(body.message).to.equal('addons.forums.comment.created');
            done();
        });
    });

});