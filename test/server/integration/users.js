'use strict';
/*jshint expr: true, indent:4*/

var expect = require('chai').expect,
    helper = require('../common').request,
    properties = require('../common').properties,
    models = require('../../../lib/config/models'),
    User = models.model('User');

before(function(done) {
    helper.waitForServerReady(done);
});

/**
 * Integration tests for users accounts and varius endopint abilities.
 */

describe('Users API', function () {
    this.timeout(20000);
    
    describe('Registration', function() {

        it('should fail when registering because I did not include a valid email', function (done) {
            helper.register({
                displayName: properties.users.simple.displayName,
                username: properties.users.simple.username,
                email: '',
                password: properties.users.simple.password,
                lang: properties.users.simple.lang,
                birthdate: properties.users.simple.birthdate,
                gender: properties.users.simple.gender
            }, function(error, response) {
                expect(response.statusCode).to.equal(500);
                done();
            });
        });

        it('should succeed when registering', function (done) {
            helper.register({
                displayName: properties.users.simple.displayName,
                username: properties.users.simple.username,
                email: properties.users.simple.email,
                password: properties.users.simple.password,
                lang: properties.users.simple.lang,
                birthdate: properties.users.simple.birthdate,
                gender: properties.users.simple.gender
            }, function(error, response) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('should exist in the database and be an instance of User model', function (done) {
            User.findOne({ username: properties.users.simple.username }, function(err, user) {
                expect(err).to.not.exist;
                expect(user).to.exist;
                expect(user).to.be.an.instanceof(User);
                done();
            });
        });

    });

    describe('Connections', function () {

        it('should get users who @'+properties.users.simple.username+' is following', function(done) {
            helper.request.get(helper.serverAddress('/api/user/'+properties.users.simple.username+'/following'), function(error,response,body) {
                expect(error).to.not.exist;
                body = JSON.parse(body);
                expect(response.statusCode).to.equal(200);
                expect(body.data.following).to.be.an.instanceof(Array);
                done();
            });
        });

        it('should get users who follow @'+properties.users.simple.username, function(done) {
            helper.request.get(helper.serverAddress('/api/user/'+properties.users.simple.username+'/followers'), function(error,response,body) {
                expect(error).to.not.exist;
                body = JSON.parse(body);
                expect(response.statusCode).to.equal(200);
                expect(body.data.followers).to.be.an.instanceof(Array);
                done();
            });
        });

        it('should tell me the connection status between @'+properties.users.simple.username + ' and user logged-in', function (done) {
            helper.login(
                function (/*error, response, body*/) {
                    helper.request.get(helper.serverAddress('/api/user/'+properties.users.simple.username+'/connection'), function(error, response, body) {
                        expect(error).to.not.exist;
                        body = JSON.parse(body);
                        expect(response.statusCode).to.equal(200);
                        expect(body.data.connection).to.equal('no connection');
                        done();
                    });
                }
            );
        });

        it('should let me follow @'+properties.users.simple.username, function(done) {
            helper.follow(properties.users.simple.username, function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(201);
                done();
            });
        });

        it('should let me stop following @'+properties.users.simple.username, function(done) {
            helper.unfollow(properties.users.simple.username, function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

    });

    describe('Reactions', function () {

        it('should let me like @'+properties.users.simple.username, function(done) {
            helper.react('user', properties.users.simple.username, 'like', function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(201);
                done();
            });
        });
                
        it('should let me remove my like for @'+properties.users.simple.username, function(done) {
            helper.react('user', properties.users.simple.username, 'like', function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('should let me laugh @'+properties.users.simple.username, function(done) {
            helper.react('user', properties.users.simple.username, 'lol', function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(201);
                done();
            });
        });
                
        it('should let me remove my laugh for @'+properties.users.simple.username, function(done) {
            helper.react('user', properties.users.simple.username, 'lol', function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('should let me flag @'+properties.users.simple.username, function(done) {
            helper.react('user', properties.users.simple.username, 'flag', function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(201);
                done();
            });
        });
                
        it('should let me remove my flag for @'+properties.users.simple.username, function(done) {
            helper.react('user', properties.users.simple.username, 'flag', function(error, response /*, body */) {
                expect(error).to.not.exist;
                expect(response.statusCode).to.equal(200);
                done();
            });
        });


    });

    describe('Updating', function () {

        it('should NOT let me update someone elses name, I am just a regular user.', function (done) {
            helper.updateUser(properties.users.simple.username, {
                displayName: 'Bob Saget'
            }, function(error, response /*, body */) {
                expect(response.statusCode).to.equal(403);
                done();
            });
        });

        it('should let me update my own name.', function (done) {
            helper.updateUser(properties.username, {
                displayName: 'Bob Saget'
            }, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                expect(body.data.user.displayName).to.equal('Bob Saget');
                done();
            });
        });

        it('should NOT let me change my role to admin', function (done) {
            helper.updateUser(properties.username, {
                role: 'admin'
            }, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                expect(body.data.user.role).to.equal('user');
                done();
            });
        });

    });
    
    
});