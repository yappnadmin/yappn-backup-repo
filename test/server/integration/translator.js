'use strict';
/*jshint expr: true, indent:4*/

var expect = require('chai').expect,
    helper = require('../common').request;

before(function (done) {
    helper.waitForServerReady(done);
});

/**
 * Integration test for detecting the user's language. 
 * There is no security to this endpoint. Simple, straight to the point.
 */
describe('Translator API', function () {
    // set the time ot to be 20 seconds
    this.timeout(20000);

    it('should be able to detect users language if nothing else is sent', function(done) {
        helper.getUserLanguage(function(err, response, body) {
            expect(err).to.not.exist;
            body = JSON.parse(body);
            expect(body.data.lang).to.exist;
            done();
        });
    });

    it('should be able to override the detection by sending our own lang param of ?lang=fr', function(done) {
        helper.overrideUserLanguage(function(err, response, body) {
            expect(err).to.not.exist;
            body = JSON.parse(body);
            expect(body.data.lang).to.exist;
            expect(body.data.lang).to.equal('fr');
            done();
        });
    });

    it('should be able to use API as translation service like Ortsbo WebTranslator', function(done) {
        helper.request(helper.serverAddress('/services/translator/translate?tag=testinghello&fromlang=en&lang=fr&text=hello%20sir'), function(err, response, body) {
            expect(err).to.not.exist;
            body = JSON.parse(body);
            expect(body.data.translatedText).to.contain('bonjour monsieur');
            done();
        });
    });

});