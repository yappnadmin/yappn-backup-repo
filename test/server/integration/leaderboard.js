'use strict';
/*jshint expr: true, indent:4*/

var expect = require('chai').expect,
    helper = require('../common').request,
    properties = require('../common').properties;

before(function (done) {
    helper.waitForServerReady(done);
});

describe('Leaderboard API', function () {
    // set the time ot to be 20 seconds
    this.timeout(20000);

    it('should be able to get a list of users on the leaderboard', function(done) {
        helper.request(helper.serverAddress('/api/ranking'), function(err, response, body) {
            expect(err).to.not.exist;
            body = JSON.parse(body);
            expect(body.data.leaderboard).to.exist;
            done();
        });
    });

    it('should be able to get a users position on the leaderboard', function(done) {
        helper.request(helper.serverAddress('/api/ranking/' + properties.username), function(err, response, body) {
            expect(err).to.not.exist;
            body = JSON.parse(body);
            expect(body.data.ranking).to.exist;
            done();
        });
    });

});