'use strict';

var passport = require('passport'),
    oauth2 = require('./config/oauth2'),
    config = require('./config/config');

var fs = require('fs'),
    path = require('path');

var index = require('./controllers'),
    users = require('./controllers/users'),
    session = require('./controllers/session'),
    tokens = require('./controllers/tokens'),
    clients = require('./controllers/clients'),
    translations = require('./controllers/translations'),
    leaderboard = require('./controllers/leaderboard'),
    blacklist = require('./controllers/blacklist'),
    history = require('./controllers/history'),
    avatars = require('./controllers/avatars'),
    invitations = require('./controllers/invitations'),
    conversations = require('./controllers/conversations'),
    bagdes = require('./controllers/badges'),
    comments = require('./controllers/comments'),
    categories = require('./controllers/categories'),
    tags = require('./controllers/tags'),
    rooms = require('./controllers/rooms'),
    discussions = require('./controllers/discussions'),
    shortlinks = require('./controllers/shortlinks'),
    errors = require('./controllers/errors'),
    integrated = require('./controllers/integrated');

var securityPolicy = require('./config/security-policy');

// Application Routes
module.exports = function( app ) {

    app.use(blacklist.middleware); // this is a placeholder to be used at a later time

    // translations
    app.get('/services/translator/detect', securityPolicy.enforce('anonUserApi'), translations.detect);
    app.get('/services/translator/translate', securityPolicy.enforce('anonUserApi'), translations.translate);

    // leaderboard -- tested
    app.get('/api/ranking', securityPolicy.enforce('anonUserApi'), leaderboard.ranking);
    app.get('/api/ranking/:lbuser', securityPolicy.enforce('anonUserApi'), leaderboard.byUsername);

    // User & Session Routes -- tested
    app.options('/api/user', securityPolicy.enforce('anonUserApi'));
    app.post('/api/user', securityPolicy.enforce('anonUserApi'), users.create);
    app.get('/api/user/search', securityPolicy.enforce('anonUserApi'), users.search);
    app.get('/api/user/me', securityPolicy.enforce('knownUserApi'), users.me);
    app.get('/api/user/:user', securityPolicy.enforce('anonUserApi'), users.show);
    app.put('/api/user/:user', securityPolicy.enforce('knownUserApi'), users.update);
    app.del('/api/user/:user', securityPolicy.enforce('knownUserApi'), users.destroy);

    app.post('/api/user/:user/like', securityPolicy.enforce('knownUserApi'), users.like);
    app.post('/api/user/:user/lol', securityPolicy.enforce('knownUserApi'), users.lol);
    app.post('/api/user/:user/flag', securityPolicy.enforce('knownUserApi'), users.flag);

    app.post('/api/user/:user/follow', securityPolicy.enforce('knownUserApi'), users.follow);
    app.post('/api/user/:user/unfollow', securityPolicy.enforce('knownUserApi'), users.unfollow);
    app.get('/api/user/:user/connection', securityPolicy.enforce('anonUserApi'), users.connection);
    app.get('/api/user/:user/followers', securityPolicy.enforce('anonUserApi'), users.followers);
    app.get('/api/user/:user/following', securityPolicy.enforce('anonUserApi'), users.following);



    // admin methods for users
    app.get('/api/user/:user/ban', securityPolicy.enforce('knownUserApi'), users.ban);
    app.get('/api/user/:user/unban', securityPolicy.enforce('knownUserApi'), users.unban);

    // social
    app.post('/api/social/:social/authenticate', users.socialAuth);

    // reset and activate
    app.get('/api/activate', users.activate);
    app.post('/api/reset', users.reset);

    // user history -- tested
    app.get('/api/history/:user', securityPolicy.enforce('anonUserApi'), history.user);

    app.param('user', users.user);

    // blacklist api
    app.options('/api/blacklist', securityPolicy.enforce('knownUserApi'));
    app.all('/api/blacklist', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }));
    app.get('/api/blacklist', blacklist.list);
    app.post('/api/blacklist', blacklist.create);
    app.del('/api/blacklist/:blacklist', blacklist.destroy);

    // Session Routes
    app.post('/api/session', securityPolicy.enforce('loginUserPageApi'), session.login);
    app.del('/api/session', securityPolicy.enforce('anonUserPageApi'), session.logout);

    app.options('/api2/clientinfo', securityPolicy.enforce('knownUserApi'));
    app.get('/api2/clientinfo',  securityPolicy.enforce('knownUserApi'), clients.info);
    
    // Me Routes
    app.options('/api/me', securityPolicy.enforce('knownUserApi'));
    app.get('/api/me', securityPolicy.enforce('knownUserApi'), users.me);

    // Me Routes
    app.options('/api2/me', securityPolicy.enforce('knownUserApi'));
    app.get('/api2/me', securityPolicy.enforce('knownUserApi'), users.me);


    app.put('/api/me/avatar', securityPolicy.enforce('knownUserApi'), users.avatar);
    app.put('/api/me/password', securityPolicy.enforce('knownUserApi'), users.changePassword);

    //
    // OAuth2orize Server routes
    //

    app.get('/oauth2/authorize', securityPolicy.enforce('knownUserPage'), oauth2.authorization);
    app.post('/oauth2/authorize/decision', securityPolicy.enforce('knownUserPage'), oauth2.decision);
    app.options('/oauth2/token', securityPolicy.enforce('loginUserApi'));
    app.post('/oauth2/token', securityPolicy.enforce('loginUserApi'), oauth2.token);

    // Mimicking google's token info endpoint from
    // https://developers.google.com/accounts/docs/OAuth2UserAgent#validatetoken
    app.options('/oauth2/tokeninfo', securityPolicy.enforce('anonUserApi'));
    app.get('/oauth2/tokeninfo', securityPolicy.enforce('anonUserApi'), tokens.info);

    // CAS OAuth emulation points
    app.get('/cas/oauth2.0/authorize', securityPolicy.enforce('knownUserPage'), oauth2.casAuthorization);
    app.options('/cas/oauth2.0/accessToken', securityPolicy.enforce('loginUserApi'));
    app.get('/cas/oauth2.0/accessToken', securityPolicy.enforce('loginUserApi'), oauth2.formToken);
    app.options('/cas/oauth2.0/profile', securityPolicy.enforce('knownUserApi'));
    app.get('/cas/oauth2.0/profile', securityPolicy.enforce('knownUserApi'), oauth2.casProfile);

    // CAS Protocol emulation points
    app.get('/cas/login', securityPolicy.enforce('knownUserPageApi'), oauth2.casLogin);
    app.get('/cas/validate', securityPolicy.enforce('loginUserApi'), oauth2.casValidate);
    app.get('/cas/serviceValidate', securityPolicy.enforce('loginUserApi'), oauth2.casServiceValidate);
    app.get('/cas/logout', securityPolicy.enforce('anonUserPage'), session.logoutPage); // no return url support




    // Single pages. Note: OAuth2 Server also has some single pages
    
    app.get('/logout', securityPolicy.enforce('anonUserPage'), session.logoutPage); // no return url support

    // Avatar
    app.get('/avatar/:username', securityPolicy.enforce('anonUserApi'), avatars.getByUsersname);
    app.get('/api/avatars', securityPolicy.enforce('anonUserApi'), avatars.getPackLists);
    app.get('/api/avatars/:pack', securityPolicy.enforce('anonUserApi'), avatars.getAvatarsByPackId);
    app.post('/api/avatars', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), avatars.doCreatePack);
    app.put('/api/avatars/:pack', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), avatars.doUpdatePack);
    app.put('/api/avatars/:pack/launch', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), avatars.doMakePackAvailable);
    app.del('/api/avatars/:pack', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }),avatars.doRemovePack);
    app.post('/api/avatars/:pack', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), avatars.doPushAvatarToPack);
    app.param('pack', avatars.getPackByParam);

    // Conversations
    app.get('/api/conversation/:conversation', securityPolicy.enforce('knownUserApi'), conversations.getConversation);
    app.get('/api/conversations', securityPolicy.enforce('knownUserApi'), conversations.getConversations);
    app.post('/api/conversations', securityPolicy.enforce('knownUserApi'), conversations.createConversation);
    app.del('/api/conversation/:conversation', securityPolicy.enforce('knownUserApi'), conversations.destroyConversation);
    app.post('/api/conversation/:conversation/comment', securityPolicy.enforce('knownUserApi'), conversations.createPost);
    app.put('/api/conversation/:conversation/comment/:commentId', securityPolicy.enforce('knownUserApi'), conversations.updateComment);
    app.del('/api/conversation/:conversation/comment/:commentId', securityPolicy.enforce('knownUserApi'), conversations.destroyComment);
    app.param('conversation', conversations.conversation);

    // Invitations
    app.get('/api/invitations/generate/:numcodes/:percode', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), invitations.generate);

    // Badges
    app.get('/api/trophyroom/:user', securityPolicy.enforce('anonUserApi'), bagdes.trophyroom);

    // Comments
    app.options('/api/forums/comment/*', securityPolicy.enforce('anonUserApi'));
    app.options('/api/forums/comments', securityPolicy.enforce('anonUserApi'));

    app.get('/api/forums/comments/by/:user', securityPolicy.enforce('anonUserApi'), comments.byUser);
    app.get('/api/forums/comments/:referenceTo', securityPolicy.enforce('anonUserApi'), comments.list);
    app.post('/api/forums/comments', securityPolicy.enforce('knownUserApi'), comments.create);
    app.get('/api/forums/comment/:comment', securityPolicy.enforce('anonUserApi'), comments.show);
    app.put('/api/forums/comment/:comment', securityPolicy.enforce('knownUserApi'), comments.update);
    app.del('/api/forums/comment/:comment', securityPolicy.enforce('knownUserApi'), comments.destroy);
    app.post('/api/forums/comment/:comment/like', securityPolicy.enforce('knownUserApi'), comments.like);
    app.post('/api/forums/comment/:comment/lol', securityPolicy.enforce('knownUserApi'), comments.like);
    app.post('/api/forums/comment/:comment/flag', securityPolicy.enforce('knownUserApi'), comments.like);
    app.param('comment', comments.comment);

    // Catgegories
    app.get('/api/forums/categories', securityPolicy.enforce('anonUserApi'), categories.list);
    app.post('/api/forums/categories', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), categories.create);
    app.get('/api/forums/category/:category', securityPolicy.enforce('anonUserApi'), categories.show);
    app.put('/api/forums/category/:category', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), categories.update);
    app.del('/api/forums/category/:category', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), categories.destroy);
    app.param('category', categories.category);

    // Tags
    app.get('/api/forums/tags', securityPolicy.enforce('anonUserApi'), tags.all);
    app.get('/api/forums/tags/:user', securityPolicy.enforce('anonUserApi'), tags.user);
    app.get('/api/forums/tag/:tag', securityPolicy.enforce('anonUserApi'), tags.discussion);

    // Rooms
    app.get('/api/forums/rooms', securityPolicy.enforce('anonUserApi'), rooms.doGetRooms); // 1.0
    app.get('/api/forums/rooms/by/:user', securityPolicy.enforce('anonUserApi'), rooms.getRoomsByUser); // 1.0
    app.get('/api/forums/rooms/supporting', securityPolicy.enforce('knownUserApi'), rooms.doGetMySupporting); // 1.0
    app.get('/api/forums/rooms/suggested', securityPolicy.enforce('anonUserApi'), rooms.doGetSuggested); // 1.0
    app.get('/api/forums/rooms/mine', securityPolicy.enforce('knownUserApi'), rooms.doGetMyRooms); // 1.0
    app.get('/api/forums/rooms/mine/suggested', securityPolicy.enforce('knownUserApi'), rooms.doGetMySuggested); // 1.0
    app.get('/api/forums/rooms/lead', securityPolicy.enforce('anonUserApi'), rooms.doGetLeadRooms); // 1.0
    app.get('/api/forums/rooms/lead/:lead', securityPolicy.enforce('anonUserApi'), rooms.doGetRoomsByLead); // 1.0
    app.post('/api/forums/rooms', securityPolicy.enforce('knownUserApi'), rooms.tryAndCreateRoom); // 1.0
    app.get('/api/forums/room/:room', securityPolicy.enforce('anonUserApi'), rooms.doGetRoom); // 1.0
    app.put('/api/forums/room/:room', securityPolicy.enforce('knownUserApi'), rooms.tryAndUpdateRoom); // 1.0
    app.del('/api/forums/room/:room', securityPolicy.enforce('knownUserApi'), rooms.tryAndDeleteRoom); // 1.0
    app.put('/api/forums/room/:room/endorse', securityPolicy.enforce('knownUserApi'), rooms.tryAndEndorseRoom); // 1.0
    app.put('/api/forums/room/:room/private', securityPolicy.enforce('knownUserApi'), rooms.tryAndMakeRoomPrivate); // 1.0
    app.get('/api/forums/room/:room/forced', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), rooms.doTipRoom); // 1.0
    app.put('/api/forums/room/:room/moderator/:user', securityPolicy.enforce('knownUserApi'), rooms.tryAndMakeUserModerator); // 1.0
    app.del('/api/forums/room/:room/moderator/:user', securityPolicy.enforce('knownUserApi'), rooms.tryAndRemoveModerator); // 1.0
    app.post('/api/forums/room/:room/challenge', securityPolicy.enforce('knownUserApi'), rooms.tryAndChallengeRoom); // 1.0
    app.post('/api/forums/room/:room/like', securityPolicy.enforce('knownUserApi'), rooms.tryAndLikeRoom); // 1.0
    app.post('/api/forums/room/:room/lol', securityPolicy.enforce('knownUserApi'), rooms.tryAndLaughRoom); // 1.0
    app.post('/api/forums/room/:room/flag', securityPolicy.enforce('knownUserApi'), rooms.tryAndFlagRoom); // 1.0

    app.post('/api/forums/room/:room/subscribe', securityPolicy.enforce('knownUserApi'), rooms.doSubscribe); // 1.1.3
    app.del('/api/forums/room/:room/subscribe', securityPolicy.enforce('knownUserApi'), rooms.doUnSubscribe); // 1.1.3

    app.param('room', rooms.getRoom);
    app.param('lead', rooms.getLead);

    // Discussions
    app.get('/api/forums/discussion/:discussion', securityPolicy.enforce('anonUserApi'), discussions.doGetDiscussion); // 1.0
    // app.get('/api/forums/discussions', securityPolicy.enforce('anonUserApi'), DiscussionController.doGetDiscussions); // 1.0
    app.get('/api/forums/discussions/feed', securityPolicy.enforce('knownUserApi'), discussions.doGetFeed); // 1.2.3
    app.get('/api/forums/discussions/recent', securityPolicy.enforce('anonUserApi'), discussions.doGetRecent); // 1.1.3
    app.get('/api/forums/discussions/popular', securityPolicy.enforce('anonUserApi'), discussions.doGetPopular); // 1.1.3
    app.get('/api/forums/discussions/trending', securityPolicy.enforce('anonUserApi'), discussions.doGetTrending); // 1.1.3
    app.get('/api/forums/discussions/by/:user', securityPolicy.enforce('anonUserApi'), discussions.getDiscussionsByUser); // 1.0
    app.get('/api/forums/discussions/lead/:lead', securityPolicy.enforce('anonUserApi'), discussions.doGetDiscussionsByLead); // 1.0
    app.get('/api/forums/discussions/:room', securityPolicy.enforce('anonUserApi'), discussions.doGetDiscussionsByCategory); // 1.0
    app.post('/api/forums/discussions', securityPolicy.enforce('knownUserApi'), discussions.doCreateDiscussion); // 1.0
    app.put('/api/forums/discussion/:discussion', securityPolicy.enforce('knownUserApi'), discussions.tryAndUpdateDiscussion); // 1.0
    app.post('/api/forums/discussion/:discussion/like', securityPolicy.enforce('knownUserApi'), discussions.tryAndLikeDiscussion); // 1.0
    app.post('/api/forums/discussion/:discussion/lol', securityPolicy.enforce('knownUserApi'), discussions.tryAndLaughDiscussion); // 1.0
    app.post('/api/forums/discussion/:discussion/flag', securityPolicy.enforce('knownUserApi'), discussions.tryAndFlagDiscussion); // 1.0
    app.put('/api/forums/discussion/:discussion/move/:room', securityPolicy.enforce({ policy: 'knownUserApi', role: 'admin' }), discussions.tryAndMoveDiscussion); // 1.0
    app.del('/api/forums/discussion/:discussion', securityPolicy.enforce('knownUserApi'), discussions.tryAndDeleteDiscussion); // 1.0
    app.param('discussion', discussions.getDiscussion);

    app.get('/discussion/:discussion', securityPolicy.enforce('anonUserApi'), discussions.metaShare); // 1.2.6


    // Integrated Solution
    app.get('/integrated/:room/:discussion/:theme/:lang', securityPolicy.enforce('anonIntegratedIframe'), securityPolicy.setUserCookie, integrated.chat);
    app.options('/integrated/:room', securityPolicy.enforce('anonIntegratedPage'));
    app.post('/integrated/:room', securityPolicy.enforce('anonIntegratedPage'), integrated.init);


    // Third-party
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: ['email', 'user_about_me'],
        failureRedirect: '/login'
    }));

    app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        successReturnToOrRedirect: '/',
        failureRedirect: '/login'
    }));

    app.get('/auth/twitter', passport.authenticate('twitter', {
        failureRedirect: '/login'
    }));

    app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        successReturnToOrRedirect: '/',
        failureRedirect: '/login'
    }));

    // Tests
    app.get('/test/security-policy/limiter', securityPolicy.enforce({policy:'anonUserPage', authLimit: 'byAnyoneTest' }), function testRoute(req, res) { res.send({ok:true}); });

    // URL Shortener Service SET
    app.post('/api/shorten', securityPolicy.enforce('anonUserApi'), shortlinks.tryAndCreateLink);


    // dynamic addons
    var addonsPath = path.join(__dirname, './addons');
    fs.readdirSync(addonsPath).forEach(function (folder) {
        if (folder.indexOf('.DS_Store') !== -1) { return; }
        require(addonsPath + '/' + folder + '/index.js')(app, securityPolicy, config, passport);
    });



    // Partial Views for Angular UI
    app.get('/partials/*', index.partials); // open route to essentially static resources

    // Fallback on all API requests
    app.get('/services/*', function(req, res) {
        res.send(404);
    });

    app.get('/api/*', function(req, res) {
        res.send(404);
    });

    app.get('/api2/*', function(req, res) {
        res.send(404);
    });
  
    // UI Error Record, so we can view later.
    app.post('/sudo/errors', errors.record);

    // Shortlink Servicer GET
    app.get('/:shortId', securityPolicy.enforce('anonUserPage'), shortlinks.tryAndFindShortlink); // shortlinks!
    app.get('/:shortname', securityPolicy.enforce('anonUserPage'), users.redirectShortName); // allows for: http://yap.pn/bandit

    // Everything else, goes to the homepage and Angular UI will handle the routing.
    app.get('/*', securityPolicy.enforce('anonUserPage'), securityPolicy.setUserCookie, index.index); // open route

};