'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    configExpose = require('../config/config-expose'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    slug = require('slug'),
    url = require('url');

exports.chat = function (req, res, next) {
    if (req.room.id !== req.discussion.room.id) { return res.status(500).send('Invalid configuration.  See Integration documentation.'); }
        
    configExpose.targetLinks = 'pop';
    configExpose.rootUrl = config.rootUrl;
    configExpose.account = req.room;
    configExpose.discussion = { id: req.discussion.id, _id: req.discussion._id, displayName: req.discussion.displayName, content: req.discussion.content, lang: req.discussion.lang, room: { moderators: req.room.moderators } };

    return res.render('integrated', { yappnConfig: JSON.stringify(configExpose), meta: res.metadata });
};

exports.init = [
    function (req, res, next) {
        User.findOne({ username_idx: 'yasmine' }, function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('No default user'));
            req.profile = user;
            next();
        });
    },
    function (req, res, next) {
        // need to check the domain.
        var incomingReferer = req.get('referer');
        var referer = url.parse(incomingReferer);
        var room = req.room;
        if (referer.hostname !== room.domain) {
            return res.status(500).send('Yappn Integrated Error:\nCause: Invalid domain referer.');
        }
        next();
    },
    function (req, res, next) {

        var title = req.param('title', null);
        var content = req.param('description', null);
        var lang = req.param('lang', 'en');
        var width = req.param('width', 500);
        var height = req.param('height', 500);
        var theme = req.param('theme', 'default');
        var url = req.param('url', req.get('referer'));

        if (title) {
            if (!content) return next(new Error('Missing parameter: content'));

            var indexable = slug(title).toLowerCase();
            Discussion.findOne({ urlCode: indexable }, function(err, doc) {
                if (err) return next(err);
                if (!doc) {
                    doc = new Discussion();
                    doc.displayName = title;
                    doc.content = content;
                    doc.lang = lang;
                    doc.author = req.profile;
                    doc.displayMode = 'integrated';
                    doc.room = req.room;
                    doc.url = url;
                    doc.save(function(err, doc) {
                        if (err) return next(err);
                        req.discussion = doc;
                        return next();
                    });
                } else {
                    if (doc.room.toString() !== req.room.id) { return res.status(500).send('Invalid configuration.  See Integration documentation.'); }
                    req.discussion = doc;
                    return next();
                }
            });
        } else {
            return res.status(500).send('Yappn Integrated not installed properly.  See Integration documentation.');
        }
    },
    function (req, res) {
        res.set('Content-Type', 'text/html');
        res.send('<iframe src="'+config.rootUrl+'/integrated/'+req.room.id+'/'+req.discussion.id+'/'+req.param('theme', 'default')+'/'+req.param('lang', 'en')+'" width="'+req.param('width', 500)+'" height="'+req.param('height', 500)+'" frameborder="0" seamless="seamless" style="border:none; overflow:hidden; width:'+req.param('width', 500)+'px; height:'+req.param('height', 500)+'px;"></iframe>');
    }
];