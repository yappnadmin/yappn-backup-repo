'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    History = models.model('History');

exports.user = function(req, res, next) {
    var page = req.param('page', 1),
        limit = req.param('limit', 50);

    History
        .find({ user: req.profile.id })
        .sort('-createdAt')
        .populate('discussion')
        .populate('comment')
        .populate('room')
        .populate('person', config.settings.default_public_user_fields)
        .paginate(page, limit, function(err, histories, total) {
            if (err) return res.respond(400, err);
            return res.respond(200, {
                page: page,
                pages: Math.ceil(total/limit),
                total:total,
                history: histories
            });
        });
};