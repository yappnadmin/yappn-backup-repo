'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    ee = require('../config/pubsub').ee,
    translate = require('../helpers/translate'),
    log = require('../helpers/logger'),
    User = models.model('User'),
    Notification = models.model('Notification'),
    Room = models.model('Room'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Threshold = models.model('Threshold'),
    Profanity = models.model('Profanity');

var twitter = require('node-twitter-text');

exports.list = function (req, res, next) {
    var limit = req.param('limit', 10),
        after = req.param('after', false),
        direction = req.param('direction', 'before'),
        referenceTo = req.param('referenceTo'),
        query = null,
        nextId = '';

    if (after !== '' && after !== false && after !== 'false') { // redundant, by just covering ass.
        if (direction === 'new') {
            query = Comment.find({ referenceTo: referenceTo, deleted: false, _id: { '$gt': after } });
        } else {
            query = Comment.find({ referenceTo: referenceTo, deleted: false, _id: { '$lt': after } });
        }
    } else {
        query = Comment.find({ referenceTo: referenceTo, deleted: false });
    }

    query.populate('author', 'username username_idx displayName active verified id gender coins_balance points_balance image roles')
    .sort({ '_id': -1 })
    .limit(limit)
    .exec(function (err, posts) {
        if (err) { return next(err); }
        if (posts && posts.length > 0) {
            nextId = posts[(posts.length - 1)].id;
        } else { nextId = ''; }

        //return res.respond(200, '', { comments: posts, 'next': nextId });

        var tolang = req.param('lang', res.getLocale());
        async.each(posts, function(post, cb) {
            translate.fromObject('comment', post, tolang, 'content', function(obj) {
                post = obj;
                cb();
            });
        }, function(err) {
            res.respond(200, '', { comments: posts, 'next': nextId });
        });
    });
};


exports.create = [
    // initialize the comment
    function (req, res, next) {
        req.comment = new Comment();
        next();
    },
    // is it a discussion comment?
    function (req, res, next) {
        if (req.param('reference') === 'discussions') {
            req.comment.referenceTo = req.param('referenceTo');
            req.comment.reference = req.param('reference', 'discussions');

            Discussion.findOne({ _id: req.param('referenceTo') })
            .populate('room', '_id id displayName')
            .exec(function(err, discussion) {
                if (err) throw err;
                if (!discussion) { throw new Error('no discussion here'); }
                
                req.discussion = discussion;
                req.room = discussion.room;
                req.threshReference = req.room._id.toString();
                next();
            });
        } else {
            req.threshReference = 'master';
            next();
        }
    },
    // do threshold
    function (req, res, next) {
        Threshold.get(req.threshReference, 'post-profanity', function(thresh) {
            if (!thresh) { console.log('post-profanity missing'); }
            req.post_profanity_threshold = thresh;
            next();
        });
    },
    // get profanity list
    function (req, res, next) {
        Profanity.score(req.threshReference, req.param('content'), req.param('lang', res.getLocale()), function(occurances,severity,weighted,infractions) {
            if (weighted > req.post_profanity_threshold) {
                return res.respond(417, 'chatterbox.profanity');
                // return next(new Error('Post contains too much profanity. ('+ infractions.join(',') +')'));
            }
            req.comment.profanity_score = weighted;
            next();
        });
    },
    // we are good, lets comment!
    function (req, res, next) {
        var comment = req.comment;
        comment.author = req.user;
        comment.content = req.param('content');
        comment.lang = req.param('lang', res.getLocale());

        if (typeof req.body.thumbnail !== 'undefined') {
            comment.thumbnail = req.body.thumbnail;
        }
        if (typeof req.body.image !== 'undefined') {
            comment.image = req.body.image;
        }

        comment.save(function(err, comment) {
            if (req.discussion) {
                req.discussion.addParticipant(req.user);
                req.discussion.last_comment = comment;
                req.discussion.last_comment_by = req.user;
                req.discussion.last_comment_date = new Date().getTime();
                req.discussion.save(function(err, discussion) {
                    if (err) { return next(err); }
                    req.room.subscribe(req.user);
                    req.room.save(function(err, room) {
                        res.respond(201, res.__('addons.forums.comment.created'), { user: req.user, comment: comment });
                        ee.emit('EVENT.NEW_COMMENT', { user: req.user, comment: comment, discussion: discussion });
                        return;
                    });
                });
            } else {
                res.respond(201, res.__('addons.forums.comment.created'), { user: req.user, comment: comment });
            }

            var mentions = twitter.extractMentions(comment.content);
            async.each(mentions, function(username, cb) {
                ee.emit('EVENT.USER_MENTIONED', { user: req.user, discussion: req.discussion, comment: comment, mention: username });
                cb();
            }, function(err) {
                if (err) { log.error(err); }
            });
            
            if (comment.reference === 'discussions') {
                ee.emit('BADGE.YOU_POSTED', { user: req.user, discussion: req.param('referenceTo') });
            }
            if (typeof req.body.image !== 'undefined') {
                ee.emit('EVENT.UPLOAD_PHOTO', { user: req.user, image: req.body.image });
            }
            return;
        });

    }
];

exports.show = function (req, res, next) {
    translate.fromObject('comment', req.comment, req.param('lang', res.getLocale()), 'content', function(obj) {
        return res.respond(200, '', { comment: obj });
    });
};

exports.update = [
    
    // we need to determine the discussion and room;
    function (req, res, next) {
        if (req.comment.reference === 'discussions') {
            Discussion.findOne({ _id: req.comment.referenceTo })
            .populate('room', '_id id displayName moderators')
            .exec(function(err, discussion) {
                if (err) return next(err);
                req.discussion = discussion;
                req.room = discussion.room;
                req.threshReference = discussion.room._id;
                next();
            });
        } else {
            req.threshReference = 'master';
            next();
        }
    },

    // okay, do we have permission?
    function (req, res, next) {
        if (req.comment.reference === 'discussions') {
            if (req.discussion.room.moderators.indexOf(req.user._id.toString()) !== -1 || req.user.hasRole('admin') || req.comment.author._id.toString() === req.user._id.toString()) {
                return next();
            } else {
                return res.respond(403, res.__('global.noaccess'));
            }
        } else {
            // lets check if we are an admin, or the author of the comment.
            if (req.user.hasRole('admin') || req.comment.author._id.toString() === req.user._id.toString()) {
                next();
            } else {
                return res.respond(403, res.__('global.noaccess'));
            }
        }
    },

    // do threshold
    function (req, res, next) {
        Threshold.get(req.threshReference, 'post-profanity', function(thresh) {
            if (!thresh) { console.log('post-profanity missing'); }
            req.post_profanity_threshold = thresh;
            next();
        });
    },
    // get profanity list
    function (req, res, next) {
        Profanity.score(req.threshReference, req.param('content'), req.param('lang', res.getLocale()), function(occurances,severity,weighted,infractions) {
            if (weighted > req.post_profanity_threshold) {
                return res.respond(417, 'chatterbox.profanity');
                //return next(new Error('Post contains too much profanity. ('+ infractions.join(',') +')'));
            }
            req.comment.profanity_score = weighted;
            next();
        });
    },

    // ok, save the comment.
    function (req, res, next) {
        var comment = req.comment;

        if (typeof req.body.content !== 'undefined') {
            comment.content = req.param('content');
        }
        if (typeof req.body.lang !== 'undefined') {
            comment.lang = req.param('lang', res.getLocale());
        }
        if (typeof req.body.thumbnail !== 'undefined') {
            comment.thumbnail = req.body.thumbnail;
        }
        if (typeof req.body.image !== 'undefined') {
            comment.image = req.body.image;
        }
        comment.save(function(err) {
            if (err) { return next(err); }
            translate.deleteById(comment._id, function() {
                res.respond(200, res.__('addons.forums.comment.updated'), { user: req.user, comment: comment });
            });
        });
    }

];

exports.destroy = function (req, res, next) {
    var comment = req.comment;
    // if I am the admin ....
    if (req.user.hasRole('admin')) {
        comment.deleted         = true;
        comment.deletedAt       = new Date();
        comment.deletedReason   = 'admin removed';
        comment.save(function(err) {
            if (err) { return next(err); }
            return res.respond(200, res.__('addons.forums.comment.removed'));
        });
    }

    // if I am the author ....
    else if (req.user._id.toString() === comment.author._id.toString()) {
        comment.deleted         = true;
        comment.deletedAt       = new Date();
        comment.deletedReason   = 'author removed';
        comment.save(function(err) {
            if (err) { return next(err); }
            return res.respond(200, res.__('addons.forums.comment.removed'));
        });
    }
    
    // if not admin and not author and is discussiom ....
    else if (comment.reference === 'discussions') {
        Discussion.findOne({ _id: comment.referenceTo })
        .populate('room', 'displayName moderators')
        .exec(function(err, discussion) {
            if (err) { return next(err); }
            if (discussion.room.moderators.indexOf(req.user._id) !== -1) {
                comment.deleted         = true;
                comment.deletedAt       = new Date();
                comment.deletedReason   = 'moderator removed';
                comment.save(function(err) {
                    if (err) { return next(err); }
                    res.respond(200, res.__('addons.forums.comment.removed'));
                });
            } else { return res.respond(403, res.__('global.noaccess')); }
        });
    }
    
    // else error
    else { return res.respond(403, res.__('global.noaccess')); }
};

exports.comment = function (req, res, next, id) {
    Comment.findOne({ _id: id, deleted: false })
    .populate('author', config.settings.default_public_user_fields)
    .exec(function(err, comment) {
        if (err) { return next(err); }
        if (!comment) { return next(new Error(res.__('global.notfound'))); }
        req.comment = comment;
        next();
    });
};

exports.like = function (req, res, next) {
    var comment = req.comment;

    // check for string (legacy, deprecated)
    if (comment.like.indexOf(req.user._id.toString()) !== -1) {
        comment.like.splice(comment.like.indexOf(req.user._id.toString()), 1);
        comment.save(function (err) {
            if (err) { return next(err); }
            return res.respond(200, 'like removed', { count: comment.like.length });
        });
    }

    // no? ok, lets put it in.
    else {
        comment.like.push(req.user._id.toString());
        comment.save(function(err) {
            if (err) { return next(err); }
            res.respond(201, '', { count: comment.like.length });
            ee.emit('EVENT.LIKE_POST', { user: req.user, comment: comment });
            ee.emit('BADGE.PEOPLE_LIKE_YOUR_POST', { user: comment.author, comment: comment });
        });
    }
};

exports.lol = function (req, res, next) {
    var comment = req.comment;

    // check for string (legacy, deprecated)
    if (comment.lol.indexOf(req.user._id.toString()) !== -1) {
        comment.lol.splice(comment.lol.indexOf(req.user._id.toString()), 1);
        comment.save(function (err) {
            if (err) { return next(err); }
            return res.respond(200, 'like removed', { count: comment.lol.length });
        });
    }

    // no? ok, lets put it in.
    else {
        comment.lol.push(req.user._id.toString());
        comment.save(function(err) {
            if (err) { return next(err); }
            res.respond(201, '', { count: comment.lol.length });
            ee.emit('BADGE.SOMEONE_FINDS_YOU_FUNNY', { user: comment.author, comment: comment });
        });
    }
};

exports.flag = function (req, res, next) {
    var comment = req.comment;

    if (comment.author.hasRole('admin')) {
        return res.respond(400, res.__('addons.flag.notadmin'));
    }

    if (comment.flag.indexOf(req.user._id.toString()) !== -1) {
        comment.flag.splice(comment.flag.indexOf(req.user._id.toString()), 1);
        comment.save(function(err) {
            if (err) { return next(err); }
            return res.respond(200, 'flag removed', { count: comment.flag.length });
        });
    } else {
        comment.flag.push(req.user._id.toString());

        Threshold.get('master', 'post-flag', function(thresh) {
            if (!thresh) { console.log('thresh does not exist!'); }

            if (comment.flag.length >= thresh) {
                comment.removedText = comment.content;
                comment.content = 'The comment made by this user has been removed because it has been flagged too many times.';
                comment.lang = 'en';
                comment.removedAt = new Date();
                comment.removed = true;
                comment.deleted = true;
            } else {
                comment.removed = false;
            }

            translate.deleteById(comment._id, function() {
                comment.save(function(err) {
                    if (err) { return next(err); }
                    res.respond(201, '', { removed: comment.removed, count: comment.flag.length });
                    ee.emit('BADGE.YOU_FLAGGED', { user: req.user, comment: comment });
                });
            });

        });

        
        
    }
};

exports.byUser = function (req, res, next) {
    var page        = req.param('page', 1);
    var limit       = req.param('limit', 15);


    Comment
    .find({ author: req.profile.id, deleted: false })
    .lean()
    .populate('author', config.settings.default_public_user_fields)
    .sort('-_id')
    .paginate(page, limit, function(err, docs, total) {
        if (err) { return next(err); }
        var tolang = req.param('lang', res.getLocale());
        async.each(docs, function(comment, cb) {
            
            translate.fromObject('comment', comment, tolang, 'content', function(obj) {
                comment = obj;
                if (comment.reference === 'discussions') {
                    Discussion
                    .findOne({ _id: comment.referenceTo, deleted: false })
                    .select('displayName content lang thumbnail image room lead')
                    .populate('room', 'displayName content lang moderators')
                    .exec(function(err, discussion) {
                        if (err) { return cb(err); }
                        //console.log('discussion', discussion);
                        translate.fromObject('discussion', discussion, tolang, ['content', 'displayName'], function(doc) {
                            comment.discussion = doc;
                            //console.log('comment: ', comment);
                            cb();
                        });
                    });
                } else {
                    cb();
                }
            });

        }, function (error) {
            if (error) { return next(error); }
            res.respond(200, '', {
                total: total,
                comments: docs,
                page: page,
                pages: Math.ceil(total/limit),
                author: req.profile
            });
        });

    });
};