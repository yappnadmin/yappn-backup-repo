'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    UIError = models.model('UIError'),
    _ = require('lodash');

exports.record = function (req, res, next) {

    var data = _.extend(req.body, {
        username: (req.isAuthenticated()) ? req.user.username.toLowerCase() : 'guest'
    });

    UIError.create(data, function(err, record) {
        res.send(200, 'ok');
        if (err) console.log(err);
    });

};