'use strict';

var models = require('../config/models'),
    config = require('../config/config'),
    User = models.model('User'),
    History = models.model('History'),
    AccessToken = models.model('AccessToken'),
    Threshold = models.model('Threshold'),
    passport = require('passport'),
    async = require('async'),
    _ = require('lodash'),
    uid = require('../helpers/uid'),
    ee = require('../config/pubsub').ee;

function _gen_password() {
    var chars = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789',
        length = 8,
        password = '';
    while (length--) {
        var pos = (Math.random() * chars.length);
        password += chars.charAt(pos);
    }
    return password;
}

exports.redirectShortName = function(req, res, next) {
    User.findOne({ username_idx: req.param('shortname').toLowerCase() })
    .select('_id username_idx')
    .exec(function(err, user) {
        if (err || !user) return next();
        return res.redirect('/people/' + user.username_idx);
    });
};

exports.create = function (req, res, next) {
    var creatableFields = ['displayName','email','gender','birthdate','lang','summary','content','image','icon','thumbnail','feed','feed','username','password'];
    var user = new User();
    user.role = 'user';
    user.username_idx = req.body.username.toLowerCase();
    user.location = (req.body.location) ? { displayName: req.body.location } : null;
    user.active = true;
    user.banned = false;
    user.deleted = false;
    user.notifications = {
        mention : true,
        badge : true,
        conversation : true,
        discussion : true,
        follower : true,
        newsletter : true
    };

    creatableFields.forEach(function(field) {
        if (req.body[field]) {
            user[field] = req.body[field];
        }
    });

    user.save(function(err) {

        if (err) {
            // hashedPassword is provided by a virtual field. TODO: handle this in the model
            if ((err.errors || {}).hashedPassword) {
                err.errors.password = err.errors.hashed_password;
                delete err.errors.hashed_password;
            }

            //console.log(err);

            return next(err);
        }

        req.logIn(user, function(err) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.respond(200, { user: req.user });
        });

        ee.emit('EVENT.USER_REGISTERED', { user: user });
        return;
    });
};

exports.show = function (req, res, next) {
    res.respond(200, { profile: req.profile });
    if (req.isAuthenticated()) {
        History.capture({
            user: req.user._id,
            person: req.profile._id,
            history: 'view'
        }, function() {});
    }
};

exports.update = function (req, res, next) {

    var userId = req.user.id;
    var profileId = req.profile.id;

    if (userId !== profileId && req.user.role !== 'admin') { 
        return res.respond(403, new Error(res.__('permission_denied'))); 
    }

    var updatableFields = ['displayName','email','gender','birthdate','lang','location','summary','content','image','icon','thumbnail','notifications','feed'];
    if (req.user.hasRole('admin')) {
        updatableFields.push('password');
        updatableFields.push('username');
        updatableFields.push('plan');
        updatableFields.push('role');
        updatableFields.push('groups');
    }
    User.findById(profileId, function(err, user) {

        // Cycle through the updatable fields, set and continue.
        updatableFields.forEach(function(field) {
            if (req.body[field]) {
                user[field] = req.body[field];
            }
        });

        user.save(function(err, user) {
            if (err) return res.respond(400, err);
            return res.respond(200, { user: user.profile });
        });
    });

};

exports.destroy = function (req, res, next) {
    var userId = req.user.id;
    var profileId = req.profile.id;
    if (userId !== profileId || req.user.hasGroup('Administrators')) return res.respond(400, new Error(res.__('permission_denied')));

    User.findById(userId, function(err, user) {
        user.deleted = true;
        user.active = false;
        user.save(function(err, user) {
            if (err) return res.respond(400, err);
            return res.respond(200);
        });
    });

};

exports.changePassword = function (req, res, next) {
    var userId = req.user._id;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);

    User.findById(userId, function (err, user) {

        if (user.authenticate(oldPass)) {
            user.password = newPass;
            user.save(function(err) {
                if (err) return res.respond(400, err);
                res.respond(200);
                return;
            });
        } else {
            res.respond(403);
            return;
        }
        
    });
};

exports.avatar = function (req, res, next) {
    User.findById(req.user_id, function(err, user) {
        if (err) return res.respond(400, err);
        user.image = { url: req.param('avatar') };
        user.save(function(err) {
            if (err) return res.respond(400, err);
            return res.json({ user: user });
        });
    });
};

exports.me = function (req, res, next) {
    res.json(req.user || null);
    //return res.respond(200, { me: req.user });
};

exports.like = function (req, res, next) {
    var profile = req.profile;
    var userId = req.user.id;

    var index = profile.like.indexOf(userId);

    if (index !== -1) {
        User.findById(profile.id, function(err, user) {
            user.like.splice(index, 1);
            user.save(function(err) {
                if (err) return res.respond(400, err);
                return res.respond(200, { removed: true, count: user.like.length });
            });
        });
    } else {
        User.findById(profile.id, function(err, user) {
            user.like.push(userId);
            user.save(function(err) {
                if (err) return res.respond(400, err);
                res.respond(201, { count: user.like.length });
                ee.emit('EVENT.BE_LIKED', { user: user.profile });
                return;
            });
        });
    }
};

exports.lol = function (req, res, next) {
    var profile = req.profile;
    var userId = req.user.id;

    var index = profile.lol.indexOf(userId);

    if (index !== -1) {
        User.findById(profile.id, function(err, user) {
            user.lol.splice(index, 1);
            user.save(function(err) {
                if (err) return res.respond(400, err);
                return res.respond(200, { removed: true, count: user.lol.length });
            });
        });
    } else {
        User.findById(profile.id, function(err, user) {
            user.lol.push(userId);
            user.save(function(err) {
                if (err) return res.respond(400, err);
                res.respond(201, { count: user.lol.length });
                ee.emit('BADGE.SOMEONE_FINDS_YOU_FUNNY', { user: user.profile });
                return;
            });
        });
    }
};

exports.flag = function (req, res, next) {
    var profile = req.profile;
    var userId = req.user.id;

    Threshold.get('master', 'user-flag', function(thresh) {
        if (!thresh) { console.log('user-flag threshold missing'); }

        User.findById(profile.id, function(err, user) {
            if (err) return res.respond(400, err);
            if (!user) return res.respond(404);
            if (user.hasRole('admin')) return res.respond(403, new Error(res.__('cannot.flag.admin')));

            var index = user.flag.indexOf(userId),
                from = new Date(),
                then = new Date();

            if (index !== -1) {
                user.flag.splice(index, 1);
                user.save(function(err) {
                    if (err) return res.respond(400, err);
                    return res.respond(200);
                });
            } else {
                user.flag.push(userId);
                user.save(function(err) {
                    if (err) return res.respond(400, err);
                    res.respond(201);
                    if (user.flag.length >= thresh) {
                        user.banned = true;
                        user.bannedUntil = then.setDate(from.getDate() + 7);
                        ee.emit('EVENT.BANNED_BY_PEERS', { user: user });
                        ee.emit('BADGE.YOU_FLAGGED', { user: req.user, profile: user });
                        return;
                    }
                });
            }

        });
    });

    
};

exports.ban = function (req, res, next) {
    var profile = req.profile, 
        from = new Date(),
        then = new Date();

    User.findById(profile.id, function(err, user) {
        user.banned = true;
        user.bannedUntil = then.setDate(from.getDate() + 365);
        user.active = false;
        user.deleted = true;
        user.save(function(err) {
            if (err) return res.respond(400, err);
            res.respond(200);
            ee.emit('EVENT.USER_BANNED', { user: user });
            return;
        });
    });

};

exports.unban = function (req, res, next) {
    var profile = req.profile;
    User.findById(profile.id, function(err, user) {
        if (err) return res.respond(400, err);
        user.banned = false;
        user.active = true;
        user.deleted = false;
        user.save(function(err) {
            if (err) return res.respond(400, err);
            res.respond(200);
            ee.emit('EVENT.USER_UNBANNED', { user: user });
            return;
        });
    });
};

exports.socialAuth = [
    function (req, res, next) {
        if (_.isUndefined(req.body.id)) return res.respond(400, new Error('Social Provider ID missing'));
        var provider = req.param('social');
        
        if (req.isAuthenticated()) {
            User.findById(req.user._id, function(err, user) {
                if (err) return res.respond(400, err);
                user[provider] = req.body._json;
                user.save(function(err) {
                    if (err) return res.respond(400, err);
                    req.social_user = user;
                    next();
                });
            });
        } else {
            var query = null;
            switch(provider) {
                case 'facebook':
                    query = User.findOne({ 'facebook.id': req.body.id });
                    break;
                case 'twitter':
                    query = User.findOne({ 'twitter.id_str': req.body.id });
                    break;
            }

            query.exec(function(err, user) {
                if (err) return res.respond(400, err);

                if (!user) {
                    User.findOne({ username_idx: req.body.username.toLowerCase() }).exec(function(err, isUser) {
                        if (err) return res.respond(400, err);
                        if (isUser) req.body.username = req.body.username + '_' + _.random(0, 999);

                        user = new User();
                        user.displayName = req.body.displayName;
                        user.username = req.body.username;
                        user.lang = req.param('lang', res.getLocale());
                        if (!_.isUndefined(req.body._json.gender)) user.gender = req.body._json.gender;
                        user.password = req.body.id + '|' + new Date().getTime() + '|' + provider;
                        user.provider = provider;
                        user[provider] = req.body._json;

                        if (provider === 'facebook') {
                            user.image = { url: '//graph.facebook.com/'+ req.body.id +'/picture?type=large' };
                            user.thumbnail = { url: '//graph.facebook.com/'+ req.body.id +'/picture?type=square' };
                        } else if (provider === 'twitter') {
                            user.image = { url: req.body.avatar };
                            user.thumbnail = { url: req.body.avatar };
                        }

                        user.save(function(err) {
                            if (err) return res.respond(400, err);
                            req.social_user = user;
                            next();
                            ee.emit('EVENT.USER_REGISTERED', { user: user });
                        });

                    });
                } else {

                    if (provider === 'facebook') {
                        user.image = { url: '//graph.facebook.com/'+ req.body.id +'/picture?type=large' };
                        user.thumbnail = { url: '//graph.facebook.com/'+ req.body.id +'/picture?type=square' };

                    } else if (provider === 'twitter') {
                        user.image = { url: req.body.avatar };
                        user.thumbnail = { url: req.body.avatar };
                    }

                    user[provider] = req.body._json;
                    user.save(function(err) {
                        if (err) return res.respond(400, err);
                        req.social_user = user;
                        next();
                    });
                }
            });
        }
    },
    function (req, res, next) {
        req.logIn(req.social_user, function(err) {
            if (err) {
                console.log('login err: ', err);
                return next(err);
            }

            if (req.user) {
                console.log('\tUser logged-in:', req.user.email);
            }
    
            res.json(req.user.userInfo);
        });
    }
];

exports.activate = function (req, res, next) {
    var actCode = req.params.code;
    if (!actCode) return res.respond(403);

    User.findOne({ actication_code: actCode, deleted: false }, function(err, user) {
        if (err) return res.respond(400, err);
        if (!user) return res.respond(404);
        user.active = true;
        user.save(function(err) {
            if (err) return res.respond(400, err);
            res.respond(200);
            ee.emit('EVENT.USER_ACTIVATED', { user: user });
            return;
        });
    });
};

exports.reset = function (req, res, next) {
    var email = req.body.email;
    if (!email) return res.respond(400, new Error(res.__('users.reset.missing_email_param')));

    User.findOne({ email: email }, function(err, user) {
        if (err) return res.respond(400, err);
        if (!user) return res.respond(404);
        if (!user.active) return res.respond(400, new Error(res.__('account.not_active')));
        if (user.deleted) return res.respond(404);
        if (user.provider !== 'local') return res.respond(400, new Error(res.__('account.not_local')));

        var newPassword = _gen_password();
        user.password = newPassword;
        user.save(function(err) {
            if (err) return next(err);
            res.respond(200);
            console.log('new password ['+newPassword+'] for user: ' + user.email);
            ee.emit('EVENT.USER_RESET', { user: user, new_password: newPassword });
            return;
        });


    });

};

exports.follow = function (req, res, next) {
    var personId = req.profile.id;
    var userId = req.user.id;
    if (personId === userId) return res.respond(400, new Error(res.__('cannot.follow.self')));

    var index = req.user.following.indexOf(personId);
    if (index === -1) {
        async.parallel([
            function(callback) {
                User.findById(userId, function(err, user) {
                    if (err) return callback(err);
                    user.following.push(personId);
                    user.save(function(err) {
                        if (err) return callback(err);
                        return callback();
                    });
                });
            },
            function(callback) {
                User.findById(personId, function(err, person) {
                    if (err) return callback(err);
                    person.followers.push(userId);
                    person.save(function(err) {
                        if (err) return callback(err);
                        return callback();
                    });
                });
            }
        ], function(err, results) {
            if (err) return res.respond(400, err);
            res.respond(201);
            ee.emit('EVENT.USER_FOLLOWING', { user: req.user, person: req.profile });
            ee.emit('BADGE.YOU_FOLLOWED_SOMEONE', { user: req.user, person: req.profile });
            ee.emit('BADGE.YOU_ARE_BEING_FOLLOWED', { user: req.profile, person: req.user });
            return;
        });
    } else {
        return res.respond(400, new Error(res.__('already.following.user')));
    }

};

exports.unfollow = function (req, res, next) {
    var personId = req.profile.id;
    var userId = req.user.id;
    if (personId === userId) return res.respond(400, new Error(res.__('cannot.unfollow.self')));

    var index = req.user.following.indexOf(personId);
    if (index !== -1) {
        async.parallel([
            function(callback) {
                User.findById(userId, function(err, user) {
                    user.following.splice(index, 1);
                    user.save(function(err) {
                        if (err) return callback(err);
                        return callback();
                    });
                });
            },
            function(callback) {
                User.findById(personId, function(err, person) {
                    person.followers.splice(person.followers.indexOf(userId), 1);
                    person.save(function(err) {
                        if (err) return callback(err);
                        return callback();
                    });
                });
            }
        ], function(err, results) {
            if (err) return res.respond(400, err);
            res.respond(200);
            return;
        });
    } else {
        return res.respond(400, new Error(res.__('not.following.user')));
    }

};

exports.connection = function (req, res, next) {
    //console.log('req.profile=', req.profile);
    var personId = req.profile.id;
    var userId = req.user.id;
    if (personId === userId) return res.respond(200, { connection: 'self' });

    var following = false, follower = false, conn = 'no connection';

    if (req.user.following.indexOf(personId) !== -1) {
        following = true;
        conn = 'following';
    }

    if (req.user.followers.indexOf(personId) !== -1) {
        follower = true;
        conn = 'followed';
    }

    if (following && follower) {
        conn = 'mutual';
    }

    return res.respond(200, { connection: conn });

};

exports.followers = function (req, res, next) {

    var followers = req.profile.followers;
    var page = req.param('page', 1),
        limit = req.param('limit', 100);

    User
        .find({ _id: { $in: followers }})
        .select(config.settings.default_public_user_fields)
        .paginate(page, limit, function(err, docs, total) {
            if (err) return res.respond(400, err);
            return res.respond(200, {
                followers: docs,
                total: total,
                page: page,
                pages: Math.ceil(total/limit)
            });
        });
};

exports.following = function (req, res, next) {

    var following = req.profile.following,
        page = req.param('page', 1),
        limit = req.param('limit', 100);

    User
        .find({ _id: { $in: following }})
        .select(config.settings.default_public_user_fields)
        .paginate(page, limit, function(err, docs, total) {
            if (err) {
                console.log('error', err);
                return res.respond(400, err);
            }

            return res.respond(200, {
                following: docs,
                total: total,
                page: page,
                pages: Math.ceil(total/limit)
            });
        });

};

exports.user = function (req, res, next, username) {
    User.findOne({ username_idx: username.toLowerCase() }, function (err, user) {
        if (err) return next(err);
        if (!user) return res.respond(404);
        req.profile = user.profile;
        next();
    });
};

exports.search = function (req, res, next) {
    var page    = req.param('page', 1);
    var limit   = req.param('limit', 100);
    var re      = new RegExp(req.param('search'), 'i');
    User
    .find({ $or: [ { email: req.param('search') }, { displayName: { $regex: re } }, { username: { $regex: re } } ] })
    .select('displayName username username_idx banned active deleted removed image provider')
    .sort('displayName')
    .paginate(page, limit, function(err, docs, total) {
        if (err) return next(err);
        res.respond(200, '', {
            users: docs,
            total: total,
            page: page,
            pages: Math.ceil(total/limit)
        });
    });
};

// we need to clean up expired tokens in the database.
setInterval(function () {
    User.unBanExpired(function(err) {
        if (err) { console.log('Error unbanning users'); }
    });
}, config.settings.timeToCheckBannedUsers * 1000);