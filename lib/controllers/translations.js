'use strict';

var models = require('../config/models'),
    config = require('../config/config'),
    TranslationCacheModel = models.model('TranslationCache'),
    Translator = require('../helpers/translate').Translator;

exports.detect = function (req, res, next) {
    // this will be how we determine the language.
    return res.respond(200, '', { lang: res.getLocale() });
};

exports.translate = function (req, res, next) {
    var tag     = req.param('tag');
    var from    = req.param('fromlang');
    var to      = req.param('lang', res.getLocale());
    var text    = req.param('text');

    var t = new Translator();

    try {
        //var domain = t.checkDomain(req.get('referer'), req.param('domain'));
        t.translate(tag, from, to, text, true, function(err, response) {
            if (err) { return res.respond(400, '', t.errorToObject(err)); }
            return res.respond(response.status, response.statusMessage, response);
        });
    } catch (e) {
        console.log(e);
        return res.respond(400, e);
    }
};

setInterval(function () {
    TranslationCacheModel.removeExpired(function(err) {
        if (err) { console.log('Error removing expired translations'); }
    });
}, config.translations.db.timeToCheckExpiredCache * 1000);