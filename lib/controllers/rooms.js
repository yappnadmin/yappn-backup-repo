'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    History = models.model('History'),
    User = models.model('User'),
    Room = models.model('Room'),
    Discussion = models.model('Discussion'),
    Threshold = models.model('Threshold'),
    transaction = require('../helpers/transaction'),
    ee = require('../config/pubsub').ee;

exports.doGetRooms = function(req, res, next) {
    var page = req.param('page', 1),
        limit = req.param('limit', 25);

    Room.find({
        'endorsed': true,
        allow_discussions: true,
        isPrivate: false,
        deleted: false
    })
    .populate('parent')
    .sort('-updated')
    .paginate(page, limit, function(err, docs, total) {
        if (err) {
            return next(err);
        }
        res.respond(200, {
            total: total,
            pages: Math.ceil(total / limit),
            page: page,
            rooms: docs
        });
    });
};

exports.getRoomsByUser = function(req, res, next) {

    var page = req.param('page', 1),
    limit = req.param('limit', 25),
    query = null;

    if (req.isAuthenticated() && (req.user.hasRole('admin') || req.user.username_idx === req.profile.username.toLowerCase())) {
        query = {
            author: req.profile.id,
            deleted: false
        };
    } else {
        query = {
            author: req.profile.id,
            endorsed: true,
            deleted: false
        };
    }

    Room.find(query)
    .paginate(page, limit, function(err, rooms, total) {

        if (err) {
            return next(err);
        }

        return res.respond(200, {
            total: total,
            pages: Math.ceil(total / limit),
            page: page,
            rooms: rooms,
            author: req.profile
        });

    });
};

exports.doGetRoomsByLead = function(req, res, next) {
    var page = req.param('page', 1),
    limit = req.param('limit', 100);

    Room.find({
        'endorsed': true,
        allow_discussions: true,
        parent: req.lead._id,
        isPrivate: false,
        deleted: false
    })
    .populate('parent')
    .sort('displayName')
    .paginate(page, limit, function(err, docs, total) {
        if (err) {
            return next(err);
        }
        res.respond(200, '', {
            total: total,
            pages: Math.ceil(total / limit),
            page: page,
            rooms: docs,
            lead: req.lead
        });
    });
};

exports.doTipRoom = function(req, res, next) {
    var room = req.room;
    room.endorsed = true;
    room.percentage_endorsed = 100;
    room.save(function(err, room) {
        if (err) {
            return next(err);
        }
        return res.respond(200, 'room tipped', {
            room: room
        });
    });
};

exports.doGetLeadRooms = function(req, res, next) {
    var page = req.param('page', 1),
    limit = req.param('limit', 100);
    
    Room.find({
        parent: null,
        allow_discussions: false,
        deleted: false
    })
    .sort('displayName')
    .paginate(page, limit, function(err, docs, total) {
        if (err) {
            return next(err);
        }
        res.respond(200, '', {
            total: total,
            pages: Math.ceil(total / limit),
            page: page,
            rooms: docs
        });
    });
};

exports.doGetRoom = [

    function(req, res, next) {
        if (req.isAuthenticated()) {
            History.capture({
                room: req.room._id,
                user: req.user._id
            }, next);
        } else {
            return next();
        }
    },
    function(req, res, next) {
        var room = req.room;
        // deo the challenge here

        return res.respond(200, '', {
            room: req.room
        });
    }
];

exports.tryAndCreateRoom = function(req, res, next) {

    var settings = config.settings;
    var given = parseInt(req.param('endorsement', 0), 10);

    // do they have enough coins to open?
    if (given > req.user.coins_balance) {
        return next(new Error('You do not have enough coins. You only have ' + req.user.coins_balance + ' coins.  You need at least ' + settings.min_cost_suggest_room + ' to suggest a new room.'));
    }
    if (given < settings.min_cost_suggest_room) {
        return next(new Error('You need to give at least ' + settings.min_cost_suggest_room + ' to suggest a new room.'));
    }
    if (given > settings.max_cost_suggest_room) {
        given = settings.max_cost_suggest_room;
    }


    // go ahead and create the room.
    var room = new Room();
    room.author = req.user;
    room.author = req.user;
    room.displayName = req.param('displayName');
    room.content = req.param('content');
    room.lang = req.param('lang', res.getLocale());

    room.coins_to_open = settings.room_cost_to_tip;

    if (typeof req.body.thumbnail !== 'undefined') {
        room.thumbnail = req.body.thumbnail;
    }

    if (typeof req.body.image !== 'undefined') {
        room.image = req.body.image;
    }

    room.coins_endorsed = given;
    room.subscribe(req.user);

    if (req.param('parent')) {
        room.parent = req.param('parent');
    }

    room.allow_discussions = req.param('allow_discussions', true);

    room.save(function(err, room) {
        if (err) {
            return next(err);
        }
        transaction.create(req.user, 0, given, req.body, function(err, transaction) {
            if (err) {
                return next(err);
            }
            room.endorsements.push(transaction);
            room.save(function(err) {
                res.respond(201, 'room created', {
                    room: room
                });
                ee.emit('EVENT.ROOM_SUGGESTED', {
                    room: room,
                    user: req.user
                });
                if (typeof req.body.image !== 'undefined') {
                    ee.emit('EVENT.UPLOAD_PHOTO', {
                        user: req.user,
                        image: req.body.image
                    });
                }
                return;
            });
        });
    });
};


exports.tryAndUpdateRoom = function(req, res, next) {

    var room = req.room;

    if (room.author.username_idx === req.user.username_idx || req.user.hasRole('admin')) {
        if (req.body.endorsed) {
            delete req.body.endorsed;
        }

        Room.findByIdAndUpdateX(req.room._id, req.body, function(err, room) {
            if (err) {
                return next(err);
            }
            res.respond(200, 'room updated.', {
                room: room
            });
            //app.listener.emit('user.room_updated', { user: user, room: room });
        });
    } else {
        return res.respond(403, 'access denied');
    }

};

/**
 * Deletes an existing category
 * Auth? Yes
 */
exports.tryAndDeleteRoom = function(req, res, next) {
    if ((req.room.author._id.toString() === req.user._id.toString()) || req.user.hasRole('admin')) {
        var room = req.room;
        room.deleted = true;
        room.save(function(err, room) {
            if (err) {
                return next(err);
            }

            Discussion.find({
                room: req.room._id
            }).exec(function(err, discussions) {
                async.each(discussions, function(discussion, cb) {
                    discussion.deleted = true;
                    discussion.deletedAt = new Date();
                    discussion.deletedReason = 'Parent room was roomed';
                    discussion.save(cb);
                }, function(err) {
                    if (err) {
                        return next(err);
                    }
                    return res.respond(200, 'room deleted', {
                        room: req.room
                    });
                });
            });

        });

    } else {
        return res.respond(403, 'You do not have rights to delete this room');
    }
};

/**
 * Endorse a room waiting
 * Auth? Yes
 *
 * @param
 */
exports.tryAndEndorseRoom = function(req, res, next) {

    var settings = config.settings;
    var given = parseInt(req.param('amount', 0), 10);

    // do they have enough that they are giving?
    if (given > req.user.coins_balance) {
        return next(new Error('You do not have enough coins.  You need at least ' + settings.min_cost_suggest_room + ' to endorse this room.'));
    }
    if (given < settings.min_cost_endorse_room) {
        return next(new Error('You need at least ' + settings.min_cost_endorse_room + ' coins to endorse.'));
    }
    if (given > settings.max_cost_endorse_room) {
        given = settings.max_cost_endorse_room;
    }

    // we need to see if they can endorse the room before creating the transaction!
    var room = req.room;

    var founded = false;

    room.founders.forEach(function(founder) {
        if (founder._id.toString() === req.user._id.toString()) {
            founded = true;
        }
    });

    if (room.founders.indexOf(req.user._id.toString()) !== -1 || founded) {
        return next(new Error('You are already endorsing this room.'));
    }

    if (room.endorsed) {
        return next(new Error('Thank you, but this room is already tipped'));
    }

    var amount = given;


    room.founders.push(req.user._id);
    room.coins_endorsed = room.coins_endorsed + given;

    if (room.coins_endorsed >= room.coins_to_open) {
        room.endorsed = true;
    }

    room.subscribe(req.user);

    room.save(function(err, room) {
        if (err) {
            return next(err);
        }
        transaction.create(req.user, 0, given, req.body, function(err, transaction) {
            if (err) {
                return next(err);
            }
            room.endorsements.push(transaction);
            room.save(function(err) {
                res.respond(200, 'room endorsed', {
                    room: room,
                    amount: amount
                });
                ee.emit('EVENT.ROOM_ENDORSED', {
                    user: req.user,
                    room: room
                });

                if (room.endorsed) {
                    ee.emit('EVENT.ROOM_TIPPED', {
                        user: req.user,
                        room: room
                    });
                    ee.emit('BADGE.YOU_CREATED_A_ROOM_THAT_WENT_LIVE', {
                        user: room.author,
                        room: room
                    });
                    // founders?
                    ee.emit('BADGE.YOU_FOUNDED_A_ROOM_THAT_WENT_LIVE', {
                        user: req.user,
                        room: room
                    });
                    req.room.founders.forEach(function(founder) {
                        ee.emit('BADGE.YOU_FOUNDED_A_ROOM_THAT_WENT_LIVE', {
                            user: founder,
                            room: room
                        });
                    });
                }
                return;
            });

        });
    });


};

/**
 * Get a list of suggeted rooms
 * Auth? No
 *
 */
exports.doGetSuggested = function(req, res, next) {
    var page = req.param('page', 1);
    var limit = req.param('limit', 10);
    Room.find({
        'endorsed': false,
        deleted: false
    })
    .populate('parent')
    .populate('author', config.settings.default_public_user_fields)
    .sort('-percentage_endorsed')
    .paginate(page, limit, function(err, docs, total) {
        if (err) {
            return next(err);
        }
        res.respond(200, '', {
            total: total,
            pages: Math.ceil(total / limit),
            page: page,
            rooms: docs
        });
    });
};

exports.doGetMySupporting = function(req, res, next) {
    var page = req.param('page', 1);
    var limit = req.param('limit', 10);
    Room.find({
        founders: req.user.id,
        endorsed: false,
        deleted: false
    })
        .populate('parent')
        .populate('author', config.settings.default_public_user_fields)
        .sort('-percentage_endorsed')
        .paginate(page, limit, function(err, docs, total) {
            if (err) {
                return next(err);
            }
            res.respond(200, '', {
                total: total,
                pages: Math.ceil(total / limit),
                page: page,
                rooms: docs
            });
        });
};

exports.doGetMyRooms = function(req, res, next) {
    var page = req.param('page', 1);
    var limit = req.param('limit', 10);

    Room.find({
        author: req.user._id,
        endorsed: true,
        deleted: false
    })
        .populate('parent')
        .populate('author', config.settings.default_public_user_fields)
        .sort('-published -updated')
        .paginate(page, limit, function(err, docs, total) {
            if (err) {
                return next(err);
            }
            res.respond(200, '', {
                total: total,
                pages: Math.ceil(total / limit),
                page: page,
                rooms: docs
            });
        });
};

/**
 * Get a list of my rooms
 */
exports.doGetMySuggested = function(req, res, next) {
    var page = req.param('page', 0);
    var limit = req.param('limit', 10);

    Room.find({
        author: req.user._id,
        endorsed: false,
        deleted: false
    })
        .populate('parent')
        .populate('author', config.settings.default_public_user_fields)
        .sort('-published -updated')
        .paginate(page, limit, function(err, docs, total) {
            if (err) {
                return next(err);
            }
            res.respond(200, '', {
                total: total,
                pages: Math.ceil(total / limit),
                page: page,
                rooms: docs
            });
        });
};

/**
 * Middlware to grab the category
 */
exports.getRoom = function(req, res, next, id) {
    Room.findOne({
        _id: id,
        deleted: false
    })
        .populate('author', config.settings.default_public_user_fields)
        .populate('founders', config.settings.default_public_user_fields)
        .populate('parent')
        .exec(function(err, room) {
            if (err) {
                return next(err);
            }
            if (!room) {
                return next(new Error('room does not exist'));
            }

            req.room = room;

            if (req.isAuthenticated() && req.user.hasRole('admin')) {
                return next();
            } else if (req.isAuthenticated() && (req.user && room.author && req.user.username_idx === room.author.username_idx)) {
                return next();
            } else if (req.isAuthenticated()) {
                if (room.isPrivate) {

                    if (req.path.indexOf('/challenge') !== -1) {
                        return next();
                    }

                    var myId = req.user._id.toString();
                    if (room.members.indexOf(myId) !== -1) {
                        return next();
                    } else {
                        return res.respond(403, 'challenge required');
                    }

                } else {
                    return next();
                }

            } else {
                if (room.isPrivate) {
                    return res.respond(403, 'room is private');
                } else {
                    return next();
                }
            }

        });
};

exports.getLead = function(req, res, next, id) {
    Room.findOne({
        _id: id,
        deleted: false
    })
        .exec(function(err, lead) {
            if (err) {
                return next(err);
            }
            if (!lead) {
                return next(new Error('lead does not exist'));
            }
            req.lead = lead;
            next();
        });
};

exports.tryAndChallengeRoom = function(req, res, next) {
    var room = req.room;
    var challenge = req.param('challenge', '-iam-not-a-challenge-');
    if (challenge === room.challenge) {
        var userId = req.user._id.toString();
        room.members.push(userId);
        room.save(function(err, room) {
            if (err) {
                return next(err);
            }
            res.respond(200, 'You now have access to ' + room.displayName, {
                room: room
            });
        });
    } else {
        return res.respond(403, 'access denied');
    }
};

exports.tryAndMakeRoomPrivate = function(req, res) {
    if (req.room.author._id.toString() !== req.user._id.toString()) {
        // ok, so we are not the person who created the room.
        // are we an admin?
        if (req.user.roles.indexOf('admin') === -1) {
            // nope, no admin, lets just kick them out.
            return res.respond(400, 'You do not have rights to update this room');
        }
    }

    var room = req.room;
    room.challenge = room.generateChallenge(32);
    room.isPrivate = true;
    room.members.push(req.user._id.toString());
    room.save(function(err, room) {
        return res.respond(200, 'You have now made this room private', {
            room: room,
            challenge: room.challenge
        });
    });

};

exports.tryAndMakeUserModerator = function(req, res, next) {
    var room = req.room;
    var user = req.profile;

    if (req.user.hasRole('admin') || (room.author.username_idx === req.user.username_idx)) {
        if (room.moderators.indexOf(user._id) === -1) {
            room.moderators.push(user._id);
            room.save(function(err) {
                if (err) {
                    return next(err);
                }
                res.respond(200, user.username + ' is now a moderator in this room.');
                // do we want to email or anything from here?
                return;
            });
        } else {
            return next(new Error(user.username + ' is aleady a moderator to this room.'));
        }
    } else {
        return next(new Error('access denied'));
    }

};

exports.tryAndRemoveModerator = function(req, res, next) {
    var room = req.room;
    var user = req.profile;

    if (req.user.hasRole('admin') || (room.author.username_idx === req.user.username_idx)) {
        if (room.moderators.indexOf(user._id) >= 0) {

            room.moderators.splice(room.moderators.indexOf(user._id), 1);
            room.save(function(err) {
                if (err) {
                    return next(err);
                }
                res.respond(200, user.username + ' is no longer a moderator');
                // do we want to email or anything from here?
                return;
            });
        } else {
            return next(new Error(user.username + ' is not a moderator to this room.'));
        }
    } else {
        return next(new Error('access denied'));
    }
};

exports.tryAndLikeRoom = function(req, res, next) {
    var room = req.room;

    // check for string (legacy, deprecated)
    if (room.like.indexOf(req.user._id.toString()) !== -1) {
        room.like.splice(room.like.indexOf(req.user._id.toString()), 1);
        room.save(function(err) {
            if (err) {
                return next(err);
            }
            return res.respond(200, 'like removed');
        });
    }

    // no? ok, lets put it in.
    else {
        room.like.push(req.user._id.toString());
        room.save(function(err) {
            if (err) {
                return next(err);
            }
            res.respond(201, '', {
                status: 'ok'
            });
            ee.emit('EVENT.LIKE_POST', {
                user: req.user,
                room: room
            });
            ee.emit('BADGE.PEOPLE_LIKE_YOUR_POST', {
                user: room.author,
                room: room
            });
        });
    }


};

exports.tryAndLaughRoom = function(req, res, next) {
    var room = req.room;

    // check for string (legacy, deprecated)
    if (room.lol.indexOf(req.user._id.toString()) !== -1) {
        room.lol.splice(room.lol.indexOf(req.user._id.toString()), 1);
        room.save(function(err) {
            if (err) {
                return next(err);
            }
            return res.respond(200, 'like removed');
        });
    }

    // no? ok, lets put it in.
    else {
        room.lol.push(req.user._id.toString());
        room.save(function(err) {
            if (err) {
                return next(err);
            }
            res.respond(201, '', {
                status: 'ok'
            });
            ee.emit('BADGE.SOMEONE_FINDS_YOU_FUNNY', {
                user: room.author,
                room: room
            });
        });
    }


};

exports.tryAndFlagRoom = function(req, res, next) {
    var room = req.room;

    if (room.author.hasRole('admin')) {
        return res.respond(400, 'You cannot flag a room created by an admin');
    }

    Threshold.get('master', 'room-flag', function(thresh) {

        if (!thresh) { console.log('missing room-flag thresh'); }

        if (room.flag.indexOf(req.user._id.toString()) !== -1) {
            room.flag.splice(room.flag.indexOf(req.user._id.toString()), 1);
            return res.respond(200, 'flag removed', {
                count: room.flag.length
            });
        }
        room.flag.push(req.user._id.toString());
        room.save(function(err) {
            if (err) {
                return next(err);
            }
            res.respond(201, '', {
                removed: room.removed
            });
            ee.emit('BADGE.YOU_FLAGGED', {
                user: req.user,
                room: room
            });
            if (room.flag.length >= thresh) {
                ee.emit('EVENT.ROOM_FLAGGED', {
                    room: room
                });
            }
        });

    });

    
};

exports.doSubscribe = function(req, res, next) {
    var room = req.room;
    room.subscribe(req.user);
    room.save(function(err) {
        if (err) {
            return next(err);
        }
        res.respond(201, 'subscribed');
        return;
    });
};

exports.doUnSubscribe = function(req, res, next) {
    var room = req.room;
    room.unsubscribe(req.user);
    room.save(function(err) {
        if (err) {
            return next(err);
        }
        res.respond(201, 'unsubscribed');
        return;
    });
};