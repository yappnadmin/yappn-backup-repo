'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    Category = models.model('Category');

exports.category = function (req, res, next, slug) {
    Category.findOne({ urlCode: slug }).exec(function(err, cat) {
        if (err) return res.respond(400, err);
        if (!cat) return res.respond(404);
        req.category = cat;
        next();
    });
};

exports.destroy = function (req, res, next) {
    var cat = req.category;
    cat.active = false;
    cat.save(function(err) {
        if (err) { return next(err); }
        res.respond(200);
    });
};

exports.update = function (req, res, next) {
    var cat = req.category;
    cat = _.extend(cat, req.body);
    cat.save(function(err, cat) {
        if (err) { return next(err); }
        res.respond(200, { category: cat });
    });
};

exports.show = function (req, res, next) {
    return res.respond(200, { category: req.category });
};

exports.create = function (req, res, next) {
    var cat = new Category(req.body);
    cat.save(function(err, cat) {
        if (err) { return next(err); }
        return res.respond(201, { category: cat });
    });
};

exports.list = function (req, res, next) {
    Category.find({ active: true }).exec(function(err, cats) {
        if (err) { return next(err); }
        return res.respond(200, { categories: cats });
    });
};