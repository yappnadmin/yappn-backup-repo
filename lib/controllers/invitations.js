'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Invitation = models.model('Invitation'),
    async = require('async');

exports.generate = function (req, res, next) {

    var num = parseInt(req.param('numcodes', 1));
            
    if (num > 250) {
        return res.respond(400, 'MAX_INVITATION_CODES_EXCEEDED');
    }
    
    var per = parseInt(req.param('percode', 10));
    var codes = [];
    var data = [];
    var count = 0;

    async.whilst (
        function() { return count < num; },
        function(cb) {
            var invite = new Invitation();
            invite.actor = req.user;
            invite.allocated = per;
            invite.save(function(err, invitation) {
                codes.push(invitation.code);
                data.push(invitation);
                count++;
                cb();
            });
        },
        function(err) {
            if (err) { return next(err); }
            return res.respond(200, '', { codes: codes, data: data });
        }
    );

};