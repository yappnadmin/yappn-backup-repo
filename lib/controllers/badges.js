'use string';

var config = require('../config/config'),
    models = require('../config/models'),
    User = models.model('User'),
    Badge = models.model('Badge'),
    Action = models.model('UserAction'),
    Notification = models.model('Notification'),
    _ = require('lodash'),
    ee = require('../config/pubsub').ee,
    log = require('../helpers/logger'),
    async = require('async');

exports.trophyroom = function(req, res, next) {
    User.findById(req.profile.id, 'badges', function(err, user) {
        if (err) return res.respond(400, err);
        return res.respond(200, { badges: user.badges });
    });
};

ee.on('BADGE.*', function(data) {
    if ( _.isUndefined(data.user) || !data.user ) { return log.error('data.user is undefined or missing'); }
    if ( _.isUndefined(data.user._id) ) { return log.error('data.user._id is undefined or missing.'); }
    var trigger = this.event, user = data.user;

    Action.findOneAndUpdate({ action: trigger, user: user._id }, { $inc: { counter: 1 }, action: trigger, user: user._id }, { new: true, upsert: true }, function(err, doc) {
        if (err) return log.error(err.message);

        //console.log('Action', doc);
        //console.log('Looking for badge with trigger', trigger);

        Badge.find({ action: trigger }).exec(function(err, badges) {
            if (err) return log.error(err);
            if (badges.length === 0) return log.debug('no badges for trigger: ' + trigger);
            async.eachSeries(badges, function(badge, cb) {
                //console.log('------------------------------------------------------------------------------------------');
                //console.log('Badge', badge);

                //console.log('doc.counter ['+doc.counter+'] < badge.threshold ['+badge.threshold+']');
                if (doc.counter < badge.threshold) {
                    //console.log('did not meet threshold, get out of here.');
                    return cb();
                }

                //console.log('looking for user._id: ' + user._id + ' with badges._id: ' + badge._id);

                User.findOne({ _id: user._id, badges: { $elemMatch: { _id: badge._id } } }).exec(function(err, user_badge) {
                    if (err) return cb(err);
                    if (user_badge) {
                        //console.log('already got this badge.  out.');
                        return cb();
                    } else {
                        //console.log('did not recieve this badge, can they get it?');
                        if (doc.counter >= badge.threshold) {
                            //console.log('yes, they can get it, give it and notify.');
                            User.findById(user._id).exec(function(err, user) {
                                var b = badge.toObject();
                                b.awarded_at = new Date();
                                user.badges.push(b);
                                user.save(function(err) {
                                    if (err) return cb(err);
                                    //console.log('should be broadcasting new_badge');
                                    Notification.broadcast(user, 'badge', '<div class="clearfix"><img src="http://'+badge.icon.url+'" style="width:64px; height:64px; float:left; margin-right:10px;">You just recieved the badge for <b>' + badge.displayName + '</b></div>', function() {
                                        return cb();
                                    });
                                });
                            });
                        } else {
                            //console.log('they cannot get this yet, '+(badge.threshold - doc.counter)+' times to go.');
                            return cb();
                        }
                    }
                });
            }, function(err) {
                if (err) return log.error(err);
                //console.log('done checking for badge for this event.');
                return;
            });
        });
    });
});