'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    History = models.model('History'),
    User = models.model('User'),
    Room = models.model('Room'),
    Comment = models.model('Comment'),
    Discussion = models.model('Discussion'),
    Threshold = models.model('Threshold'),
    Profanity = models.model('Profanity'),
    ee = require('../config/pubsub').ee,
    _ = require('lodash'),
    async = require('async'),
    translate = require('../helpers/translate'),
    log = require('../helpers/logger');

module.exports = {

    doGetFeed: function(req, res, next) {

        var limit = req.param('limit', 25);
        var page = req.param('page', 0);

        if (req.user.feed.length === 0) {
            return res.respond(200, '', {
                total: 0,
                pages: 0,
                page: 0,
                discussions: []
            });
        }

        var finalTags = [];
        var userTags = req.user.feed || [];

        userTags.forEach(function(tag) {
            finalTags.push(tag.toLowerCase());
        });



        var query = Discussion.find({
            tags: {
                $in: finalTags
            },
            deleted: false
        })
            .sort('-last_comment_date')
            .populate({
                path: 'last_comment',
                match: {
                    deleted: false
                }
            })
            .populate('last_comment_by', config.settings.default_public_user_fields)
            .populate('author', config.settings.default_public_user_fields)
            .limit(limit)
            .paginate(page, limit, function(err, docs, total) {
                if (err) {
                    return next(err);
                }
                var toLang = req.param('lang', res.getLocale());
                async.each(docs, function(doc, cb) {
                    translate.fromObject('discussion', doc, toLang, ['content', 'displayName'], function(obj) {
                        doc = obj;
                        cb();
                    });
                }, function(err) {
                    res.respond(200, '', {
                        total: total,
                        pages: Math.ceil(total / limit),
                        page: page,
                        discussions: docs
                    });
                });
            });

    },

    metaShare: function(req, res, next) {
        var discussion = req.discussion;
        res.locals.metadata.url = config.host + '/discussion/' + discussion.id;
        res.locals.metadata.title = "Yappn | " + discussion.displayName;
        res.locals.metadata.description = discussion.content;
        next();
    },


    doGetRecent: [

        function(req, res, next) {

            var limit = req.param('limit', 25);
            var after = req.param('after', false);
            var before = req.param('before', false);
            var direction = req.param('direction', 'before');

            var sorted = '-last_comment_date';

            var criteria = req.param('criteria'),
                _query;
            if (criteria && criteria.length >= 3) {
                _query = Discussion.find({
                    $or: [{
                        displayName: new RegExp(criteria, 'i')
                    }, {
                        content: new RegExp(criteria, 'i')
                    }, {
                        tags: new RegExp(criteria, 'i')
                    }]
                });
                _query.where('deleted', false);
            } else {
                _query = Discussion.find().where('deleted', false);
            }

            if (after && after !== false && after !== 'false') {
                _query.where('last_comment_date').gt(after);
            }

            if (before && before !== false && before !== 'false') {
                _query.where('last_comment_date').lt(before);
            }

            var previousId = '',
                nextId = '';

            _query.where({
                deleted: false
            })
                .populate({
                    path: 'last_comment',
                    match: {
                        deleted: false
                    }
                })
                .populate('last_comment_by', config.settings.default_public_user_fields)
                .populate('author', config.settings.default_public_user_fields)
                .limit(limit)
                .sort(sorted)
                .exec(function(err, docs) {
                    if (err) {
                        return next(err);
                    }
                    if (docs && docs.length > 0) {
                        previousId = docs[docs.length - 1].last_comment_date;
                        nextId = docs[0].last_comment_date;
                    } else {
                        nextId = '';
                        previousId = '';
                    }

                    async.each(docs, function(doc, cb) {
                        var toLang = req.param('lang', res.getLocale());

                        var fieldsToTranslate = ['content', 'displayName'];

                        translate.fromObject('discussion', doc, toLang, fieldsToTranslate, function(obj) {
                            if (obj.last_comment && typeof obj.last_comment.content !== 'undefined') {
                                translate.fromObject('comment', obj.last_comment, toLang, 'content', function(cObj) {
                                    obj.last_comment = cObj;
                                    doc = obj;
                                    cb();
                                });
                            } else {
                                doc = obj;
                                cb();
                            }

                        });
                    }, function(err) {
                        res.respond(200, '', {
                            discussions: docs,
                            'next': nextId,
                            'previous': previousId,
                            count: docs.length
                        });
                    });



                });

        }
    ],

    doGetPopular: [
        function(req, res, next) {
            Threshold.get('master', 'popular-days', function(thresh) {
                if (!thresh) { console.log('missing popular-days threshold'); }
                req.popDaysThresh = thresh;
                next();
            });
        },
        function(req, res, next) {
            var page = req.param('page', 1);
            var limit = req.param('limit', 25);
            var sorted = '-isSticky -views -comments';

            var cutoff = new Date();
            cutoff.setDate(cutoff.getDate() - req.popDaysThresh);

            //console.log('im popular');
            //console.log('cutoff: ' + cutoff);

            var criteria = req.param('criteria'),
                _query;
            if (criteria && criteria.length > 3) {
                _query = Discussion.find({
                    $or: [{
                        displayName: new RegExp(criteria, 'i')
                    }, {
                        content: new RegExp(criteria, 'i')
                    }]
                });
                _query.where('deleted', false);
            } else {
                _query = Discussion.find().where('deleted', false);
            }

            _query.find({
                deleted: false,
                updated: {
                    $gte: cutoff
                }
            })
                .populate({
                    path: 'last_comment',
                    match: {
                        deleted: false
                    }
                })
                .populate('last_comment_by', config.settings.default_public_user_fields)
                .populate('author', config.settings.default_public_user_fields)
                .populate('room')
                .populate('lead')
                .sort(sorted)
                .paginate(page, limit, function(err, docs, total) {
                    if (err) {
                        return next(err);
                    }

                    async.each(docs, function(doc, cb) {
                        var toLang = req.param('lang', res.getLocale());
                        translate.fromObject('discussion', doc, toLang, ['content', 'displayName'], function(obj) {
                            doc = obj;
                            cb();
                        });
                    }, function(err) {
                        res.respond(200, '', {
                            total: total,
                            pages: Math.ceil(total / limit),
                            page: page,
                            discussions: docs
                        });
                    });
                });
        }
    ],

    doGetTrending: [

        function(req, res, next) {
            var page = req.param('page', 1);
            var limit = req.param('limit', 25);
            var sorted = req.param('sort', '-last_comment_date -published -updated');
            var criteria = req.param('criteria'),
                _query;
            if (criteria && criteria.length > 3) {
                _query = Discussion.find({
                    $or: [{
                        displayName: new RegExp(criteria, 'i')
                    }, {
                        content: new RegExp(criteria, 'i')
                    }]
                });
                _query.where('deleted', false);
            } else {
                _query = Discussion.find().where('deleted', false);
            }

            _query.find({
                deleted: false
            })
                .populate({
                    path: 'last_comment',
                    match: {
                        deleted: false
                    }
                })
                .populate('last_comment_by', config.settings.default_public_user_fields)
                .populate('author', config.settings.default_public_user_fields)
                .populate('room')
                .populate('lead')
                .sort(sorted)
                .paginate(page, limit, function(err, docs, total) {
                    if (err) {
                        return next(err);
                    }

                    async.each(docs, function(doc, cb) {
                        var toLang = req.param('lang', res.getLocale());
                        translate.fromObject('discussion', doc, toLang, ['content', 'displayName'], function(obj) {
                            if (typeof doc.last_comment.content !== 'undefined') {
                                translate.fromObject('comment', doc.last_comment, toLang, 'content', function(comment) {
                                    obj.last_comment = comment;
                                    doc = obj;
                                    cb();
                                });
                            } else {
                                doc = obj;
                                cb();
                            }

                        });
                    }, function(err) {
                        res.respond(200, '', {
                            total: total,
                            pages: Math.ceil(total / limit),
                            page: page,
                            discussions: docs
                        });
                    });

                });
        }
    ],

    /**
     * Get a list of discussions within a category
     * Auth? Not required. But perpective will be of a guest
     *
     * @param {ObjectId} category
     * @param {Integer} page
     */
    doGetDiscussionsByCategory: [

        function(req, res, next) {
            var page = req.param('page', 1);
            var limit = req.param('limit', 25);
            var sorted = req.param('sort', '-last_comment_date -published -updated');

            Discussion.find({
                room: req.room._id,
                deleted: false
            })
                .populate({
                    path: 'last_comment',
                    match: {
                        deleted: false
                    }
                })
                .populate('last_comment_by', config.settings.default_public_user_fields)
                .populate('author', config.settings.default_public_user_fields)
                .populate('room')
                .sort(sorted)
                .paginate(page, limit, function(err, docs, total) {
                    if (err) {
                        return next(err);
                    }

                    async.each(docs, function(doc, cb) {
                        var toLang = req.param('lang', res.getLocale());
                        translate.fromObject('discussion', doc, toLang, ['content', 'displayName'], function(obj) {
                            doc = obj;
                            cb();
                        });
                    }, function(err) {
                        res.respond(200, '', {
                            total: total,
                            pages: Math.ceil(total / limit),
                            page: page,
                            discussions: docs,
                            room: req.room
                        });
                    });
                });
        }
    ],

    /**
     * Get a list of discussions within a category
     * Auth? Not required. But perpective will be of a guest
     *
     * @param {ObjectId} category
     * @param {Integer} page
     */
    doGetDiscussionsByLead: [

        function(req, res, next) {
            var page = req.param('page', 1);
            var limit = req.param('limit', 25);

            var sorted = req.param('sort', '-last_comment_date -published -updated');

            Discussion.find({
                lead: req.lead._id,
                deleted: false
            })
                .populate({
                    path: 'last_comment',
                    match: {
                        deleted: false
                    }
                })
                .populate('last_comment_by', config.settings.default_public_user_fields)
                .populate('author', config.settings.default_public_user_fields)
                .populate('room')
                .sort(sorted)
                .paginate(page, limit, function(err, docs, total) {
                    if (err) {
                        return next(err);
                    }

                    async.each(docs, function(doc, cb) {
                        var toLang = req.param('lang', res.getLocale());
                        translate.fromObject('discussion', doc, toLang, ['content', 'displayName'], function(obj) {
                            doc = obj;
                            cb();
                        });
                    }, function(err) {
                        res.respond(200, '', {
                            total: total,
                            pages: Math.ceil(total / limit),
                            page: page,
                            discussions: docs,
                            lead: req.lead
                        });
                    });

                });
        }
    ],

    /**
     * Create a new discussion
     * Auth? Yes
     *
     * @param {String} name
     * @param {String} body
     * @param {ObjectId} category
     * @param {CSV} tags
     * @param {String} type
     * @param {Boolean} closed
     * @param {Boolean} announce
     * @param {Boolean} sink
     */
    doCreateDiscussion: [

        // first lets initialize the discussion
        function (req, res, next) {
            req.discussion = new Discussion();
            next();
        },

        // if no room, go create the default one!
        function (req, res, next) {
            if (req.room) {
                req.default_room = req.room;
                next();
            } else {
                Room.findOne({ displayName: config.room.defaults.displayName }).exec(function(err, room) {
                    if (err) { return next(err); }
                    if (!room) {
                        room = new Room(config.room.defaults);
                        room.author = req.user;
                        room.save(function(err, room) {
                            if (err) { return next(err); }
                            req.default_room = room;
                            next();
                        });
                    } else {
                        req.default_room = room;
                        next();
                    }
                });
            }
        },

        // threshold
        function (req, res, next) {
            var ref = req.default_room._id.toString();
            req.threshReference = ref;
            
            Threshold.get(ref, 'post-profanity', function(thresh) {
                if (!thresh) { console.log('post-profanity missing'); }
                req.post_profanity_threshold = thresh;
                
                next();
            });
        },

        // profanity
        function (req, res, next) {
            Profanity.score(req.threshReference, req.param('content'), req.param('lang', res.getLocale()), function(occurances,severity,weighted,infractions) {
                if (weighted > req.post_profanity_threshold) {
                    return res.respond(417, 'chatterbox.profanity');
                    //return next(new Error('Post contains too much profanity. ('+ infractions.join(',') +')'));
                }
                req.discussion.profanity_score = weighted;
                next();
            });
        },

        // am I allowed to post in here?
        function (req, res, next) {
            if (!req.default_room.allow_discussions) {
                return next(new Error(res.__('addons.forums.discussions.notallowed')));
            }
            next();
        },

        // create the discussion.
        function(req, res, next) {

            var discussion = req.discussion;
            discussion.room = req.default_room;
            discussion.displayName = req.param('displayName');
            discussion.content = req.param('content');
            discussion.tags = req.param('tags', []);
            discussion.lang = req.param('lang');
            discussion.author = req.user;
            discussion.last_comment_date = new Date().getTime();

            discussion.participants.push(req.user._id);

            if (typeof req.body.thumbnail !== 'undefined') {
                discussion.thumbnail = req.body.thumbnail;
            }

            if (typeof req.body.image !== 'undefined') {
                discussion.image = req.body.image;
            }

            if (typeof req.body.masthead !== 'undefined') {
                discussion.masthead = req.body.masthead;
            }

            if (typeof req.body.externalcss !== 'undefined') {
                discussion.externalcss = req.body.externalcss;
            }

            if (typeof req.body.style !== 'undefined') {
                discussion.style = req.body.style;
            }

            discussion.save(function(err, discussion) {
                if (err) { console.log(err); }
                if (err) { return next(err); }

                res.respond(201, res.__('addons.forums.discussions.created'), {
                    discussion: discussion
                });

                ee.emit('EVENT.NEW_DISCUSSION', {
                    user: req.user,
                    discussion: discussion
                });

                ee.emit('BADGE.YOU_POSTED', {
                    user: req.user,
                    discussion: discussion
                });

                if (typeof req.body.image !== 'undefined') {
                    ee.emit('EVENT.UPLOAD_PHOTO', {
                        user: req.user,
                        image: req.body.image
                    });
                }

                return;
            });


        }
    ],

    /**
     * Edit a discussion
     * Auth? Yes
     *
     * @param {String} name
     * @param {String} body
     * @param {ObjectId} category
     * @param {CSV} tags
     * @param {String} type
     * @param {Boolean} closed
     * @param {Boolean} announce
     * @param {Boolean} sink
     */
    tryAndUpdateDiscussion: [


        // check to see if I am allowed to edit this.
        function (req, res, next) {
            if (req.user.hasRole('admin') || req.discussion.author._id.toString() === req.user._id.toString()) {
                return next();
            } else {
                return res.respond(403, res.__('global.noaccess'));
            }
        },

        // do threshold
        function (req, res, next) {
            req.threshReference = (req.discussion.room) ? req.discussion.room._id.toString() : 'master';
            Threshold.get(req.threshReference, 'post-profanity', function(thresh) {
                if (!thresh) { console.log('post-profanity missing'); }
                req.post_profanity_threshold = thresh;
                next();
            });
        },
        
        // get profanity list
        function (req, res, next) {
            Profanity.score(req.threshReference, req.param('content'), req.param('lang', res.getLocale()), function(occurances,severity,weighted,infractions) {
                if (weighted > req.post_profanity_threshold) {
                    return res.respond(417, 'chatterbox.profanity');
                    //return next(new Error('Post contains too much profanity. ('+ infractions.join(',') +')'));
                }
                req.discussion.profanity_score = weighted;
                next();
            });
        },

        // save the discussion.
        function(req, res, next) {
            var discussion = req.discussion;

            if (req.user.hasRole('admin') || discussion.author._id.toString() === req.user._id.toString()) {
                discussion = _.extend(discussion, req.body);

                discussion.save(function(err, discussion) {
                    if (err) {
                        return next(err);
                    }

                    translate.deleteById(discussion._id, function() {
                        return res.respond(200, res.__('addons.forums.discussions.updated'), {
                            discussion: discussion
                        });
                    });

                });
            } else {
                return res.respond(403, res.__('global.noaccess'));
            }
        }
    ],

    /**
     * Remove a discussion
     * Auth? Yes
     */
    tryAndDeleteDiscussion: [

        function(req, res, next) {
            if (req.user.hasRole('admin') || req.discussion.author._id.toString() === req.user._id.toString() || req.discussion.room.moderators.indexOf(req.user._id) !== -1) {
                var discussion = req.discussion;
                discussion.deleted = true;
                discussion.deletedAt = new Date();
                discussion.save(function(err) {
                    if (err) {
                        return next(err);
                    }

                    Comment.find({
                        reference: 'discussions',
                        referenceTo: req.discussion._id
                    }).exec(function(err, comments) {
                        if (err) {
                            return next(err);
                        }
                        async.each(comments, function(comment, cb) {
                            comment.deleted = true;
                            comment.deletedAt = new Date();
                            comment.deletedReason = 'Discussion removed';
                            comment.save(cb);
                        }, function(err) {
                            if (err) {
                                return next(err);
                            }
                            translate.deleteById(discussion._id, function() {
                                return res.respond(200, res.__('addons.forums.discussions.removed'), {
                                    discussion: discussion
                                });
                            });
                        });
                    });

                });

            } else {
                return next(new Error(res.__('global.noaccess')));
            }

        }
    ],

    doGetDiscussion: [

        function(req, res, next) {
            if (req.isAuthenticated()) {
                History.capture({
                    discussion: req.discussion._id,
                    user: req.user._id
                }, next);
            } else {
                return next();
            }
        },
        function(req, res, next) {
            var discussion = req.discussion;

            // check for moderator compatible...

            var tolang = req.param('lang', res.getLocale());
            translate.fromObject('discussion', req.discussion, tolang, ['content', 'displayName'], function(obj) {
                res.respond(200, '', {
                    discussion: obj
                });

                ee.emit('EVENT.DISCUSSION_VIEW', {
                    discussion: discussion
                });

                if (req.isAuthenticated()) {
                    //console.log('they are authed')
                    ee.emit('EVENT.VISIT_ROOM', {
                        user: req.user,
                        discussion: discussion
                    });
                    ee.emit('BADGE.YOU_VISITED_ROOM', {
                        user: req.user,
                        discussion: discussion
                    });
                }


            });

            

        }
    ],

    /**
     * discussion middleware
     */
    getDiscussion: function(req, res, next, id) {
        Discussion.findOne({
            _id: id,
            deleted: false
        })
            .populate({
                path: 'last_comment',
                match: {
                    deleted: false
                }
            })
            .populate('last_comment_by', config.settings.default_public_user_fields)
            .populate('author', config.settings.default_public_user_fields)
            .populate('room')
            .populate('lead')
            .exec(function(err, doc) {

                if (err) {
                    return next(err);
                }
                if (!doc) {
                    return next(new Error(res.__('global.notfound')));
                }

                var room = doc.room;
                var toLang = req.param('lang', res.getLocale());

                //console.log('DOC', doc);

                if (req.isAuthenticated() && req.user.hasRole('admin')) {
                    req.discussion = doc;
                    return next();
                } else if (req.isAuthenticated() && (req.user._id.toString() === room.author.toString())) { // because no popolation
                    req.discussion = doc;
                    return next();
                } else if (req.isAuthenticated()) {
                    if (room.isPrivate) {

                        if (req.path.indexOf('/challenge') !== -1) {
                            return next();
                        }

                        var myId = req.user._id.toString();
                        if (room.members.indexOf(myId) !== -1) {
                            req.discussion = doc;
                            return next();
                        } else {
                            return res.respond(403, 'challenge required');
                        }

                    } else {
                        req.discussion = doc;
                        return next();
                    }

                } else {
                    if (room.isPrivate) {
                        return res.respond(403, 'room is private');
                    } else {
                        req.discussion = doc;
                        return next();
                    }
                }

            });
    },

    getDiscussionsByUser: [

        function(req, res, next) {
            var page = req.param('page', 0);
            page = parseInt(page, 10);

            var limit = req.param('limit', 25);
            limit = parseInt(limit, 10);

            var sorted = '-_id';

            Discussion.find({
                author: req.profile.id,
                deleted: false
            })
                .populate({
                    path: 'last_comment',
                    match: {
                        deleted: false
                    }
                })
                .populate('last_comment_by', config.settings.default_public_user_fields)
                .populate('author', config.settings.default_public_user_fields)
                .populate('room')
                .populate('lead')
                .sort(sorted)
                .paginate(page, limit, function(err, discussions, total) {
                    if (err) {
                        return next(err);
                    }
                    var tolang = req.param('lang', res.getLocale());
                    async.each(discussions, function(discussion, cb) {
                        translate.fromObject('discussion', discussion, tolang, ['content', 'displayName'], function(obj) {
                            discussion = obj;
                            cb();
                        });
                    }, function(err) {
                        if (err) {
                            return next(err);
                        }
                        return res.respond(200, '', {
                            total: total,
                            pages: Math.ceil(total / limit),
                            page: page,
                            discussions: discussions,
                            'author': req.profile
                        });
                    });
                });
        }
    ],

    removeDiscussionsByUser: function(req, res, next) {
        if (req.user.hasRole('admin') || req.profile.username_idx === req.user.username_idx) {
            Discussion.find({
                author: req.profile._id
            }).exec(function(err, discussions) {
                if (err) {
                    return next(err);
                }
                async.each(discussions, function(discussion, cb) {
                    translate.deleteById(discussion._id, function() {
                        discussion.remove(cb);
                    });
                }, function(err) {
                    if (err) {
                        return next(err);
                    }
                    return res.respond(200, 'discussions by [' + req.profile.username + '] have been deleted.');
                });
            });
        } else {
            return next(new Error(res.__('global.notfound')));
        }
    },

    tryAndLikeDiscussion: [

        function(req, res, next) {
            var discussion = req.discussion;

            // check for string (legacy, deprecated)
            if (discussion.like.indexOf(req.user._id.toString()) !== -1) {
                discussion.like.splice(discussion.like.indexOf(req.user._id.toString()), 1);
                discussion.save(function(err) {
                    if (err) {
                        return next(err);
                    }
                    return res.respond(200, 'like removed', {
                        count: discussion.like.length
                    });
                });
            }

            // no? ok, lets put it in.
            else {
                discussion.like.push(req.user._id.toString());
                discussion.save(function(err) {
                    if (err) {
                        return next(err);
                    }

                    Room.findOne({
                        _id: discussion.room._id
                    }).exec(function(err, room) {
                        room.subscribe(req.user);
                        room.save(function(err) {
                            if (err) {
                                return next(err);
                            }
                            res.respond(201, '', {
                                count: discussion.like.length
                            });
                        });
                    });

                    ee.emit('EVENT.LIKE_POST', {
                        user: req.user,
                        discussion: discussion
                    });
                    ee.emit('BADGE.PEOPLE_LIKE_YOUR_POST', {
                        user: discussion.author,
                        discussion: discussion
                    });
                });
            }


        }
    ],

    tryAndLaughDiscussion: [

        function(req, res, next) {
            var discussion = req.discussion;

            // check for string (legacy, deprecated)
            if (discussion.lol.indexOf(req.user._id.toString()) !== -1) {
                discussion.lol.splice(discussion.lol.indexOf(req.user._id.toString()), 1);
                //console.log('lol removed');
                discussion.save(function(err) {
                    if (err) {
                        return next(err);
                    }
                    return res.respond(200, 'lol removed', {
                        count: discussion.lol.length
                    });
                });
            }

            // no? ok, lets put it in.
            else {
                discussion.lol.push(req.user._id.toString());
                //console.log('lol added')
                discussion.save(function(err) {
                    if (err) {
                        return next(err);
                    }
                    res.respond(201, '', {
                        count: discussion.lol.length
                    });
                    ee.emit('BADGE.SOMEONE_FINDS_YOU_FUNNY', {
                        user: discussion.author,
                        discussion: discussion
                    });
                });
            }


        }
    ],

    tryAndFlagDiscussion: [

        function(req, res, next) {
            var discussion = req.discussion;

            // if the author of this discussion is an admin, then do not allow it to be flagged.
            if (discussion.author.hasRole('admin')) {
                return res.respond(400, 'You cannot flag a discussion created by an admin');
            }

            if (discussion.flag.indexOf(req.user._id.toString()) !== -1) {
                discussion.flag.splice(discussion.flag.indexOf(req.user._id.toString()), 1);
                discussion.save(function(err) {
                    if (err) {
                        return next(err);
                    }
                    return res.respond(200, 'flag removed', {
                        count: discussion.flag.length
                    });
                });
            } else {
                discussion.flag.push(req.user._id.toString());

                Threshold.get('master', 'discussion-flag', function(thresh) {
                    if (!thresh) { console.log('missing discussion-flag threshold value'); }

                    if (discussion.flag.length >= thresh) {
                        discussion.removedText = discussion.content;
                        discussion.removedAt = new Date();
                        discussion.removed = true;
                    } else {
                        discussion.removed = false;
                    }

                    discussion.save(function(err) {
                        if (err) {
                            return next(err);
                        }
                        res.respond(201, '', {
                            removed: discussion.removed,
                            count: discussion.flag.length
                        });
                        ee.emit('BADGE.YOU_FLAGGED', {
                            user: req.user,
                            discussion: discussion
                        });
                    });

                });
            }


        }
    ],

    tryAndMoveDiscussion: [

        function(req, res, next) {
            var discussion = req.discussion;
            var room = req.room;
            var lead = room.lead;

            if (req.user.hasRole('admin') || discussion.room.moderators.indexOf(req.user._id) !== -1) {
                discussion.room = room;
                discussion.lead = lead;
                discussion.save(function(err, discussion) {
                    if (err) {
                        return next(err);
                    }
                    return res.respond(200, 'discussion moved', {
                        discussion: discussion,
                        room: room,
                        lead: lead
                    });
                });
            } else {
                return res.respond(403, 'access denied');
            }

        }
    ]
};