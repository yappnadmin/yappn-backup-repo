'use strict';

var models = require('../config/models'),
    Activity = models.model('Activity');

exports.list = function (req, res, next) {
    var page = req.param('page', 1),
        limit = req.param('limit', 25),
        query = null;

    if (req.isAuthenticated()) {
        var criteria = req.user.following;
        criteria.push(req.user.id);

        query = Activity.find({ $or: [{'actor._id': { $in: criteria } }, { 'object._id': req.user.id}] });
    } else {
        query = Activity.find();
    }

    query.sort('-published')
    .paginate(page, limit, function(err, docs, total) {
        if (err) { return next(err); }
        res.respond(200, {
            page: page,
            pages: Math.ceil(total/limit),
            total: total,
            activities: docs
        });
    });


};