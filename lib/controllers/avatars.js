'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Avatar = models.model('Avatar'),
    User = models.model('User');

exports.getByUsersname = function (req, res, next) {
    var username = req.param('username').toLowerCase();
    User.findOne({ username_idx: username }, function(err, user) {
        if (err) { return res.redirect('http://cdn.yappn.com/common-images/avatars/A03.png'); }
        if (!user) { return res.redirect('http://cdn.yappn.com/common-images/avatars/A03.png'); }
        if (typeof (user.image) === 'undefined' || typeof user.image.url === 'undefined') { return res.redirect('http://cdn.yappn.com/common-images/avatars/A03.png'); }
        return res.redirect(user.image.url);
    });
};

exports.getPackLists = function (req, res, next) {
    Avatar.find({ available: true }).exec(function(err, packs) {
        if (err) { return next(err); }
        return res.respond(200, '', { packs: packs });
    });
};

exports.getAvatarsByPackId = function (req, res, next) {
    return res.respond(200, '', { pack: req.avatarPack });
};

exports.doCreatePack = function (req, res, next) {
    var pack = new Avatar(req.body);
        pack.available = false;
        pack.save(function(err, pack) {
            if (err) { return next(err); }
            return res.respond(201, 'Pack created', { pack: pack });
        });
};

exports.doUpdatePack = function (req, res, next) {
    if (req.param('avatars')) {
        delete req.body.avatars;
    }

    Avatar.findByIdAndUpdateX(req.avatarPack._id, req.body, function(err, pack) {
        if (err) { return next(err); }
        res.respond(200, 'pack updated', { pack: pack });
    });
};

exports.doMakePackAvailable = function (req, res, next) {
    var pack = req.avatarPack;
    pack.available = true;
    res.respond(200, 'pack is now available', { pack: pack });
};

exports.doRemovePack = function (req, res, next) {
    var pack = req.avatarPack;
    res.respond(200, 'pack is no longer available', { pack: pack });
    pack.remove();
};

exports.doPushAvatarToPack = function (req, res, next) {
    var pack = req.avatarPack;
    var MediaLinkHash = {
        height: req.param('height', null),
        width: req.param('width', null),
        url: req.param('url', null)
    };

    if (!MediaLinkHash.url) { return next(new Error('You must supply the url for the avatar')); }

    pack.avatars.push(MediaLinkHash);
    pack.save(function(err) {
        if (err) { return next(err); }
        return res.respond(200, '', { avatar: MediaLinkHash });
    });
};

exports.getPackByParam = function (req, res, next, id) {
    Avatar.findOne({ _id: id }, function(err, pack) {
        if (err) { return next(err); }
        if (!pack) { return next(new Error('That avatar pack does not exist')); }
        req.avatarPack = pack;
        next();
    });
};