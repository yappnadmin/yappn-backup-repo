'use strict';

var leaderboard = require('../helpers/leaderboard').board;

exports.ranking = function (req, res, next) {
    leaderboard.list(function(err, list) {
        if (err) return res.respond(400, err);
        return res.respond(200, { leaderboard: list });
    });
};

exports.byUsername = function (req, res, next) {
    var username = req.param('lbuser').toLowerCase();
    leaderboard.rank(username, function(err, rank) {
        if (err) return res.respond(400, err);
        return res.respond(200, { ranking: rank });
    });
};