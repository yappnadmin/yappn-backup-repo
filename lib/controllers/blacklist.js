'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Blacklist = models.model('Blacklist');

exports.middleware = function(req, res, next) {
    next();
};

exports.list = function(req, res, next) {
    var page = req.param('page', 1),
        limit = req.param('limit', 25);

    Blacklist.find().paginate(page, limit, function (err, docs, total) {
        if (err) { return res.respond(400, err); }
        return res.respnd(200, {
            total: total,
            blacklist: docs, 
            pages: Math.ceil(total/limit),
            page: page
        });
    });
};

exports.create = function(req, res, next) {
    Blacklist.add(req.body, function(err, result) {
        if (err) return res.respond(400, err);
        return res.respond(201);
    });
};

exports.destroy = function (req, res, next) {
    Blacklist.findOne(req.param('blacklist')).exec(function(err, blacklist) {
        if (err) return res.respond(400, err);
        if (!blacklist) return res.respond(404);
        blacklist.remove(function(err) {
            if (err) res.respond(400, err);
            return res.respond(200);
        });
    });
};