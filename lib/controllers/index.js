'use strict';

var _ = require('lodash'),
    path = require('path'),
    configExpose = require('../config/config-expose');

/**
 * Send client configuration data as JSON for OAuth clients
 */
exports.yappnConfig = function(req, res) {
  res.json(configExpose);
};


/**
 * Send partial, or 404 if it doesn't exist
 */
exports.partials = function(req, res) {
    var stripped = req.url.split('.')[0];
    var requestedView = path.join('./', stripped);
    res.render(requestedView, function(err, html) {
        if(err) {
            console.log("Error rendering partial '" + requestedView + "'\n", err);
            res.status(404);
            res.send(404);
        } else {
            res.send(html);
        }
    });
};

/**
 * Send our single page app
 */
exports.index = function(req, res) {

    if (req.path === '/%7B%7Bimageurl%7D%7D') {
        return res.send(404);
    }
    
    res.render('index', { yappnConfig: JSON.stringify(configExpose), meta: res.metadata  });
};
