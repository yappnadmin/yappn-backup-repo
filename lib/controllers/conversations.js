'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    ee = require('../config/pubsub').ee,
    realtime = require('../helpers/realtime').realtime,
    User = models.model('User'),
    Conversation = models.model('Conversation'),
    _ = require('lodash'),
    i18n = require('i18n'),
    async = require('async'),
    translate = require('../helpers/translate');

ee.on('NEW_COMMENT_ADDED', function(data) {
    var conversation = data.conversation;
    var comment = data.comment;
    var ignore_username = data.ignore;

    var recipients = _.without(conversation.recipients, ignore_username);

    User.find({ username_idx: { $in: recipients }}, function(err, users) {
        if (err) throw err;
        users.forEach(function(user) {
            user.new_messages.push(comment._id.toString());
            user.save(function(err, user) {
                if (err) throw err;
                // todo: notify this user of a new message in a certain conversation.
                realtime.pushTo(user.username.toLowerCase(), i18n.__('component.conversation.new.message'), { conversation: conversation, comment: comment });
                return;
            });
        });
    });
});

var canModifyConversation = function(req, res, next) {
    var conversation = req.conversation;
    var hasAccess = false;
    if (conversation.recipients.indexOf(req.user.username.toLowerCase()) !== -1) { hasAccess = true; }
    if (!hasAccess) { return res.respond(400, res.__('component.conversation.denied')); }
    next();
};

exports.getConversations = function (req, res, next) {
    var page = req.param('page', 1),
        limit = req.param('limit', 25),
        options = {
            limit: limit,
            page: page,
        };

    options.criteria = { recipients: req.user.username.toLowerCase() };

    Conversation.list(options, function(err, docs, total) {
        if (err) { return next(err); }
        if (!docs || total === 0) {
            return res.respond(200, '', {
                total: total,
                pages: Math.ceil(total/limit),
                page: page,
                conversations: docs
            });
        }

        async.each(docs, function(doc, callback) {
            var comment = doc.comments[doc.comments.length - 1];
            comment.body = decodeURI(comment.body);

            if ( typeof comment.lang === 'undefined' ) {
                comment.lang = 'en';
            }

            var tolang = req.param('lang', res.getLocale());
            translate.fromObject('convo', comment, tolang, 'body', function(obj) {
                doc.last_message = obj;
                callback();
            });
        }, function(err) {
            if (err) { return next(err); }
            res.respond(200, '', {
                total: total,
                pages: Math.ceil(total/limit),
                page: page,
                conversations: docs
            });
        });
    });
};

exports.createConversation = function(req, res, next) {
    if (!req.param('body')) {
        return res.respond(400, 'You must include conversation text');
    } else {
        req.body.body = encodeURI(req.body.body);
    }

    if (!req.param('recipients')) { return res.respond(400, res.__('component.conversation.norecipient')); }

    var conversation = new Conversation(req.body);
    conversation.recipients.push(req.user.username.toLowerCase());
    
    var input = {
        body: req.param('body', 'no comment specified'),
        lang: req.param('lang', req.param('lang', res.getLocale())),
        thumbnail: null,
        image: null
    };

    if (typeof req.body.thumbnail !== 'undefined') {
        input.thumbnail = req.body.thumbnail;
    }

    if (typeof req.body.image !== 'undefined') {
        input.image = req.body.image;
    }



    conversation.addComment(req.user, input, function(err, conversation, comment) {
        if (err) {
            var err_message = err.message || err;
            //var err_message = ((err+"").indexOf("duplicate key error") > -1) ? "That email address is already registered." : err;
            if (err.name === 'ValidationError') {
                err_message = res.__('global.validation.please.check');
                var error_fields = [];
                _.each(err.errors, function(e) { error_fields.push(e.path); });
                err_message += error_fields.join(', ');
            }

            return res.respond(400, err_message);
        }
        else {
            res.respond(200, 'NEW_CONVERSATION', { conversation: conversation });
            ee.emit('EVENT.NEW_PRIVATE_MESSAGE', { comment: comment, conversation: conversation, ignore: comment.user.toLowerCase() });
            return;
        }
    });
};



exports.destroyConversation = [
    canModifyConversation,
    function (req, res, next) {
        var conversation = req.conversation;
        _.each(conversation.recipients, function(username, index) {
            if (username === req.user.username.toLowerCase()) {
                conversation.recipients.splice(index,1);
            }
        });

        conversation.save(function(err, conversation) {
            if (err) {
                var err_message = err.message || err;
                //var err_message = ((err+"").indexOf("duplicate key error") > -1) ? "That email address is already registered." : err;
                if (err.name === 'ValidationError') {
                    err_message = 'Please check the following fields: ';
                    var error_fields = [];
                    _.each(err.errors, function(e) { error_fields.push(e.path); });
                    err_message += error_fields.join(', ');
                }

                return res.respond(400, err_message);
            }
            else { return res.respond(200, res.__('component.conversation.removed'), { conversation: conversation }); }
        });
    }
];

exports.createPost = [
    canModifyConversation,
    function (req, res, next) {

        if (!req.param('body')) {
            return res.respond(400, res.__('global.bad.params'));
        } else {
            req.body.body = encodeURI(req.body.body);
        }


        var conversation = req.conversation;

        var input = {
            body: req.param('body', 'no comment specified'),
            lang: req.param('lang', req.param('lang', res.getLocale())),
            thumbnail: null,
            image: null
        };

        if (typeof req.body.thumbnail !== 'undefined') {
            input.thumbnail = req.body.thumbnail;
        }

        if (typeof req.body.image !== 'undefined') {
            input.image = req.body.image;
        }

        conversation.addComment(req.user, input, function(err, conversation, comment) {
            if (err) { return next(err); }
            
            res.respond(201, 'NEW_COMMENT', { comment: comment });
            //ee.emit('NEW_COMMENT_ADDED', { comment: comment, conversation: conversation, ignore: req.user.username.toLowerCase() });
            // todo: email all the recipients letting them know a new comment is available.
            return;
            
        });
    }
];

exports.updateComment = [
    function(req, res, next) {
        // but are you the owner of the comment?
        Conversation
            .findOne({ _id: req.conversation._id })
            .select({ comments: { $elemMatch: { _id: req.param('commentId')} } })
            .exec(function(err, con) {
                if (err) { return res.respond(400, err); }
                if (!con) { return res.respond(404); }
                if (con.comments.length === 0) { return res.respond(404); }
                var comment = con.comments[0];
                if (req.user.username.toLowerCase() !== comment.user.toLowerCase()) { return res.respond(403); }
                comment.body = req.param('content');
                Conversation
                    .update({ '_id': req.conversation._id, 'comments': { $elemMatch: { _id: req.param('commentId') } } }, { $set: { 'comments.$.body': req.param('content') } }, function(err, doc) {
                        if (err) return next(err);
                        comment.user_avatar = '/avatar/' + comment.user;
                        return res.respond(200, { comment: comment });
                    });
            });
    }
];

exports.destroyComment = [
    function(req, res, next) {
        // but are you the owner of the comment?
        Conversation
            .findOne({ _id: req.conversation._id })
            .select({ comments: { $elemMatch: { _id: req.param('commentId')} } })
            .exec(function(err, con) {
                if (err) { return res.respond(400, err); }
                if (!con) { return res.respond(404); }
                if (con.comments.length === 0) { return res.respond(404); }
                var comment = con.comments[0];
                console.log(comment);
                if (req.user.username.toLowerCase() !== comment.user.toLowerCase()) { return res.respond(403); }
                next();
            });
    },
    function(req, res, next) {
        Conversation.findOneAndUpdate(
            { _id: req.conversation._id },
            { $pull: { comments: { _id: req.param('commentId') } } },
            function(err) {
                if (err) {
                    return res.respond(400, err.message);
                }
                else { return res.respond(200); }
            }
        );
       return res.respond(200);
    }
];

exports.conversation = function(req, res, next, id) {
    Conversation.findById(id, function(err, conversation) {
        if (err) { return next(err); }
        if (!conversation) { return res.respond(404); }
        req.conversation = conversation;
        next();
    });
};

exports.getConversation = [
    canModifyConversation,
    function (req, res, next) {
        var conversation = req.conversation;
        
        async.each(conversation.comments, function(comment, cb) {
            comment.body = decodeURI(comment.body);
            var tolang = req.param('lang', res.getLocale());

            if (typeof comment.lang === 'undefined') {
                comment.lang = 'en';
            }

            comment.user_avatar = '/avatar/' + comment.user;

            translate.fromObject('convo', comment, tolang, 'body', function(obj) {
                comment = obj;
                cb();
            });
        }, function(err) {
            if (err) { return next(err); }
            conversation.last_message = conversation.comments[conversation.comments.length - 1];
            res.respond(200, '', { conversation: conversation });
        });

        // so this is where I must check to see if new_comments_for is present.
        if (conversation.new_comments_for.indexOf(req.user.username.toLowerCase()) !== -1) {
            //console.log('spliced!')
            conversation.new_comments_for.splice(conversation.new_comments_for.indexOf(req.user.username.toLowerCase()),1);
            conversation.save();
        }

        conversation.comments.forEach(function(comment) {
            var commentId = comment._id.toString();
            var user = req.user;
            //console.log('looking for: ' + commentId);
            if (user.new_messages.indexOf(commentId) !== -1) {
                //console.log('oh yeah ' + commentId + ' is here! removing!...')
                user.new_messages.splice(user.new_messages.indexOf(commentId), 1);
                user.save();
            }
        });
    }
];