'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Room = models.model('Room'),
    Discussion = models.model('Discussion'),
    _ = require('lodash'),
    async = require('async');

exports.all = function (req, res, next) {
    Discussion.aggregate({ $match: { deleted: false, tags: { $not: { $size: 1 } } } })
    .unwind('tags')
    .group({ _id: '$tags', tagsCount: { $sum: 1 } })
    .sort('-tagsCount')
    .exec(function(err, results) {
        if (err) { return next(err); }
        res.respond(200, '', { tags: results });
    });
};

exports.user = function (req, res, next) {
    var _ids = [];

    function pushRoom (room, cb) {
        _ids.push(room._id);
        cb();
    }

    Room.find({ subscribers: req.profile._id.toString() }).exec(function (err, rooms) {
        if (err) { return next(err); }
        async.each(rooms, pushRoom, function (err) {
            if (err) { return next(err); }
            if (_ids.length > 0) {
                var params = { deleted: false, tags: { $not: { $size: 1 } }, room: { $in: _ids } };
                Discussion.aggregate({ $match: params })
                .unwind('tags')
                .group({ _id: '$tags', tagsCount: { $sum: 1 } })
                .sort('-tagsCount')
                .exec(function(err, results) {
                    if (err) { return next(err); }
                    res.respond(200, '', { tags: results });
                });
            } else { return res.respond(200, '', { tags: [] }); }
        });
    });
};

exports.discussion = function (req, res, next) {
    var criteria = { tags: req.param('tag') };
    var perPage = 5;
    var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;

    var sorted = '-isSticky -last_comment_date -published -updated';
    if (req.param('sort')) {
        sorted = req.param('sort');
    }

    var options = {
        perPage: perPage,
        page: page,
        criteria: criteria,
        sorted: sorted
    };

    Discussion.list(options, function(err, discussions) {
        if (err) { return next(err); }
        Discussion.count(criteria).exec(function (err, count) {
            res.respond(200, '', {
                discussions: discussions,
                page: page + 1,
                pages: Math.ceil(count / perPage)
            });
        });
    });
};