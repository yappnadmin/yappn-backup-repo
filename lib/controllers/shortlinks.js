'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Shortlink = models.model('Shortlink');

exports.tryAndFindShortlink = function(req, res, next) {
    Shortlink.findByHash(req.params.shortId, function(err, link) {
        if (err || !link) return next();
        res.redirect(link.url);
    });
};

exports.tryAndCreateLink = function (req, res, next) {
    Shortlink.findOne({ url: req.param('originalUrl', '') }, function(err, link) {
        if (err) return res.respond(400, err);
        if (link) return res.respond(201, { url: config.shortlink_host + '/' + link.hash });
        link = new Shortlink({ url: req.param('originalUrl', '') });
        link.save(function(err, link) {
            if (err) return res.respond(400, err);
            res.respond(201, { url: config.shortlink_host + '/' + link.hash });
            return;
        });
    });
};