'use strict';

var config = require('../config/config'),
    redis = require('../config/redis'),
    leaderboard = require('leaderboard');

var board = new leaderboard('leaderboard', null, redis.client);

exports.board = board;