'use strict';

var badwords = { // scored, 1->5 5 being worst case
    'en': [
        { word: 'skank', severity: 1 },
        { word: 'wetback', severity: 1 },
        { word: 'bitch', severity: 1 },
        { word: 'cunt', severity: 9 },
        { word: 'dick', severity: 1 },
        { word: 'douchebag', severity: 4 },
        { word: 'dyke', severity: 3 },
        { word: 'fag', severity: 7 },
        { word: 'nigger', severity: 5 },
        { word: 'tranny', severity: 3 },
        { word: 'trannies', severity: 3 },
        { word: 'paki', severity: 1 },
        { word: 'pussy', severity: 4 },
        { word: 'retard', severity: 2 },
        { word: 'slut', severity: 4 },
        { word: 'titt', severity: 3 },
        { word: 'tits', severity: 3 },
        { word: 'wop', severity: 2 },
        { word: 'whore', severity: 4 },
        { word: 'chink', severity: 9 },
        { word: 'fatass', severity: 2 },
        { word: 'shemale', severity: 1 },
        { word: 'daygo', severity: 1 },
        { word: 'dego', severity: 1 },
        { word: 'dago', severity: 1 },
        { word: 'gook', severity: 4 },
        { word: 'kike', severity: 6 },
        { word: 'kraut', severity: 4 },
        { word: 'spic', severity: 3 },
        { word: 'twat', severity: 9 },
        { word: 'lesbo', severity: 3 },
        { word: 'homo', severity: 3 },
        { word: 'fatso', severity: 1 },
        { word: 'lardass', severity: 1 },
        { word: 'jap', severity: 1 },
        { word: 'biatch', severity: 2 },
        { word: 'tard', severity: 1 },
        { word: 'gimp', severity: 1 },
        { word: 'gyp', severity: 1 },
        { word: 'chinamen', severity: 1 },
        { word: 'golliwog', severity: 1 },
        { word: 'crip', severity: 1 },
        { word: 'fuck', severity: 8 },
        { word: 'fucking', severity: 8 }
    ]
};

exports.threshold = 5;

exports.score = function (string, lang, fn) {

    if (typeof lang === 'undefined') { lang = 'en'; }

    if (typeof string !== 'string') { return fn(0,0,0,[]); }
    
    if (typeof badwords[lang] === 'undefined') {
        // let them through, we cant check it; our fault.
        //app.logger.error('Could not check against language ['+lang+'] as we dont have that setup in the badwords yet.')
        return fn(0,0,0,[]);
    }

    var severity = 0;
    var occurances = 0;
    var weighted = 0;
    var infractions = [];
    for (var i = 0; i < badwords[lang].length; i++) {
        if (string.toLowerCase().indexOf(badwords[lang][i].word) >= 0) {
            var badword = badwords[lang][i];
            occurances++;
            severity += badword.severity;
            infractions.push(badword.word);
        }
    }

    // determine the weight.
    if (occurances > 0) {
        weighted = (severity / occurances);
    }

    return fn(occurances, severity, weighted, infractions);

};