'use strict';

var winston = require('winston'),
    config = require('../config/config');

var customLevels = {
    levels: {
        debug   : 0,
        info    : 1,
        warn    : 2,
        error   : 3
    }
};

var logger = new (winston.Logger)({
    level: config.log_level,
    levels: customLevels.levels,
    transports: [
        new (winston.transports.Console)({
            level: config.log_level,
            silent: process.env.NODE_ENV === 'test' ? true : false,
            colorize: false,
            timestamp: true
        })
    ]
});

module.exports = logger;