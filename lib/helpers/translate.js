'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    TranslationCacheModel = models.model('TranslationCache'),
    crypto = require('crypto'),
    querystring = require('querystring'),
    async = require('async'),
    request = require('request');


function Translator () {
    this.id         = null;
    this.fromLang   = null;
    this.toLang     = null;
    this.text       = '';
    this.domain     = config.translations.services.ortsbo.domain;
    this.cache      = true;
    this.service    = config.translations.service;
    this.url        = '';

}

Translator.prototype.createIdFromText = function (text) {
    var shasum = crypto.createHash('sha1');
    shasum.update(text);
    return shasum.digest('hex');
};

Translator.prototype.prepareTextForTranslation = function(text) {
    return text;
};

Translator.prototype.checkDomain = function() {
    return 'internal';
};

Translator.prototype.checkOrCreateCache = function (id, fn) {
    var that = this;
    TranslationCacheModel.findOne({ hash: id }).exec(function(err, cache) {
        if (err) { return fn(err); }
        if (!cache) {
            cache = new TranslationCacheModel();
            cache.hash = id;
            cache.expires = config.translations.cache.calculateExpirationDate();
            if (that.fromLang) {
                cache.lang = that.fromLang;
                cache.setTranslationFor(that.fromLang, that.text);
            }
            return fn(null, cache);
        } else { return fn(null, cache); }
    });
};

Translator.prototype.prepareUrlForService = function() {
    
    var ServiceObject   = config.translations.services[this.service];
    if (!ServiceObject) { throw new Error('core.translator.service.unsupported'); }

    var ServiceURL      = ServiceObject.url;

    var params = {};

    switch(this.service) {
        case 'google':
            params = { target: this.toLang, q: this.prepareTextForTranslation(this.text), key: ServiceObject.key };
            if (this.fromLang) {
                params.source = this.fromLang;
            }
            break;
        case 'ortsbo':
            params = { tolang: this.toLang, text: this.prepareTextForTranslation(this.text), key: ServiceObject.key, domain: this.domain, nocache: true, nowiki: true };
            if (this.fromLang) {
                params.fromlang = this.fromLang;
            }
            break;
    }

    var sUrl = ServiceURL + '?' + querystring.stringify(params);
    //console.log(sUrl);

    return sUrl;

};

Translator.prototype.errorToObject = function(err) {
    return {
        status: 400,
        statusMessage: err.message,
        fromLang: this.fromLang,
        toLang: this.toLang,
        service: this.service
    };
};

Translator.prototype.validateLang = function (inbound) {
    if (typeof inbound === 'undefined' || !inbound) { return inbound; }

    var outbound = inbound.toLowerCase();

    // China Code [zhs]
    if (outbound === 'zhs' && this.service === 'bing') {
        outbound = 'zh-CHS';
    }
    else if (outbound === 'zhs-chs' && this.service === 'obing') {
        outbound = 'zhs';
    }
    else if (outbound === 'zhs' && this.service === 'google') {
        outbound = 'zh-CN';
    }
    else if (outbound === 'zh-cn' && this.service === 'ogoogle') {
        outbound = 'zhs';
    }

    // China Code [zht]
    if (outbound === 'zht' && this.service === 'bing') {
        outbound = 'zh-CHT';
    }
    else if (outbound === 'zhs-cht' && this.service === 'obing') {
        outbound = 'zht';
    }
    else if (outbound === 'zht' && this.service === 'google') {
        outbound = 'zh-TW';
    }
    else if (outbound === 'zh-tw' && this.service === 'ogoogle') {
        outbound = 'zht';
    }

    return outbound;
};

Translator.prototype.isLanguageSupported = function (lang) {
    if (typeof lang === 'undefined' || !lang) { return false; }
    return config.languages.isLanguageAvailable(lang);
};

Translator.prototype.translate = function(id, from, to, text, cache, callback) {

    if (from === to) {
        return callback(new Error('core.translator.language.identical'));
    }

    /**
     * If the [callback] is passed instead of a [cache] boolean, set cache to true, 
     * and callback to the cache callback passed.
     * --------------------------------------------------------------------------
     */
    if (typeof cache === 'function') {
        callback = cache; // in case they omit the cache boolean
        this.cache = true;
    }

    /**
     * If the [id] is missing because they didn't have one, lets create one using the text that
     * was passed.
     * --------------------------------------------------------------------------
     */
    if (!id) {
        id = this.createIdFromText(text);
    }

    /**
     * If there is a [from] language, lets check to see if it is one of
     * the supported languages.
     * --------------------------------------------------------------------------
     */
    if (from && !this.isLanguageSupported(from)) {
        return callback(new Error('core.translator.language.inbound.unsupported'));
    }

    /**
     * Lets see if the [to] language is supported.
     * --------------------------------------------------------------------------
     */
    if (!this.isLanguageSupported(to)) {
        return callback(new Error('core.translator.language.outbound.unsupported'));
    }

    

    /**
     * We made it passed the [make sure all is there] phase, lets's place them 
     * in the [this] scope and continue.
     */
    this.toLang     = this.validateLang(to, this.service);
    this.fromLang   = this.validateLang(from, this.service);
    this.text       = text;
    this.id         = id;
    this.url        = this.prepareUrlForService();

    // do the translation, and not error then...
    // 
    // var output = this.normalizeOutput(response, service);
    var that = this;
    this.checkOrCreateCache(id, function(err, cache) {
        if (err) { return callback(err); }

        // go do the translation
        var cachedText = cache.getTranslationFor(to);
        if (cachedText) {
            return callback(null, {
                status: 200,
                statusMessage: 'Cached',
                translatedText: cachedText,
                fromLang: cache.lang,
                toLang: to,
                service: 'cache'
            });
        }

        var options = {
            url: that.url,
            headers: {
                'User-Agent': 'Yappn API',
            }
        };

        request(options, function (err, response, body) {

            if (err || response.statusCode === 400) {
                console.log(err || 'translation server is experiencing a problem.  -- ', body);
                return callback(new Error('core.translator.service.error'));
            }

            try {
            body = JSON.parse(body);
                if (body.error) {
                    console.log(body.error);
                    return callback(new Error('core.translator.service.error'));
                }
            } catch (e) {
                return callback(new Error('core.translator.service.error'));
            }

            var obj = {};
            
            if (that.service === 'google') {
                obj = body.data.translations[0];
                obj.timeToTranslate = '';
                if (!that.fromLang) {
                    that.fromLang = obj.detectedSourceLanguage;
                    cache.lang = that.fromLang;
                    cache.setTranslationFor(that.fromLang, that.text);
                }
            } else if (that.service === 'ortsbo') {
                obj = { translatedText: body.translatedText, timeToTranslate: body.time };
            }

            cache.setTranslationFor(that.toLang, obj.translatedText);
            cache.save(function(err, cache) {
                if (err) { return callback(new Error(err.message)); }

                return callback(null, {
                    status: (cache.cached) ? 200 : 201,
                    statusMessage: (cache.cached) ? 'Cached' : 'Created',
                    timeToTranslate: obj.timeToTranslate,
                    translatedText: obj.translatedText,
                    fromLang: that.fromLang,
                    toLang: that.toLang,
                    service: that.service
                });
            });
        });

    });

};

exports.Translator = Translator;

exports.fromObject = function (prefix, obj, tolang, fieldToTranslate, cb) {
    if (typeof obj === 'undefined' || !obj) {
        return cb(obj);
    }

    if (typeof obj.lang === 'undefined' || !obj.lang) {
        obj.lang = 'en';
    }

    if (obj.lang === tolang) {
        return cb(obj);
    }

    if (typeof (fieldToTranslate) === 'object') {
        async.each(fieldToTranslate, function(field, fn) {
            var t = new Translator();
            var fL = obj.lang || 'en';
            t.translate(prefix + '.' + field + '.' + obj._id, fL, tolang, obj[field], true, function(err, response) {
                if (err) { return fn(new Error('core.translator.cache.error')); }
                obj[field] = response.translatedText;
                return fn();
            });
        }, function(err) {
            if (err) { return cb(obj); }
            return cb(obj);
        });
    } else { // ok, it's a string
        var t = new Translator();
        var objKey = obj[fieldToTranslate]; //t.getObjectKey(obj, fieldToTranslate);
        var fL = obj.lang || 'en';
        t.translate(prefix + '.' + fieldToTranslate + '.' + obj._id, fL, tolang, objKey, true, function(err, response) {
            if (err) { return cb(obj); }
            obj[fieldToTranslate] = response.translatedText;
            return cb(obj);
        });
    }
};

exports.deleteById = function (id, cb) {
    TranslationCacheModel.findOne({ hash: id }).exec(function(err, result) {
        if (err || !result) { return cb(); }
        result.remove(cb);
    });
};