'use strict';

var config = require('../config/config'),
    sendgrid = require('sendgrid')(config.sendgrid.user, config.sendgrid.pass),
    fs = require('fs'),
    _ = require('underscore'),
    cachedTemplates = {},
    log = require('./logger');

_.templateSettings = {
    interpolate : /\{\{(.+?)\}\}/g
};

function _load_template(file, props, callback) {

    function buildEmail(template) {
        var temp_file = _.template(template.toString(), props),
            split_i = temp_file.indexOf('\n\n'),
            options = JSON.parse('{' + temp_file.slice(0, split_i) + '}');
        options.html = temp_file.slice(split_i + 1);
        callback(null, options);
    }

    // check cache for template
    if (cachedTemplates[file]) {
        buildEmail(cachedTemplates[file]);
    }
    // load template from file
    else {
        fs.readFile(file, function (err, data) {
            if (!err && data) {
                cachedTemplates[file] = data;
                buildEmail(data);
            } else {
                callback(err);
            }
        });
    }   
}


var default_props = {
    contact_email           : config.email.team_email,
    host                    : config.host,
    generateTextFromHTML    : true,
    team_nick               : config.email.team_nick,
    can_spam                : config.email.can_spam
};

exports.send = function (message, props, fn) {

    fn(); // keep the main thread going.
    
    if (!config.send_mail) {
        console.log('disabled');
        return;
    }

    _load_template(__dirname + '/templates/' + message + '.html', _.extend(props, default_props), function(err, options) {

        if (typeof options.to === 'undefined' || !options.to) {
            log.warn('no email, aborting.');
            return;
        }

        sendgrid.send({
            to: options.to,
            from: config.email.team_email,
            subject: options.subject,
            text: 'Please open this email in a HTML email viewer',
            html: options.html
        }, function(err) {
            if (err) { return console.log(err); }
        });

    });

};