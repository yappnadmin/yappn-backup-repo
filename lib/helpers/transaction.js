'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Transaction = models.model('Transaction'),
    User = models.model('User');

exports.create = function( user, points, coins, data, callback ) {
    
    User.findOneAndUpdate({ _id: user._id }, {
        $inc: {
            coins_balance: (coins * -1),
            points_balance: (points * -1)
        }
    }, { new: true }, function(err, user) {

        if (err) { return callback(err); }

        if (coins > 0) {

            return callback(null, {
                user            : user._id,
                coins           : coins,
                points          : points,
                status          : 'purchase',
                data            : data,
                purchased_at    : new Date()
            });

            // transaction.save(function(err, transaction) {
            //     if (err) return callback(err);
            //     return callback(null, transaction);
            // })

        }
        
    });
};

exports.deposit = function(user, points, coins, reason, data, callback) {
    User.findOne({ _id: user._id })
    .select('points_balance coins_balance username username_idx')
    .exec(function(err, _user_) {
        if (err) { return callback(err); }
        _user_.coins_balance += coins;
        _user_.points_balance += points;
        _user_.save(function(err) {
            if (err) { return callback(err); }

            if (coins > 0) {
                return callback(null, {
                    user            : user._id,
                    coins           : coins,
                    points          : points,
                    status          : 'deposit',
                    reason          : reason,
                    data            : data,
                    purchased_at    : new Date()
                });
            }
        });
    });
};