'use strict';

var faye = require('faye'),
    fayeRedis = require('faye-redis'),
    config = require('../config/config'),
    url = require('url'),
    leaderboard = require('./leaderboard').board;

try {
    var redis_connection = url.parse(config.redis.uri);
    var engineOptions = {
        type: fayeRedis,
        host: redis_connection.hostname,
        port: redis_connection.port,
        password: redis_connection.auth
    };
} catch( e ) {
    console.log('Error pulling redis params from config', config.redis.uri, e);
    return;
}

var fayeServer = new faye.NodeAdapter({
    mount: '/realtime',
    timeout: 45,
    engine: engineOptions
});

exports.realtime = {

    notifyClients: function (method, model, url) {
            
        fayeServer.getClient().publish(url, {
            method: method,
            body: model.toJSON()
        });
        
    },

    pushTo: function(user, message, method) {
        //console.log('in pushTo');
        fayeServer.getClient().publish('/' + user, {
            method: method || 'success',
            body: message
        });
    },

    updateLeaderboard: function () {
        leaderboard.list(function (err, list) {
            if (err) { console.log(err); }
            //console.log('list: ', list);
            fayeServer.getClient().publish('/leaderboard', { leaderboard: list });
        });
    },

    pushUserRank: function (username) {
        leaderboard.rank(username, function (err, rank) {
            if (err) { console.log(err); }
            //console.log(username + '`s rank is: ' + rank);
            fayeServer.getClient().publish('/'+username+'/rank', { rank: rank });
        });
    },

    server: fayeServer

};