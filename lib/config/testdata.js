'use strict';

var config = require('./config'),
  models = require('./models'),
  User = models.model('User'),
  Client = models.model('Client'),
  Profanity = models.model('Profanity'),
  Threshold = models.model('Threshold'),
  AccessToken = models.model('AccessToken'),
  RefreshToken = models.model('RefreshToken'),
  AuthorizationCode = models.model('AuthorizationCode');


// Because we are clearing users and clients all the old tokens are invalid
AccessToken.remove(function () {
  console.log('Cleared AccessTokens');
});
RefreshToken.remove(function () {
  console.log('Cleared RefreshTokens');
});
AuthorizationCode.remove(function () {
  console.log('Cleared AuthorizationCodes');
});

// Clear old users, then add a default admin and test user
User.find({}).remove(function() {
  User.create({
    provider: 'local',
    displayName: 'Admin User',
    email: 'admin@local.host',
    username: 'admin',
    password: 'admin',
    role: 'admin',
    gender: 'male',
    lang: 'en',
    birthdate: new Date('1979-12-31')
  }, {
    provider: 'local',
    displayName: 'Test User',
    email: 'test@test.com',
    username: 'test',
    password: 'test',
    gender: 'male',
    lang: 'en',
    birthdate: new Date('1979-12-31')
  }, function() {
      console.log('finished populating users');
    }
  );
});

Threshold.find({}).remove(function() {
    Threshold.create(
    {
        reference: 'master',
        threshold: 'post-profanity',
        value: 5
    },
    {
        reference: 'master',
        threshold: 'post-flag',
        value: 3
    },
    {
        reference: 'master',
        threshold: 'discussion-flag',
        value: 10
    },
    {
        reference: 'master',
        threshold: 'room-flag',
        value: 10
    },
    {
        reference: 'master',
        threshold: 'user-flag',
        value: 10
    },
    {
        reference: 'master',
        threshold: 'user-flag-permanant',
        value: 20
    },
    {
        reference: 'master',
        threshold: 'popular-days',
        value: 5
    },
    {
        reference: 'master',
        threshold: 'room-tip',
        value: 10
    }, function () {
        console.log('done populating thresholds');
    });
});

Profanity.find({}).remove(function() {
    Profanity.remove().exec();
    Profanity.create(
    {
        reference: 'master',
        lang: 'en',
        word: 'skank',
        severity: 1
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'wetback',
        severity: 1
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'bitch',
        severity: 1
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'cunt',
        severity: 9
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'dick',
        severity: 1
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'douchbag',
        severity: 4
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'dyke',
        severity: 3
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'fag',
        severity: 7
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'nigger',
        severity: 5
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'tranny',
        severity: 3
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'trannies',
        severity: 3
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'paki',
        severity: 1
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'pussy',
        severity: 4
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'retard',
        severity: 2
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'slut',
        severity: 4
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'tits',
        severity: 3
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'whore',
        severity: 4
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'chink',
        severity: 5
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'fatass',
        severity: 2
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'twat',
        severity: 7
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'lesbo',
        severity: 3
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'homo',
        severity: 3
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'fuck',
        severity: 8
    },
    {
        reference: 'master',
        lang: 'en',
        word: 'fucking',
        severity: 8
    }, function (err) {
        console.log('done populating profanity for master');
    });
});

// Clear old clients, then add testing clients and phonegap client
Client.find({}).remove(function () {
  Client.create({
      name: 'CAS Client',
      clientId: 'cas123',
      clientSecret: 'ssh-secret',
      redirectUri: 'http://localhost:9000/askcallback',
      allowedScopes: ['login']
    },
    {
      name: 'Trusted CAS Client',
      clientId: 'cas456',
      clientSecret: 'ssh-othersecret',
      redirectUri: 'http://localhost:9000/callback',
      allowedScopes: ['login'],
      trustedClient: true
    },
    {
      name: 'Samplr2',
      clientId: 'xyz123',
      clientSecret: 'ssh-password'
      // Basic eHl6MTIzOnNzaC1wYXNzd29yZA==
    },
    {
      name: 'Samplr3',
      clientId: 'trustedClient',
      clientSecret: 'ssh-otherpassword',
      trustedClient: true
    },
    {
      name: 'Mobile Application',
      clientId: 'phonegap-angular-client',
      clientSecret: 'ssh-not-secret', // for an installed client this is NOT a secret
      // Basic cGhvbmVnYXAtYW5ndWxhci1jbGllbnQ6c3NoLW5vdC1zZWNyZXQ=
      redirectUri: 'http://localhost' // accepts any port (and unintentionally DNS prefixes)
    }, function() {
    console.log('finished populating clients');
  });
});
