{
    "development": {
        "schema": { "migration": {} },
        "modelName": "Migration",
        "db": "mongodb://localhost:27017/hotspot-dev"
    },
    "staging": {
        "schema": { "migration": {} },
        "modelName": "Migration",
        "db": "mongodb://kburton:s1mpl1c1ty@novus.modulusmongo.net:27017/u4poduwO"
    },
    "production": {
        "schema": { "migration": {} },
        "modelName": "Migration",
        "db": "mongodb://kburton:s1mpl1c1ty@novus.modulusmongo.net:27017/xAriry2r"
    }
}