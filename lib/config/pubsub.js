'use strict';

var EventEmitter2 = require('eventemitter2').EventEmitter2,
    fs = require('fs'),
    config = require('./config');

var CachedActions = [];

var ee = new EventEmitter2({
    wildcard: true,
    delimiter: '.',
    newListener: false,
    maxListeners: 20
});

var actionsDir  = config.root + '/lib/hooks';
fs.readdirSync(actionsDir).forEach(function(file) {
    if (file !== '.DS_Store') {
        var action = file.slice(0, -3);
        var module = require(actionsDir + '/' + action);
        CachedActions[action] = module();
    }
});

ee.on('EVENT.*', function(data) {
    //console.log('\tcalling event: ' + this.event);
    if (typeof CachedActions[this.event] !== 'undefined') {
        var action = CachedActions[this.event];
        action.run(data);
    }
});

exports.ee = ee;