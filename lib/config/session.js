'use strict';

var express = require('express'),
  config = require('./config'),
  session = require('express-session'),
  RedisStore = require('connect-redis')(express),
  redis = require('./redis');

var sessionFn = express.session({
  secret: config.session.secret,
  store: new RedisStore({
    client: redis.client
  })
});

/**
 * Export the typical cookie based connect.session handler
 */
exports.cookieSession = sessionFn;

/**
 * Wrapper for connect.session that expects the sessionID to be passed in as query.session_id
 */
exports.querySession = function (req, res, next) {
  req.cookies['connect.sid'] = req.query.session_id;
  sessionFn(req, res, next);
};