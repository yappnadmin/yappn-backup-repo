'use strict';
/*jshint bitwise: false*/

var express = require('express'),
    path = require('path'),
    config = require('./config'),
    configExpose = require('./config-expose'),
    passport = require('passport'),
    i18n = require('i18n'),
    redis = require('./redis').client,
    online = require('online'),
    session = require('./session'),
    fs = require('fs');

module.exports = function(app) {

    var locales = [];
    for (var l = 0; l < config.languages.langs.length; l++) {
        locales.push(config.languages.langs[l].code);
    }

    i18n.configure({
        locales: locales,
        defaultLocale: 'en',
        cookie: 'YpUserLang',
        directory: __dirname + '/locales',
        updateFiles: false
    });

    app.configure('development', function() {
        app.use(require('connect-livereload')());

        // Disable caching of scripts for easier testing
        app.use(function noCache(req, res, next) {
            if (req.url.indexOf('/scripts/') === 0) {
                res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
                res.header('Pragma', 'no-cache');
                res.header('Expires', 0);
            }
            next();
        });

        configExpose.version = JSON.parse(fs.readFileSync(path.join(config.root, 'app', 'version.json')));
        app.use(express.static(path.join(config.root, '.tmp')));
        app.use(express.static(path.join(config.root, 'app')));
        app.set('views', config.root + '/app/views');
    });

    app.configure('test', function() { // for pre-build integration testing
        configExpose.version = JSON.parse(fs.readFileSync(path.join(config.root, 'app', 'version.json')));
        app.use(express.static(path.join(config.root, '.tmp')));
        app.use(express.static(path.join(config.root, 'app')));
        app.set('views', config.root + '/app/views');
    });

    app.configure('production', function() {
        app.use(express.favicon(path.join(config.root, 'public', 'favicon.ico')));
        configExpose.version = JSON.parse(fs.readFileSync(path.join(config.root, 'public', 'version.json')));
        app.use(express.static(path.join(config.root, 'public'), { maxAge: 86400000 }));
        app.set('views', config.root + '/views');
    });

    app.configure('staging', function() {
        app.use(express.favicon(path.join(config.root, 'public', 'favicon.ico')));
        configExpose.version = JSON.parse(fs.readFileSync(path.join(config.root, 'public', 'version.json')));
        app.use(express.static(path.join(config.root, 'public')));
        app.set('views', config.root + '/views');
    });

    app.configure(function() {
        app.engine('html', require('ejs').renderFile);
        app.set('view engine', 'html');
        // app.use(express.bodyParser()); // bodyParser is unsafe: https://groups.google.com/forum/#!topic/express-js/iP2VyhkypHo

        // add the sdk static routes
        app.use('/sdk', express.static(path.join(config.root, 'lib/sdk'), { maxAge: 86400000 }));

        app.use(express.compress());
        app.use(express.logger('dev'));
        app.use(express.urlencoded());
        app.use(express.json());
        app.use(express.methodOverride());

        //cookieParser should be above session
        app.use(express.cookieParser());

        // Persist sessions with mongoStore
        // Persist sessions with mongoStore
        app.use(session.cookieSession);

        // online && locale middleware
        online = online(redis);
        app.use(i18n.init);

        //use passport session
        app.use(passport.initialize());
        //app.use(passport.session()); //-- don't do this here! see security-policy.js

        

        app.use(function(req, res, next) {

            res.locals.metadata = {
                url: config.host,
                title: 'Yappn',
                description: 'An international meeting place where people choose and chat about their favorite topics. Regardless of languages of the people you are chatting with, everything they write is displayed in your language, and what you write is displayed in their language.',
            };

            // if the language is in the request, use it.
            var lang = req.headers['X-Yappn-Language'] || req.param('lang', res.getLocale());
            if (lang) {
                if (!config.languages.isLanguageAvailable(lang)) {
                    lang = 'en';
                }
                res.setLocale(lang);
            }
            next();
        });
        app.use(require('./response')(app));

        // Router (only error handlers should come after this)
        app.use(app.router);

        // Error handler
        app.configure('development', function() {
            app.use(express.errorHandler());
        });
    });
};