'use strict';

var redis = require('redis'),
    url = require('url'),
    config = require('./config');

try {
    var redis_connection = url.parse(config.redis.uri);
    config = {
        host: redis_connection.hostname,
        port: redis_connection.port,
        auth: redis_connection.auth
    };
} catch( e ) {
    console.log('Error pulling redis params from config', config.redis.uri, e);
    return;
}

var client = redis.createClient(config.port, config.host);
if (config.auth) {
    client.auth(config.auth);
}

exports.client = client;