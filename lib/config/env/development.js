module.exports = {
    env: 'development',
    mongo: {
        uri: 'mongodb://localhost/hotspot-dev'
    },
    redis: { // redis://redistogo:f466d99ecf34b9dd17968524985d0f21@tarpon.redistogo.com:9849/
        uri: 'redis://localhost'
    },
    host: 'http://localhost:9000'
};