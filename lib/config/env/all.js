'use strict';

var _ = require('lodash'),
    path = require('path');

var rootPath = path.normalize(__dirname + '/../../..');

/**
 * Helper to parse credentials or return null.
 * If credentials exist then options are merged into the result.
 * @param auth string of the form username:password
 * @param options additional parameters such as desired scope options
 */
function parseProviderAuth(auth, options) {
    if (!auth) return null;
    var parts = auth.split(':');
    return _.merge({
        clientID: parts[0],
        clientSecret: parts[1]
    }, options);
}

module.exports = {

    /**
     * Site specific
     */

    port: process.env.PORT || 9000,
    root: rootPath,
    request_timeout: 10000,
    host: 'http://localhost:9000',
    shortlink_host: 'http://yap.pn',

    /**
     * Redis Configuration
     */
    redis: {
        debug: false,
        options: {
            // no_ready_check: true
        }
    },

    /**
     * Mongo Configuration
     */

    mongo: {
        debug: false
    },

    /**
     * Log Level
     * debug, warn, info, error
     */
    log_level: 'error',

    // third-party logins.
    thirdParty: {
        facebook: parseProviderAuth(process.env.AUTH_FACEBOOK, {}),
        twitter: parseProviderAuth(process.env.AUTH_TWITTER, {}),
    },

    send_mail: false,

    email: {
        team_email: 'no-reply@yappn.com',
        team_nick: 'The Yappn Team',
        debug: true,
        can_spam: 'This message was send by Yappn Corp., 1001 Avenue of the Americas, 11th Floor New York, NY 10018, 1-800-395-9943',
        smtp: {
            host: 'svr1.itibitiphone.com',
            port: 465,
            user: 'no-reply@yappn.com',
            pass: 'n2013!'
        }
    },

    sendgrid: {
        user: 'yappn',
        pass: '1heartmesomeyappn'
    },

    settings: {
        // rooms
        min_cost_suggest_room: 20,
        max_cost_suggest_room: 50,
        //room_tip_threshold: 10,
        room_cost_to_tip: 130,

        // endorse
        min_cost_endorse_room: 5,
        max_cost_endorse_room: 10,

        // comment_flag_threshold: 3,
        // discussion_flag_threshold: 10,
        // room_flag_threshold: 10,
        // user_flag_threshold: 10,
        // user_flag_threshold_perm: 20,
        timeToCheckBannedUsers: 43200, // every 12 hours

        name: 'Yappn Platform',
        default_public_user_fields: 'username username_idx displayName active verified id gender coins_balance points_balance image roles following followers',
        embedded_user_fields: 'username username_idx roles',

        //popular_days_threshold: 5
    },

    api_host: 'https://platform.yappn.com',
    www_host: 'http://yappn.com',

    /**
     * OAuth 2.0 Settings
     */
    oauth: {
        /**
         * Configuration of access tokens.
         *
         * expiresIn - The time in seconds before the access token expires
         * expiresInForApps - The time in seconds before the application access token expires
         * calculateExpirationDate - A simple function to calculate the absolute
         * calculateExpirationDateForAppTokens - A simple function to calculate the absolute for Apps
         * time that th token is going to expire in.
         * authorizationCodeLength - The length of the authorization code
         * accessTokenLength - The length of the access token
         * refreshTokenLength - The length of the refresh token
         */
        token: {
            expiresIn: 3600, // 2 days
            expiresInForApps: 31536000, // a years worth of seconds
            calculateExpirationDate: function() {
                return new Date(new Date().getTime() + (this.expiresIn * 1000));
            },
            calculateExpirationDateForAppTokens: function() {
                return new Date(new Date().getTime() + (this.expiresInForApps * 1000));
            },
            authorizationCodeLength: 16,
            accessTokenLength: 256,
            refreshTokenLength: 256
        },

        /**
         * Database configuration for access and refresh tokens.
         *
         * timeToCheckExpiredTokens - The time in seconds to check the database
         * for expired access tokens.  For example, if it's set to 3600, then that's
         * one hour to check for expired access tokens.
         */
        db: {
            timeToCheckExpiredTokens: 3600
        }
    },

    blacklist: {
        expiresIn: 31536000,
        calculateExpirationDate: function() {
            return new Date(new Date().getTime() + (this.expiresIn * 1000));
        }
    },

    /**
     * Session configuration
     *
     * type - The type of session to use.  MemoryStore for 'in-memory',
     * or MonoStore for the mongo database store
     * maxAge - The maximum age in milliseconds of the session.  Use null for
     * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
     * for a year.
     * secret - The session secret that you should change to what you want
     * dbName - The database name if you're using Mongo
     */
    session: {
        maxAge: 3600000 * 24 * 7 * 52,
        secret: process.env.SESSION_SECRET || '7cs6gdc76b23eb2jydgubgdb8723ebhjbjw',
        collection: 'sessions'
    },

    languages: {
        langs: [{
                'code': 'af',
                'name': 'Afrikaans'
            }, // done
            {
                'code': 'sq',
                'name': 'Albanian'
            }, // done
            {
                'code': 'ar',
                'name': 'Arabic'
            }, // done
            {
                'code': 'hy',
                'name': 'Armenian'
            }, // done
            {
                'code': 'az',
                'name': 'Azerbaijani'
            }, // done
            {
                'code': 'eu',
                'name': 'Basque'
            }, // done
            {
                'code': 'be',
                'name': 'Belarusian'
            }, // done
            {
                'code': 'bn',
                'name': 'Bengali'
            }, // done 
            {
                'code': 'bg',
                'name': 'Bulgarian'
            }, // done
            {
                'code': 'ca',
                'name': 'Catalan'
            }, // done
            {
                'code': 'zhs',
                'name': 'Chinese (Simplified)'
            }, // done
            {
                'code': 'zht',
                'name': 'Chinese (Traditional)'
            }, // done
            {
                'code': 'hr',
                'name': 'Croatian'
            }, // done
            {
                'code': 'cs',
                'name': 'Czech'
            }, // done
            {
                'code': 'da',
                'name': 'Danish'
            }, // done
            {
                'code': 'nl',
                'name': 'Dutch'
            }, // done
            {
                'code': 'en',
                'name': 'English'
            }, // done
            {
                'code': 'eo',
                'name': 'Esperanto'
            }, // done
            {
                'code': 'et',
                'name': 'Estonian'
            }, // done
            {
                'code': 'tl',
                'name': 'Filipino'
            }, // done
            {
                'code': 'fi',
                'name': 'Finnish'
            }, // done
            {
                'code': 'fr',
                'name': 'French'
            }, // done
            {
                'code': 'gl',
                'name': 'Galician'
            }, // done
            {
                'code': 'ka',
                'name': 'Georgian'
            }, // done
            {
                'code': 'de',
                'name': 'German'
            }, // done
            {
                'code': 'el',
                'name': 'Greek'
            }, // done
            {
                'code': 'gu',
                'name': 'Gujarati'
            }, // done
            {
                'code': 'ht',
                'name': 'Haitian Creole'
            }, // done
            {
                'code': 'he',
                'name': 'Hebrew'
            }, // done
            {
                'code': 'hi',
                'name': 'Hindi'
            }, // done
            {
                'code': 'mww',
                'name': 'Hmong Daw'
            }, // error
            {
                'code': 'hu',
                'name': 'Hungarian'
            }, // done
            {
                'code': 'is',
                'name': 'Icelandic'
            }, // done
            {
                'code': 'id',
                'name': 'Indonesian'
            }, // done
            {
                'code': 'ga',
                'name': 'Irish'
            }, // done
            {
                'code': 'it',
                'name': 'Italian'
            }, // done
            {
                'code': 'ja',
                'name': 'Japanese'
            }, // done
            {
                'code': 'kn',
                'name': 'Kannada'
            }, // done
            {
                'code': 'ko',
                'name': 'Korean'
            }, // done
            {
                'code': 'lo',
                'name': 'Lao'
            }, // done
            {
                'code': 'la',
                'name': 'Latin'
            }, // done
            {
                'code': 'lv',
                'name': 'Latvian'
            }, // done
            {
                'code': 'lt',
                'name': 'Lithuanian'
            }, // error
            {
                'code': 'mk',
                'name': 'Macedonian'
            }, // done
            {
                'code': 'ms',
                'name': 'Malay'
            }, // done
            {
                'code': 'mt',
                'name': 'Maltese'
            }, // done
            {
                'code': 'no',
                'name': 'Norwegian'
            }, // error
            {
                'code': 'fa',
                'name': 'Persian'
            }, // done
            {
                'code': 'pl',
                'name': 'Polish'
            }, // done
            {
                'code': 'pt',
                'name': 'Portuguese'
            }, // done
            {
                'code': 'ro',
                'name': 'Romanian'
            }, // done
            {
                'code': 'ru',
                'name': 'Russian'
            }, // done
            {
                'code': 'sr',
                'name': 'Serbian'
            }, // done
            {
                'code': 'sk',
                'name': 'Slovak'
            }, // done
            {
                'code': 'sl',
                'name': 'Slovenian'
            }, // done
            {
                'code': 'es',
                'name': 'Spanish'
            }, // done
            {
                'code': 'sw',
                'name': 'Swahili'
            }, // done
            {
                'code': 'sv',
                'name': 'Swedish'
            }, // done
            {
                'code': 'ta',
                'name': 'Tamil'
            }, // done
            {
                'code': 'te',
                'name': 'Telugu'
            }, // done
            {
                'code': 'th',
                'name': 'Thai'
            }, // done
            {
                'code': 'tr',
                'name': 'Turkish'
            }, // done
            {
                'code': 'uk',
                'name': 'Ukrainian'
            }, // done
            {
                'code': 'ur',
                'name': 'Urdu'
            }, // done
            {
                'code': 'vi',
                'name': 'Vietnamese'
            }, // done
            {
                'code': 'cy',
                'name': 'Welsh'
            }, // done
            {
                'code': 'yi',
                'name': 'Yiddish'
            } // done
        ],
        isLanguageAvailable: function(incoming) {
            var out = false;
            this.langs.forEach(function(item) {
                if (item.code === incoming) {
                    out = true;
                }
            });
            return out;
        }

    },


    twitter: {
        consumer_key: 'HFLiOm8kv2Rl0hl2rn1ww',
        consumer_secret: 'wl6ObCx56UvrvnwquP5appfK5aHiM3BWA6T5jUpKk',
        access_token: '14946112-9K79xfPNT3btHyaGQinwO7T7UlfLYGLBfQJbDZPEI',
        access_token_secret: '8nSlQ7KGoWYQYzlHgdwbW9bV3gbDcw5oPxUQ4zZumKQ'
    },

    room: {

        defaults: {

            displayName: 'General Yappn Chat',
            content: 'When discussions have no home, we display them to the world.',
            endorsed: true,
            lang: 'en',
        
        }

    },

    translations: {
        cache: {
            expiresIn: 172800,
            calculateExpirationDate: function() {
                return new Date(new Date().getTime() + (this.expiresIn * 1000));
            }
        },
        db: {
            timeToCheckExpiredCache: 43200
        },
        options: {
            cache: true,
            wiki: false
        },
        pre: ['en', 'fr', 'es'],
        services: {
            'google': {
                url: 'https://www.googleapis.com/language/translate/v2',
                key: 'AIzaSyDO8gfatEheVxiBN8DCchHBDyYWQNyK34U'
            },
            'ortsbo': {
                url: 'http://webtranslator.cloudapp.net/OWTS/translate',
                key: '27F6-14C4-7FE6-4B99-9B2B-EE15-A0B4-FCC2',
                domain: 'yappn.com'
            }
        },
        service: 'ortsbo'
    },

    /**
     * Configuring CORS
     * See https://npmjs.org/package/cors
     *
     * TODO: Consider dynamically allowing the hosts in models/client.redirectUrl
     *
     * Note phonegap-angular-client doesn't use CORS when running in the emulator.
     */
    corsOptions: {
        origin: 'http://localhost:9001' // enable phonegap-angular-client 'grunt serve' debugging
    },

    /**
     * Configuration of rate limits on API endpoints.
     * Unspecified values use defaults from https://github.com/visionmedia/node-ratelimiter
     */
    rateLimit: {
        byAnyoneTest: { // used for testing rate limiter. 1 req/5 secs
            max: 3,
            duration: 5000
        },
        byLogin: { // protects authentication routes against distributed credential guessing attacks
            max: 1000, // Note: these current settings were pulled out of thin air!
            duration: 60000
        },
        byAnyone: { // protects user creation against resource consumption
            max: 2000, // Note: these current settings were pulled out of thin air!
            duration: 60000
        },
        byUser: {},
        byIP: {}
    },

    /**
     * Configuring application public DNS URL
     * Usually this is the load balancer or reverse proxy where SSL is terminated for this app.
     * This typically has the form: "https://<host>[/<root path>]"
     *
     * TODO: when not explicitly provided determine this dynamically using reverse DNS
     */
    rootUrl: process.env.ROOT_URL || 'http://localhost:9000'


};