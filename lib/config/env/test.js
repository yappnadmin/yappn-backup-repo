module.exports = {
    env: 'test',
    mongo: {
        uri: 'mongodb://localhost/hotspot-test'
    },
    redis: {
        uri: 'redis://localhost'
    },
    log_level: 'error',
    send_mail: false,
    host: 'http://localhost:9000'
};