'use strict';

module.exports  = function(app) {
    
    return function (req, res, next) {

        var start = new Date();

        res.respond = function(code, message, data) {

            if (typeof message === 'object' && typeof data === 'undefined') {
                data = message;
                message = null;
            }

            if (typeof data === 'undefined') { data = {}; }
            if (typeof message === 'undefined') { message = ''; }

            var response = {
                code        : code,
                message     : message,
                data        : data,
                meta        : {
                    endpoint: req._parsedUrl.pathname,
                    environment: app.get('env'),
                    time: (new Date() - start) + 'ms',
                    method: req.method,
                    lang: res.getLocale()
                }
            };

            if (req.isAuthenticated()) {
                response.meta.user = req.user.username_idx;
            }

            if (code >= 400 && code < 600) { // it was an error!
                response.error = message;
                delete response.message;
                //delete response.data;
            }

            res.send(code, response);
            
        };
        
        next();

    };

};