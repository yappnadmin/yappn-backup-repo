'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity');

module.exports = function() {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined') {
				console.log('An action required a user object');
				return;
			}

            if (data.user.provider === 'local') {
                emailer.send('user_register', {
                    name: data.user.displayName,
                    to: data.user.email,
                    activation_code: data.user.activation_code,
                    querystring: 'code='+encodeURIComponent(data.user.activation_code)+'&email='+encodeURIComponent(data.user.email)
                }, function(err) {
                    if (err) { console.log('An error occured sending that email', err); }
                });
            }
		}
	};
};