'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer'),
    ee = require('../config/pubsub').ee;

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined') {
				console.log('An action required a user object');
				return;
			}

			transaction.deposit(data.user, 0, 1, 'USER_LOGIN', {
				user: data.user
			}, function(err) {
				if (err) { console.log(err); }
			});

			if (config.is_beta_program_active) {
				console.log('BADGE.BETA_USER hit...');
				ee.emit('BADGE.BETA_USER', { user: data.user } );
			}
		}
	};
};