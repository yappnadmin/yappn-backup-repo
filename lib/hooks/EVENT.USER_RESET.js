'use strict';

var emailer = require('../helpers/emailer');

module.exports = function() {

    return {

        run: function (data) {

            console.log('ee user_reset initialized...');

            if (typeof data.user === 'undefined') {
                console.log('An action required a user object');
                return;
            }

            var user = data.user;
            var newPassword = data.new_password;

            emailer.send('password_reset', {
                name: user.username,
                to: user.email,
                username: user.username,
                password: newPassword,
                querystring: "password_reset=1&email="+encodeURIComponent(user.email)
            }, function (err) {
                if (err) {
                    if (err) { console.log('An error occured sending that email', err); }
                }
            });

        }

    };

};