'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity'),
    async = require('async'),
    emailer = require('../helpers/emailer');

module.exports = function () {
    return {
        run: function(data) {
            if (typeof data.user === 'undefined') {
                console.log('An action required a user object');
                return;
            }
            Comment.find({ author: data.user._id }).exec(function(err, comments) {
                if (err) { return console.log(err); }

                async.each(comments, function(comment, cb) {
                    comment.remove(cb);
                }, function(err) {

                    if (err) { return console.log(err); }
                    Activity.find({ actor: data.user.username.toLowerCase() }).exec(function (err, activities) {
                        if (err) { console.log(err); return; }
                        async.each(activities, function (act, cb) {
                            act.remove(cb);
                        }, function(err) {
                            if (err) { return console.log(err); }

                            // yup. lets send the email.
                            if (data.user.email) {

                                emailer.send('banned_by_peers', {
                                    username: data.user.username,
                                    to: data.user.email,
                                    bannedUntil: data.user.bannedUntil
                                }, function(err) {
                                    if (err) { return console.log(err); }
                                });

                            } // cannot send to someone without an email.

                        });
                    });
                });
            });
        }
    };
};