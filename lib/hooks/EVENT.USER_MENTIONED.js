'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity');

module.exports = function () {

    return {

        run: function(data) {
            // data = { user: req.user, discussion: req.param('referenceTo'), comment: comment, mention: username }
            
            if (typeof data.user === 'undefined' || typeof data.mention === 'undefined') {
                return console.log('User, or Mention missing');
            }
            
            var url = config.host + '/discussion/'+ data.discussion;
            var statement = '<a href="'+config.host+'/people/'+data.user.username_idx+'">@'+data.user.username+'</a> mentioned you in their post on <a href="'+url+'">'+data.discussion.displayName+'</a>';
            
            User.findOne({ username_idx: data.mention.toLowerCase(), deleted: false }).exec(function(err, person) {
                if (err) { return console.log(err); }
                if (!person) { return console.log(new Error('Could not send notification, user does not exist')); }
                Notification.broadcast(person, 'mention', statement, function() {});
            });

        }

    };

};