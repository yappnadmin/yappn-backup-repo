'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Discussion = models.model('Discussion');

module.exports = function () {
    return {
        run: function(data) {
            if (typeof data.discussion === 'undefined') {
                return console.log('requires discussion object');
            }

            Discussion.findByIdAndUpdate(data.discussion._id, { $inc: { views: 1 } }, function(err) {
                if (err) { console.log('EVENT.DISCUSSION_VIEW:Error ', err); }
            });
        }
    };
};