'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment');

module.exports = function () {

	return {

		run: function(data) {
			if (typeof data.user === 'undefined') {
				console.log('An action required a user object');
				return;
			}

			// POST
		  transaction.deposit(data.user, 25, 0, 'POST', data, function(err) {

				if (err) { console.log(err); }
                History.capture({
                    user: data.user._id,
                    comment: data.comment._id,
                    history: 'post'
                }, function() {});
			});

            // UPDATE USERS POST COUNT
            User.findByIdAndUpdate(data.user._id, { $inc: { post_count: 1 } }, function (err) {
                if (err) { console.log(err); }
                return;
            });

			var comment = data.comment;


			if (comment.reference === 'discussions') {
                var doc = data.discussion;
                doc.comments = (doc.comments + 1);
                doc.save(function(err) {
                    if (err) { console.log(err); }
                    var tags = _.union(
                        data.user.feed,
                        doc.tags
                    );

                    User.findByIdAndUpdate(data.user._id, { feed: tags }, function( err ) {
                        if (err) { console.log(err); }
                        return;
                    });

                });
			}

            // go out and send notifications
            // 
            var url = config.host + '/discussion/'+comment.referenceTo;
            var statement = '<a href="'+config.host+'"/people/'+data.user.username_idx+'">@'+data.user.username+'</a> posted in <a href="'+url+'">'+data.discussion.displayName+'</a><br><p class="notif-small">'+comment.content+'</p>';

            // 
            Comment
                .find({ referenceTo: data.discussion._id })
                .distinct('author')
                .exec(function(err, participants) {
                    if (err) { return console.log(err); }
                    async.each(participants, function(part, cb) {
                        User.findOne({ _id: part })
                        .select('username username_idx email notifications provider active banned')
                        .exec(function(err, recipient) {
                            if (err) { return cb(err); }
                            if (!recipient) { return cb(new Error('could find user: ' + part)); }
                            if (!recipient.active) { return cb(); }
                            if (recipient.username === data.user.username) { return cb(); }
                            //console.log('sending notification to ', recipient.email);
                            Notification.broadcast(recipient, 'discussion', statement, function() {
                                cb();
                            });
                        });
                    }, function(err) {
                        if (err) { return console.log(err); }
                    });
                });
		}

	};

};