'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined' || typeof data.reaction === 'undefined' || typeof data.doc === 'undefined') {
				console.log('User, Reference or Reaction missing');
				return;
			}
			Activity.publish(data.user, data.reaction, data.doc.toSafeObject(), false, false, false, function(err) {
                if (err) { return console.log(err); }
            });
		}
	};
};