'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.room === 'undefined') {
				console.log('An action required a room object');
				return;
			}

			var room = data.room;

			// get the creator =>
			var creator = room.author;
			User.findOne({ _id: creator }, function(err, user) {
				if (err) { return; }
				transaction.deposit(user, 500, 20, 'CREATOR_ROOM_TIPPED', data, function(err) {
					if (err) { return console.log(err); }
					
					var founders = room.founders;

					User.find({ _id: { $in: founders }}, function(err, users) {
						users.forEach(function(user) {
							transaction.deposit(user, 75, 5, 'FOUNDER_ROOM_TIPPED', data, function(err) {
								if (err) { return console.log(err); }
							});
						});
					});
				});
			});
		}
	};
};