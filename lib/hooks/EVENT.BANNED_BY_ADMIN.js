'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity'),
    async = require('async'),
    emailer = require('../helpers/emailer');

module.exports = function () {
    return {
        run: function(data) {
            if (typeof data.user === 'undefined') {
                console.log('An action required a user object');
                return;
            }

            Comment.find({ author: data.user._id }).exec(function(err, comments) {
                if (err) {
                    console.log(err);
                    return;
                }

                async.each(comments, function(comment, cb) {
                    comment.remove(cb);
                }, function(err) {
                    if (err) { console.log('An error occured sending that email', err); }
                  
                    Activity.find({ actor: data.user.username.toLowerCase() }).exec(function (err, activities) {
                        if (err) { console.log(err); return; }
                        async.each(activities, function (act, cb) {
                            act.remove(cb);
                        }, function(err) {
                            if (err) { console.log('An error occured sending that email', err); }
                            // yup. lets send the email.
                            if (data.user.email) {

                                emailer.send('banned_by_admin', {
                                    username: data.user.username,
                                    to: data.user.email
                                }, function(err) {
                                    if (err) { console.log('An error occured sending that email', err); }
                                });

                            } // cannot send to someone without an email.

                        });
                    });
                });
            });
        }
    };
};