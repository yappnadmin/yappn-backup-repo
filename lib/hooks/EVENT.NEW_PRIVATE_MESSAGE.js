'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment');

module.exports = function () {
    return {
        run: function(data) {
            if (typeof data.user === 'undefined') {
                console.log('An action required a user object');
                return;
            }
            async.each(data.conversation.recipients, function(recipient, cb) {
                User.findOne({ username_idx: recipient.toLowerCase(), deleted: false }).exec(function(err, person) {
                    if (err) { return cb(); }
                    Notification.broadcast(person, 'conversation', 'New private message from ' + data.user.displayName, function() {
                        cb();
                    });
                });
            }, function (err) {
                if (err) { return console.log(err); }
            });
        }
    };
};