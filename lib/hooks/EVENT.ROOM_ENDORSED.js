'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined') {
				console.log('An action required a user object');
				return;
			}

			//(user, points, coins, reason, data, callback)
			// Founder vote on a room
			transaction.deposit(data.user, 50, 20, 'ROOM_ENDORSED', data, function(err) {
				if (err) { console.log(err); }
			});
		}
	};
};