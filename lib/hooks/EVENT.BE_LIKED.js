'use strict';

var transaction = require('../helpers/transaction');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined') {
				return console.log('An action required a user object');
			}

			// POST
			transaction.deposit(data.user, 150, 2, 'BE_LIKED', data, function(err) {
				if (err) { console.log(err); }
			});
		}
	};
};