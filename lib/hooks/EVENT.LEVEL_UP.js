'use strict';

module.exports = function () {

	return {
		run: function(data) {

			if (typeof data.user === 'undefined') {
				return console.log('An action required a user object');
			}
		}
	};
};