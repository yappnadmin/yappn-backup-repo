'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity');

module.exports = function () {

    return {

        run: function(data) {

            if (typeof data.user === 'undefined') {
                console.log('An action required a user object');
                return;
            }

            //console.log('what the hell?')

            // POST
            transaction.deposit(data.user, 5, 0, 'VISIT_ROOM', data, function(err) {
                if (err) { console.log(err); }
            });
        }
    };
};