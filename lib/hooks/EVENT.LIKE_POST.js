'use strict';

var transaction = require('../helpers/transaction');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined') {
				console.log('An action required a user object');
				return;
			}

			// POST
			transaction.deposit(data.user, 10, 0, 'LIKE_POST', data, function(err) {
				if (err) { console.log(err); }
			});
		}
	};
};