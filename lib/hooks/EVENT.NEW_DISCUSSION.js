'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined') {
				console.log('An action required a user object');
				return;
			}

			// POST
			transaction.deposit(data.user, 25, 0, 'NEW_DISCUSSION', data, function(err) {
				if (err) { console.log(err); }
                History.capture({
                    user: data.user._id,
                    discussion: data.discussion._id,
                    history: 'post'
                }, function() {});
			});
		}
	};
};