'use strict';

var config = require('../config/config'),
    models = require('../config/models'),
    _ = require('lodash'),
    async = require('async'),
    transaction = require('../helpers/transaction'),
    log = require('../helpers/logger'),
    emailer = require('../helpers/emailer');

var History = models.model('History'),
    Notification = models.model('Notification'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    Comment = models.model('Comment'),
    Activity = models.model('Activity');

module.exports = function () {

	return {

		run: function(data) {

			if (typeof data.user === 'undefined' || typeof data.person === 'undefined') {
				console.log('User or Person objects missing');
				return;
			}
            
			Activity.publish(data.user, 'Follows', data.person, false, ' is following... ', true, function(err) {
                if (err) { return console.log(err); }
				transaction.deposit(data.user, 10, 0, 'FOLLOW', { user: data.person	}, function(err) {
					if (err) { return console.log(err); }
					Activity.publish(data.person, 'Followed', data.user, false, ' are being followed by...', true, function(err) {
                        if (err) { return console.log(err); }
                        transaction.deposit(data.person, 40, 5, 'BEING_FOLLOWED', { user: data.user }, function(err) {
                            if (err) { return console.log(err); }
                            User.findOne({ username_idx: data.person.username.toLowerCase(), deleted: false }).exec(function(err, person) {
                                if (err) { return console.log(err); }
                                Notification.broadcast(person, 'follower', '@' + data.user.username + ' is now following you.', function() {
                                    // do nothing
                                    return;
                                });
                            });
                        });
                    });
				});
            });
		}

	};

};