'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,
    crypto = require('crypto'),
    async = require('async'),
    leaderboard = require('../helpers/leaderboard').board;

var authTypes = ['github','twitter','facebook','google'];

var getTags = function (tags) {
    return tags.join(',');
};

var setTags = function (tags) {

    if (typeof tags === 'undefined' || !tags) {
        return [];
    }

    if (typeof tags === 'object') {
        return tags;
    }
    
    return tags.split(',');
};

var UserSchema = new Schema({
    displayName: { type: String, default: '', trim: true },
    username: { type: String, default: '', trim: true },
    username_idx: {
        type: String,
        lowercase: true,
        trim: true,
        index: true
    },
    email: { type: String, default: '', trim: true, index: true, lowercase: true },
    role: {
        type: String,
        default: 'user'
    },
    groups: [String],
    hashed_password: String,
    provider: { type: String, default: 'local' },
    salt: String,
    'facebook'          : {},
    'twitter'           : {},
    'github'            : {},
    'windows'           : {},
    'foursquare'        : {},
    'linkedin'          : {},
    'google'            : {},

    // extra information
    gender: { type: String, enum: ['male','female'], default: 'male' },
    lang: { type: String, default: 'en' },
    birthdate: { type: Date, default: '' },
    image: {
        type: { duration: Number, height: Number, width: Number, url: String },
        default: null
    },
    icon: {
        type: { duration: Number, height: Number, width: Number, url: String },
        default: null
    },
    thumbnail: {
        type: { duration: Number, height: Number, width: Number, url: String },
        default: null
    },
    summary: String,
    content: String,
    url: String,
    objectType: { type: String, default: 'person' },
    location: {
        displayName: {type: String},
        position: {
            latitude: Number,
            longitude: Number
        },
        ipaddress: { type: String }
    },
    activation_code: String,
    invitation: String,

    // status
    active: { type: Boolean, default: true },
    deleted: { type: Boolean, default: false },
    verified: { type: Boolean, default: false },
    banned: { type: Boolean, default: false },
    bannedUntil: Date,

    // gamification
    points_balance: { type: Number, default: 0 },
    coins_balance: { type: Number, default: 0 },
    level: { type: Number, default: 1 },
    badges: [],
    followers: { type: Array, default: [] },
    following: { type: Array, default: [] },

    // reactions
    like: { type: Array, default: [] },
    lol: { type: Array, default: [] },
    flag: { type: Array, default: [] },

    // notifications
    notifications: {
        'mention': { type: Boolean, default: true },
        'badge': { type: Boolean, default: true },
        'conversation': { type: Boolean, default: true },
        'discussion': { type: Boolean, default: true },
        'follower': { type: Boolean, default: true },
        'newsletter': { type: Boolean, default: true }
    },

    login_count: { type: Number, default: 0 },
    post_count: { type: Number, default: 0 },
    new_messages: { type: Array, default: [] },
    plan: { type: String, enum: ['user','business','fanhub','nonprofit','celebrity'], default: 'user' },
    feed: { type: Array, default: [], lowercase: true }
    
}, { collection: 'users' });

/**
 * Virtuals
 */
UserSchema
    .virtual('password')
    .set(function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashed_password = this.encryptPassword(password);
    })
    .get(function() {
        return this._password;
    });

// Basic info to identify the current authenticated user in the app
UserSchema
    .virtual('userInfo')
    .get(function() {
        return {
            'id': this.id,
            '_id': this.id,
            'displayName': this.displayName,
            'username': this.username,
            'username_idx': this.username_idx,
            'role': this.role,
            'groups': this.groups,
            'summary': this.summary,
            //'following': this.following,
            'followers': this.followers.length,
            'points_balance': this.points_balance,
            'login_count': this.login_count,
            //'feed': this.feed,
            'post_count': this.post_count,
            //'like': this.like,
            //'lol': this.lol,
            //'flag': this.flag,
            'image': this.image,
            'avatar': (this.image) ? this.image.url : '',
            'location': this.location
        };
    });

// Public profile information
UserSchema
    .virtual('profile')
    .get(function() {
        return {
            'id': this.id,
            '_id': this.id,
            'displayName': this.displayName,
            'username': this.username,
            'username_idx': this.username_idx,
            'role': this.role,
            'groups': this.groups,
            'summary': this.summary,
            'following': this.following,
            'followers': this.followers,
            'points_balance': this.points_balance,
            'login_count': this.login_count,
            'feed': this.feed,
            'post_count': this.post_count,
            'like': this.like,
            'lol': this.lol,
            'flag': this.flag,
            'image': this.image,
            'avatar': (this.image) ? this.image.url : '',
            'location': this.location,
            'active': this.active,
            'banned': this.banned
        };
    });

/**
 * Pre-validation hook
 */
UserSchema
    .pre('validate', function(next) {
        if (!this.isNew) return next();

        // if username isn't supplied we generate one -- username is used primarily for CAS
        if (!this.username) {
            // TODO: if this isn't unique because two people signed up simultaneously, they will get a confusing error message
            this.username = (this.displayName.replace(/[^a-zA-Z]/g, '') || 'user') + Date.now();
        }

        // mongoose will not call the validator if the field is not provided nor required
        // these fields are required if using local provider
        if (authTypes.indexOf(this.provider) === -1) {
            this.email = this.email || '';
            this.hashed_password = this.hashed_password || '';
        }

        if (this.isNew || this.isModified('username')) {
            this.username_idx = this.username.toLowerCase();
        }

        if (this.isNew && authTypes.indexOf(this.provider) === -1) {
            this.activation_code = this.createActivationCode();
            /*
            if (!validatePresenceOf(this.invitation)) {
                return next(new Error('Invalid Invitation Code'))
            }
            */
        }

        next();
    });

UserSchema
    .post('save', function(doc) {
        leaderboard.add(doc.username_idx, doc.points_balance);
    });

/**
 * Validations
 */
UserSchema
    .path('username')
    .validate(function(username) {
        // username is required even when using oauth strategies
        return (username && username.length);
    }, 'Username cannot be blank');

UserSchema
    .path('username')
    .validate(function (username, fn) {
        var User = mongoose.model('User');
        // if you are authenticating by any of the oauth strategies, don't validate
        if (authTypes.indexOf(this.provider) !== -1) fn(true);

        // Check only when it is a new user or when email field is modified
        if (this.isNew || this.isModified('username')) {
            User.find({ username_idx: username.toLowerCase() }).exec(function (err, users) {
                fn(!err && users.length === 0);
            });
        } else fn(true);
    }, 'Username is already taken.');

UserSchema
    .path('displayName')
    .validate(function(name) {
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return (name && name.length);
    }, 'Name cannot be blank');

UserSchema
    .path('lang')
    .validate(function(lang) {
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return (lang && lang.length);
    }, 'Language cannot be blank');

UserSchema
    .path('gender')
    .validate(function(gender) {
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return (gender && gender.length);
    }, 'Gender cannot be blank');

// Validate empty email
UserSchema
    .path('email')
    .validate(function(email) {
        // if you are authenticating by any of the oauth strategies, don't validate
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return (email && email.length);
    }, 'Email cannot be blank');

UserSchema
    .path('email')
    .validate(function (email, fn) {
        var User = mongoose.model('User');
  
        // if you are authenticating by any of the oauth strategies, don't validate
        if (authTypes.indexOf(this.provider) !== -1) fn(true);

            // Check only when it is a new user or when email field is modified
            if (this.isNew || this.isModified('email')) {
                User.find({ email: email }).exec(function (err, users) {
                fn(!err && users.length === 0);
            });
        } else fn(true);
    }, 'Email already exists');

// Validate empty password
UserSchema
    .path('hashed_password')
    .validate(function(hashed_password) {
        // if you are authenticating by any of the oauth strategies, don't validate
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return hashed_password.length;
    }, 'Password cannot be blank');

/**
 * Plugins
 */
//UserSchema.plugin(uniqueValidator,  { message: 'The specified {PATH} is already in use.' });

/**
 * Statics
 */
UserSchema.statics = {
    /**
     * Returns find conditions by username, email or phone
     */
    byLogin: function (name) {
        if (name.indexOf('@') !== -1) {
            return { email: name.toLowerCase() };
        }
        // TODO: add phone number support
        return { username_idx: name.toLowerCase() };
    },

    /**
     * Use to prevent externally generated objects from assigning themselves privileges
     */
    sanitize: function (src) {
        var safe = ['name', 'username', 'email', 'password'];
        var dst = {};
        safe.forEach(function (field) {
            if (src[field]) {
                dst[field] = src[field];
            }
        });
        return dst;
    },

    /**
     * Use as a proxy for the client with OAuth2 Client Credentials flow.
     * Required because passport.use verify callback needs a non-null user value for success.
     */
    clientAsUser: function (client) {
        return {
            displayName: client.displayName,
            username: client.displayName,
            role: 'client',
            clientsId: client.id, // not to be confused with client.clientId

            // HACK: stub these so it behaves like a real User Object
            hasRole: function (role) { return !role; },
            hasGroup: function (groups) { return !groups || (Array.isArray(groups) && !groups.length); }
        };
    },

    unBanExpired: function(done) {
        var now = new Date();
        this.find({ bannedUntil: { $lt: now } }).exec(function (err, users) {
            async.each(users, function(user, cb) {
                user.banned = false;
                user.activation_code = this.createActivationCode();
                user.active = false;
                user.verified = false;
                user.save(cb);
            }, function(err) {
                if (err) { return done(err); }
                return done(null);
            });
        });
    },

    load: function (id, next) {
        this.findOne({ _id: id, active: true }, function(err, user) {
            if (err) { return next(err); }
            if (!user) { return next(new Error('User account does not exist.')); }
            return next(null, user);
        });
    },

    search: function(criteria, fn) {
        this.find({ username: new RegExp(criteria, 'i'), active:true })
        .select('username displayName image')
        .limit(50)
        .sort('displayName')
        .exec(fn);
    }
};

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function(password) {
        if (!password) return '';
        var encrypred;
        try {
            encrypred = crypto.createHmac('sha1', this.salt).update(password).digest('hex');
            return encrypred;
        } catch (err) {
            return '';
        }
    },

    /**
     * Test user is in the specified role.
     * Returns true if passed empty role.
     * Currently does exact match, but could do set membership like 'admin' subsumes all of 'user'.
     */
    hasRole: function (role) {
        if (!role || this.role === role) {
            return true;
        }
        return false;
    },

    /**
     * Test user is in at least one of the specified groups.
     * Can be passed an array of groups or a single group.
     * Returns true if passed the empty string or empty array.
     * Assumes groups argument is very short, as we're O(m*n).
     */
    hasGroup: function (groups) {
        if (!groups) {
            return true;
        }
        var granted = this.groups || [];
        if(Array.isArray(groups)) {
            for (var i = groups.length; i--; ){
                if(granted.indexOf(groups[i]) !== -1) {
                    return true;
                }
            }
            return !groups.length;
        } else {
            if(granted.indexOf(groups) !== -1) {
                return true;
            }
        }
        return false;
    },

    createActivationCode: function() {
        return crypto.createHmac('sha1', this.salt).update(this.email).digest('hex');
    }
};

UserSchema.set('autoIndex', false);

var User = mongoose.model('User', UserSchema);
require('../config/models').model('User', User);