'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    pkg = require('../../package.json');

/*
 * @param {String} cause
 * @param {String} errorMessage
 * @param {String} errorUrl
 * @param {Array} stackTrace
 */
var ErrorSchema = new Schema({
    cause: String,
    errorMessage: String,
    errorUrl: String,
    stackTrace: Array,
    version: String,
    username: String,
    createdAt: { type: Date, default: Date.now } 
}, { collection: 'ui-js-errors' });

ErrorSchema.pre('save', function(next) {
    this.version = pkg.version;
    next();
});

ErrorSchema.set('autoIndex', false);

var UIError = mongoose.model('UIError', ErrorSchema);
require('../config/models').model('UIError', UIError);
