'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    slug = require('slug'),
    _ = require('lodash'),
    async = require('async');

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var LocationHash = {
    displayName: {type: String},
    position: {
        latitude: Number,
        longitude: Number
    }
};

var RoomSchema = new Schema({
    image           : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    icon            : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    displayName     : { type: String  },
    summary         : { type: String, default: '' },
    content         : { type: String, default: '' },
    url             : { type: String },
    published       : { type: Date, default: null },
    objectType      : { type: String, default: 'Room' },
    updated         : { type: Date, default: null },
    location        : LocationHash,
    fullImage       : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    thumbnail       : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    author          : { type: ObjectId, ref: 'User' },
    'externalcss'   : { type: String },
    'masthead'      : { type: { kind: String, url: String, width: Number, height: Number, duration: Number }, default: null },
    'founders'      : [{ type: ObjectId, ref: 'User' }],
    'moderators'    : [{ type: ObjectId, ref: 'User' }],
    'parent'        : { type: ObjectId, ref: 'Room', default: null },
    'rooms'             : { type: Number, default: 0 },
    'allow_discussions' : { type: Boolean, default: true },
    'archived'          : { type: Boolean, default: false },
    'endorsed'          : { type: Boolean, default: false },
    'lang'              : { type: String },
    'discussions'       : { type: Number, default: 0 },
    'last_discussion'   : { type: ObjectId, ref: 'Discussion' },
    'last_discussion_by': { type: ObjectId, ref: 'User' },
    'endorsements'      : [],
    'coins_to_open'     : { type: Number },
    'coins_endorsed'    : { type: Number, default: 0 },
    'percentage_endorsed': { type: Number },
    'style'             : {},
    'externalStylesheet': { type: String, default: '' },
    'members'           : { type: Array, default: [] },
    'isPrivate'         : { type: Boolean, default: false },
    'challenge'         : { type: String, default: '' },
    'deleted'           : { type: Boolean, default: false },
    'deletedAt'         : { type: Date },
    'deletedReason'     : { type: String, default: '' },
    'urlCode'           : { type: String },
    'removed'           : { type: Boolean, default: false },
    'removedAt'         : Date,
    'removedText'       : String,
    'categories'        : [{ type: ObjectId, ref: 'Category '}],
    // reactions
    'lol'               : [],
    'like'              : [],
    'flag'              : [],
    'profanity_score'   : { type: Number, default: 0 },
    'displayMode'       : { type: String, enum: ['forum','masonry','integrated'], default: 'forum' },
    'subscribers'       : { type: Array, default: [] },
    'published_raw'     : { type: Number },
    'updated_raw'       : { type: Number },
    'isSticky'          : { type: Number, default: 0 },
    'domain'            : { type: String, default: 'chat.yappn.com', trim: true, lowercase:true }
}, { collection: 'forums-rooms', strict:true });

RoomSchema.methods.generateChallenge = function(len) {
    var buf = [],
        chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        charlen = chars.length;

    for (var i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
};

RoomSchema.methods.doesUserHasAccess = function(user, fn) {
    var allowed = (this.isPrivate && this.members.indexOf(user._id.toString()) === -1) ? false : true;
    return fn(allowed);
};

RoomSchema.methods.subscribe = function(user) {
    if (this.subscribers && this.subscribers.indexOf(user._id.toString()) === -1) {
        this.subscribers.push(user._id.toString());
    }
};

RoomSchema.methods.unsubscribe = function(user) {
    if (this.subscribers.indexOf(user._id.toString()) !== -1) {
        this.subscribers.splice(this.subscribers.indexOf(user._id.toString()),1);
    }
};

RoomSchema.statics.search = function(criteria, leads, fn) {
    var query = this.find({ $or: [ { displayName: new RegExp(criteria, 'i') }, { content: new RegExp(criteria, 'i') } ] });
    if (leads) {
        query.where('allow_discussions', false);
        query.where('endorsed', true);
    }

    query.where('deleted', false);
    query.limit(50);
    query.sort('displayName');
    query.exec(function(err, rooms) {
        return fn(err, rooms);
    });

};

RoomSchema.path('displayName').validate(function (name) {
    return (name && name.length);
}, 'Name cannot be blank');

RoomSchema.path('lang').validate(function (lang) {
    return (lang && lang.length);
}, 'Language cannot be blank');

// -----------------------------------------------------
// HOOKS

RoomSchema.pre('save', function(next) {

    // do anything in here before saving?
    // 
    this.updated = new Date();

    if (this.isNew) {
        this.urlCode        = slug(this.displayName).toLowerCase();
        this.published      = new Date();
        this.published_raw  = new Date().getTime();
        this.subscribers    = [];
    }
    
    if (!this.endorsed) {
        this.percentage_endorsed = Math.ceil((this.coins_endorsed / this.coins_to_open) * 100);
    }
    return next();
});

RoomSchema.set('autoIndex', false);

var Room = mongoose.model('Room', RoomSchema);
require('../config/models').model('Room', Room);