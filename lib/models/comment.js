'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    slug = require('slug'),
    _ = require('lodash'),
    async = require('async'),
    models = require('../config/models');

var LocationHash = {
    displayName: {type: String},
    position: {
        latitude: Number,
        longitude: Number
    }
};

var CommentSchema = new Schema({ 
    thumbnail       : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    image           : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    content         : { type: String },
    lang            : { type: String, trim: true },
    url             : { type: String },
    objectType      : { type: String, default: 'comment' },
    location        : LocationHash,
    author          : { type: ObjectId, ref: 'User' },
    // reactions
    'like'          : { type: Array, default: [] },
    'flag'          : { type: Array, default: [] },
    'lol'           : { type: Array, default: [] },
    // where is this comment and to what?
    'reference'         : { type: String, default: 'discussions' },
    'referenceTo'       : { type: ObjectId, index: true },
    // admin
    'deleted'           : { type: Boolean, default: false },
    'deletedAt'         : { type: Date },
    'deletedReason'     : { type: String },
    // profanity?
    'profanity_score'   : { type: Number, default: 0 },
    // date and time stuff
    published       : { type: Date, default: null },
    updated         : { type: Date, default: null },
    chronological   : { type: Number },
    history: { type: [{ message: String, at: Date }], default: [] }
}, { collection: 'forums-posts', strict:true });

CommentSchema.path('content').validate(function (content) {
    return (content && content.length);
}, 'comment cannot be blank');

CommentSchema.path('lang').validate(function (lang) {
    return (lang && lang.length);
}, 'Language cannot be blank');

CommentSchema.pre('save', function(next) {
    var self = this;

    if (this.isNew) {
        this.chronological = Math.round(new Date().getTime() / 1000);
        this.published = new Date();
    }

    this.updated = new Date();
    next();


    

});

CommentSchema.index({ referenceTo: 1, deleted: 1}, { unique: false });
CommentSchema.index({ deleted: 1, author: 1}, { unique: false });

CommentSchema.statics.search = function(criteria, fn) {
    this.find({ $or: [ { displayName: new RegExp(criteria, 'i') }, { content: new RegExp(criteria, 'i') } ] })
    .where('reference', 'discussions')
    .where('deleted', false)
    .sort('-published')
    .limit(50)
    .exec(function(err, docs) {
        var ids = _.pluck(docs, '_id');
        var Discussion = models.model('Discussion');
        Discussion.find({ _id: { $in: ids }}, function(err, discussions) {
            return fn(err, discussions);
        });
    });
};

CommentSchema.statics.getByReference = function(reference, page, limit, fn) {

    this.find({ referenceTo: reference, deleted: false })
    .populate('author', 'username username_idx displayName active verified id gender coins_balance points_balance image roles')
    .sort('-published')
    .paginate(page, limit, function(err, docs, total) {
        if (err) { return fn(err); }
        return fn(null, {
            total: total,
            pages: Math.ceil(total/limit),
            page: page,
            comments: docs
        });
    });
};

CommentSchema.set('autoIndex', false);

var Comment = mongoose.model('Comment', CommentSchema);
models.model('Comment', Comment);

