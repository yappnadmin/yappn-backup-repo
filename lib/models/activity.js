'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var LocationHash = {
    displayName: {type: String},
    position: {
        latitude: Number,
        longitude: Number
    }
};

var ActivityObjectHash = {
    id: {type: String},
    image: {type: { url: String, width: Number, height: Number, duration: Number }, default: null},
    icon: {type: { url: String, width: Number, height: Number, duration: Number }, default: null},
    displayName: {type: String},
    summary: {type: String},
    content: {type: String},
    url: {type:String},
    objectType: {type: String, lowercase: true },
    location: LocationHash,
    fullImage : {type: { url: String, width: Number, height: Number, duration: Number }, default: null},
    thumbnail : {type: { url: String, width: Number, height: Number, duration: Number }, default: null},
    author : { type: ObjectId },
    attachments : [{ type: ObjectId }],
    upstreamDuplicates : [{type: String, default: null}],
    downstreamDuplicates : [{type: String, default: null}],
    published: { type: Date, default: null },
    updated: { type: Date, default: null },
};

var defaultActor = { displayName: 'Someone', objectType: 'person' };

var ActivitySchema = new Schema({
    verb: { type: String, default: 'post' },
    url: { type: String },
    title: { type: String },
    content: { type: String },
    icon: { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    object: { type: ActivityObjectHash, default: null },
    actor: { type: ActivityObjectHash, default: defaultActor },
    target: { type: ActivityObjectHash, default: null },
    published: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    inReplyTo: { type: ObjectId, ref: 'Activity' },
    provider: { type: ActivityObjectHash, default: null },
    generator: { type: ActivityObjectHash, default: null },
    streams: [{ type: String, default: [] }],
    broadcast: { type: Boolean, default: false },
    broadcasted: { type: Boolean, default: false }
}, { collection: 'users-activities', strict: true });

ActivitySchema.pre('save', function(next) {
    if (this.isNew) { this.published = new Date(); }
    this.updated = new Date();
    next();
});

ActivitySchema.statics = {

    publish: function(user, verb, object, target, title, broadcast, callback) {
        if (!user) { throw('Cannot publish activity without an actor!'); }
        //user = user.toActivityObject();

        var activity = new this({
            object: object,
            verb: verb,
            actor: user
        });


        if (!title) {
            var pastTense = verb;

            if (pastTense.substr(pastTense.length -1) === 'e') {
                pastTense += 'd';
            } else {
                pastTense += 'ed';
            }

            activity.title = pastTense + ' a new ' + activity.object.objectType;
        } else {
            activity.title = title;
        }

        if (target) {
            activity.target = target;
        }



        if (broadcast) {
            activity.broadcast = true;
            activity.broadcasted = false;
        }

        activity.save(callback);
    }

};

ActivitySchema.set('autoIndex', false);

var Activity = mongoose.model('Activity', ActivitySchema);
require('../config/models').model('Activity', Activity);