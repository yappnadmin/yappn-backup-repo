'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    log = require('../helpers/logger'),
    realtime = require('../helpers/realtime').realtime,
    emailer = require('../helpers/emailer');

var NotificationSchema = new Schema({
    user        : { type: Schema.ObjectId, ref: 'User' },
    notification: { type: String },
    message     : { type: String },
    createdAt   : { type: Date, default: Date.now },
    read        : { type: Boolean, default: false }
}, { collection: 'users-notifications' });

NotificationSchema.statics = {

    // notification can be:
    // mention
    // badges
    // conversation
    // discussion
    // follower

    broadcast: function (user, notification, message, fn) {
        fn();
        this.create({
            user: user._id,
            notification:notification,
            message: message
        }, function(err) {
            if (err) {
                console.log('there is an error creating: ', err);
                //return fn(err);
            }

            if (user.notifications[notification]) {
                //console.log('s/he wants to be notified.');
                realtime.pushTo(user.username.toLowerCase(), message);
                if (user.provider === 'local') {
                    emailer.send('notification', {
                        to: user.email,
                        body: message
                    }, function(err) {
                        if (err) {console.log('err', err);}
                    });
                }
            }
        });
    }

};


NotificationSchema.set('autoIndex', false);
var Notification = mongoose.model('Notification', NotificationSchema);
require('../config/models').model('Notification', Notification);