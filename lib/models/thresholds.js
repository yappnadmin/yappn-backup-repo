'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

var ThresholdSchema = new Schema({
    reference: { type: String, default: 'master', trim: true, lowercase: true },
    threshold: { type: String, default: '', trim: true, lowercase: true },
    value: Schema.Types.Mixed
}, { collection: 'thresholds', strict: true });

ThresholdSchema.index({ reference:1, threshold: 1}, { unique: true });

ThresholdSchema.statics = {

    get: function(ref, thresh, fn) {
        this.find({ $or: [{ reference: ref, threshold: thresh }, { reference: 'master', threshold: thresh }]}).exec(function(err, docs) {
            if (err) { 
                console.log('threshold error: looking for ref: '+ref+' and thresh: '+thresh+' failed.', err);
                return fn(null);
            }
            
            if (!docs || docs.length === 0) { return fn(null); }

            // lets check for one with the proper reference.
            var refMatch = _.find(docs, { reference: ref });

            if (_.isUndefined(refMatch)) { // no reference, check for master reference

                var masterMatch = _.find(docs, { reference: 'master' });

                if (_.isUndefined(masterMatch)) { // no master reference, send back null.
                    return fn(null);
                } else {
                    return fn(masterMatch.value);
                }

            } else {
                return fn(refMatch.value);
            }



        });
    }

};

ThresholdSchema.set('autoIndex', false);

var Threshold = mongoose.model('Threshold', ThresholdSchema);
require('../config/models').model('Threshold', Threshold);