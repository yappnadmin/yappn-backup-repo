'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var LocationHash = {
    displayName: {type: String},
    position: {
        latitude: Number,
        longitude: Number
    }
};

var AvatarSchema = new Schema({
    image: { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    icon: { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    displayName: { type: String },
    summary: { type: String },
    content: { type: String },
    url: { type: String },
    published: { type: Date, default: null },
    objectType: { type: String, default: 'avatar' },
    updated: { type: Date, default: null },
    location: LocationHash,
    author: { type: ObjectId, ref: 'User' },
    'available': { type: Boolean, default: false },
    'avatars': [{ url: String, width: Number, height: Number, duration: Number }]
}, { collection: 'avatars' });

AvatarSchema.pre('save', function(next) {
    if (this.isNew) { this.published = new Date(); }
    this.updated = new Date();
    next();
});

AvatarSchema.set('autoIndex', false);

var Avatar = mongoose.model('Avatar', AvatarSchema);
require('../config/models').model('Avatar', Avatar);
