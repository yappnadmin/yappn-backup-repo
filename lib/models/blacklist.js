'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    config = require('../config/config'),
    _ = require('lodash');

var getTags = function (tags) {
    return tags.join(',');
};

var setTags = function (tags) {
    return tags.split(',');
};

var BlacklistSchema = new Schema({
    ipaddress       : String,
    email           : String,
    note            : String,
    expires         : { type: Boolean, default: false },
    expiredAt       : Date,
    published       : Date,
    updated         : Date
}, { collection: 'blacklist' });

BlacklistSchema.pre('save', function(next) {
    this.updated = new Date();
    if (this.isNew) { this.published = new Date(); }
    next();
});

BlacklistSchema.statics = {

    add: function(criteria, fn) {
        
        var payload = _.extend(criteria, {
            'published': new Date()
        });

        if (payload.expires) {
            payload.expiresAt = config.blacklist.calculateExpirationDate();
        }

        this.findOneAndUpdate(criteria, payload, { new: true, upsert: true }, fn);
    },

    check: function (criteria, fn) {
        this.findOne(criteria, fn);
    }

};

BlacklistSchema.set('autoIndex', false);

var Blacklist = mongoose.model('Blacklist', BlacklistSchema);
require('../config/models').model('Blacklist', Blacklist);