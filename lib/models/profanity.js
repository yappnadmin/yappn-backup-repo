'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    config = require('../config/config'),
    async = require('async'),
    _ = require('lodash');

var ProfanitySchema = new Schema({
    reference: { type: String, default: 'master', trim: true },
    lang: { type: String, default: 'en' },
    word: { type: String, default: '' },
    severity: { type: Number, default: 1 }
}, { collection: 'profanity', strict: true });

ProfanitySchema
    .path('word')
    .validate(function(word) {
        return (word && word.length);
    }, 'Word cannot be blank');

ProfanitySchema
    .path('severity')
    .validate(function(severity) {
        return (severity && severity > 0);
    }, 'Weight must be greater than zero');

ProfanitySchema.statics = {

    score : function(reference, string, lang, fn) {

        if (typeof lang === 'undefined') { lang = 'en'; }

        if (typeof string !== 'string') {
            console.log('\t\t-- not string, aborting for ' + string);
            return fn(0,0,0,[]); 
        }

        var badwords = [];


        // go and get the profanity for this reference, if it's not master

        this.find({ $or: [ { reference: reference, lang: lang }, { reference: 'master', lang: lang }]}).exec(function(err, profanityList) {

            if (err || !profanityList.length) { 
                return fn(0,0,0,[]); 
            }

            badwords[lang] = [];
            var foundMatches = [];

            // ok, use underscore and pick our the reference ones
            if (reference !== 'master') {
                foundMatches = _.where(profanityList, { reference: reference });

                if (foundMatches.length > 0) {
                    for(var p = 0; p < foundMatches.length; p++) {
                        badwords[lang].push(foundMatches[p]);
                    }
                }
            }

            if (reference === 'master' || foundMatches.length === 0) {

    

                foundMatches = _.where(profanityList, { reference: 'master' });


                if (foundMatches.length) {
                    for(var pM = 0; pM < foundMatches.length; pM++) {
                        badwords[lang].push(foundMatches[pM]);
                    }
                }
            }


            var severity = 0;
            var occurances = 0;
            var weighted = 0;
            var infractions = [];
            for (var i = 0; i < badwords[lang].length; i++) {
                if (string.toLowerCase().indexOf(badwords[lang][i].word) >= 0) {
                    var badword = badwords[lang][i];
                    occurances++;
                    severity += badword.severity;
                    infractions.push(badword.word);
                }
            }

            // determine the weight.
            if (occurances > 0) {
                weighted = (severity / occurances);
            }

            return fn(occurances, severity, weighted, infractions);

        });

        
    }

};

ProfanitySchema.set('autoIndex', false);

var Profanity = mongoose.model('Profanity', ProfanitySchema);
require('../config/models').model('Profanity', Profanity);