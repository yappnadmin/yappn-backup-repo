'use strict';

var mongoose = require('mongoose'),
    async = require('async'),
    _ = require('lodash'),
    Schema = mongoose.Schema;

var TranslationCacheSchema = new Schema({
    hash: { type: String, trim: true, default: '', index: { unique: true }},
    lang: String,
    text: [{ lang: String, text: String, created: { type: Date, default: Date.now } }],
    cached: { type: Boolean, default: false },
    created: Date,
    updated: Date,
    expires: Date,
    timeToTranslate: String
}, { strict: true, collection: 'translation-cache'});

TranslationCacheSchema.pre('save', function(done) {
    if (this.isNew) {
        this.created = new Date();
        this.cached = false;
    } else {
        this.cached = true;
    }
    this.updated = new Date();
    done();
});

TranslationCacheSchema.statics = {
    removeExpired : function(done) {
        var now = new Date();
        this.find({ expires: { $lt: now } }).exec(function (err, cached) {
            async.each(cached, function(cache, cb) {
                cache.remove(cb);
            }, function(err) {
                if (err) { return done(err); }
                return done(null);
            });
        });
    },

    get: function (tag, cb) {
        this.findOne({ hash: tag }).exec(cb);
    }
};

TranslationCacheSchema.methods = {

    getTranslationFor: function (toLang) {
        var result = _.findWhere(this.text, { lang: toLang });
        if (result) {
            return result.text;
        } else { return null; }
    },

    setTranslationFor: function (toLang, translatedText) {
        this.text.push({ lang: toLang, text: translatedText, created: new Date() });
        return this;
    }

};

TranslationCacheSchema.set('autoIndex', false);

var TranslationCache = mongoose.model('TranslationCache', TranslationCacheSchema);
require('../config/models').model('TranslationCache', TranslationCache);