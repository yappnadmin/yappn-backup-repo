'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    Chance = require('chance'),
    config = require('../config/config');

var chance = new Chance();

var InvitationSchema = new Schema({
    displayName : { type: String },
    actor       : { type: ObjectId, ref: 'User' },
    published   : { type: Date, default: Date.now },
    'code'      : String,
    'allocated' : { type: Number, default: 10 },
    'used'      : { type: Number, default: 0 },
    createdAt : { type: Date, default: Date.now },
    expiresAt: Date
}, { collection: 'invitations' });

InvitationSchema.statics = {

    check: function(inviteCode, fn) {
        if (config.requires_invite_code_to_register) {
            fn();
        } else {
            this.findOne({ code: inviteCode }, function(err, invitation) {
                if (err) { return fn(err); }
                if (!invitation) { return fn(new Error('That invitation code is not valid, please try again.')); }
                if (invitation.used >= invitation.allocated) { return fn(new Error('I am sorry, but that invitation has expired, all ' + invitation.allocated + ' spots have been used. Have another code?')); }
                
                return fn(null, invitation);
            });
        }
    }

};

InvitationSchema.methods = {

    use: function(cb) {
        this.used = (this.used + 1);
        this.save(cb);
    }

};

InvitationSchema.pre('save', function(next) {
    if (!this.isNew) { return next(); }
    var token = chance.guid();
    this.code = token.toUpperCase();
    next();
});

InvitationSchema.set('autoIndex', false);

var Invitation = mongoose.model('Invitation', InvitationSchema);
require('../config/models').model('Invitation', Invitation);