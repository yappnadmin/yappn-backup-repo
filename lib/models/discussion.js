var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    slug = require('slug'),
    _ = require('lodash'),
    async = require('async');

var getTags = function (tags) {
    return tags.join(',');
};

var setTags = function (tags) {
    return tags.split(',');
};

var LocationHash = {
    displayName: {type: String},
    position: {
        latitude: Number,
        longitude: Number
    }
};

var DiscussionSchema = new Schema({
    image               : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    icon                : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    displayName         : { type: String, default: '' },
    summary             : { type: String, default: '', trim: true },
    content             : { type: String, default: '', trim: true },
    lang                : { type: String, trim: true },
    url                 : { type: String },
    published           : { type: Date, default: null, index: true },
    objectType          : { type: String, default: 'discussion', lowercase: true },
    updated             : { type: Date, default: null, index: true },
    location            : LocationHash,
    fullImage           : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    thumbnail           : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    author              : { type: ObjectId, ref: 'User' },

    'urlCode'           : { type: String },

    'participants'      : [{ type: ObjectId, ref: 'User' }],

    'lead'              : { type: ObjectId, ref: 'Room' },
    'room'              : { type: ObjectId, ref: 'Room' },
    'tags'              : { type: Array, default: [], lowercase: true },
    'views'             : { type: Number, default: 0 },
    'comments'          : { type: Number, default: 0 },

    'last_comment'      : { type: ObjectId, ref: 'Comment' },
    'last_comment_by'   : { type: ObjectId, ref: 'User' },
    'last_comment_date' : { type: Number, default: 0, index: true },

    // reactions
    'like'          : { type: Array, default: [] },
    'flag'          : { type: Array, default: [] },
    'lol'           : { type: Array, default: [] },
    
    // admin
    'deleted'           : { type: Boolean, default: false },
    'deletedAt'         : { type: Date },
    'deletedReason'     : { type: String },

    'closed'            : { type: Boolean, default: false },
    'closedBy'          : { type: ObjectId, ref: 'User' },
    'closedAt'          : Date,

    'chronological'     : { type: Number },

    'profanity_score'   : { type: Number, default: 0 },

    'stream'            : { type: String, default: 'yappn' },

    'displayMode'       : { type: String, enum: ['forum','masonry','integrated'], default: 'forum' },

    'style'             : {},
    'externalcss'       : { type: String },
    'masthead'          : { type: { kind: String, url: String, width: Number, height: Number, duration: Number }, default: null },

    'fanhub'            : { type: ObjectId, ref: 'Hub' },

    'unlockables'       : {
        'customize'         : { type: Boolean, default: false },
        'privatize'         : { type: Boolean, default: false },
        'fanhub'            : { type: Boolean, default: false }
    }

}, { collection: 'forums-discussions', strict: true });


DiscussionSchema.methods.addParticipant = function(user) {
    if (this.participants.indexOf(user._id) === -1) {
        this.participants.push(user._id);
    }
    return;
};

DiscussionSchema.statics.search = function(criteria, fn) {
    this.find({ $or: [ { displayName: new RegExp(criteria, 'i') }, { content: new RegExp(criteria, 'i') } ]})
    .where('deleted', false)
    .sort('displayName')
    .limit(50)
    .exec(function(err, docs) {
        return fn(err, docs);
    });
};

DiscussionSchema.statics.tags = function (options, cb) {
    var criteria = options.criteria || {};
    criteria.deleted = false;

    this.find(criteria).distinct('tags').exec(cb);
};

DiscussionSchema.statics.list = function (options, cb) {
    var criteria = options.criteria || {};
    criteria.deleted = false;

    this.find(criteria)
    .populate({ path: 'last_comment', match: { deleted: false } })
    .populate('last_comment_by', 'username username_idx displayName active verified id gender coins_balance points_balance image roles following followers')
    .populate('author', 'username username_idx displayName active verified id gender coins_balance points_balance image roles following followers')
    .populate('room')
    .populate('lead')
    .sort(options.sorted) // sort by date
    .limit(options.perPage)
    .skip(options.perPage * options.page)
    .exec(cb);
};

DiscussionSchema.path('displayName').validate(function (name) {
    return (name && name.length);
}, 'title cannot be blank');

DiscussionSchema.path('lang').validate(function (lang) {
    return (lang && lang.length);
}, 'Language cannot be blank');

DiscussionSchema.path('content').validate(function (content) {
    return (content && content.length);
}, 'discussion cannot be blank');

// -----------------------------------------------------
// HOOKS

DiscussionSchema.pre('save', function(next) {
    if (this.isNew) {
        this.urlCode = slug(this.displayName).toLowerCase();
        this.published = new Date();
        this.chronological = Math.round(new Date().getTime() / 1000);
        this.last_comment_date = new Date().getTime();
    }
    
    this.updated = new Date();
    next(); 
});


DiscussionSchema.set('autoIndex', false);

var Discussion = mongoose.model('Discussion', DiscussionSchema);
require('../config/models').model('Discussion', Discussion);
