var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    slug = require('slug'),
    profanity = require('../helpers/profanity'),
    _ = require('lodash'),
    async = require('async');

var CategorySchema = new Schema({
    image           : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    icon            : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    displayName     : { type: String, default: '' },
    summary         : { type: String, default: '', trim: true },
    content         : { type: String, default: '', trim: true },
    url             : { type: String },
    published       : { type: Date, default: null, index: true },
    objectType      : { type: String, default: 'category' },
    updated         : { type: Date, default: null, index: true },
    'urlCode'       : { type: String },
    active          : { type: Boolean, default: true },
}, { strict: true, collection: 'forums-categories' });

// validations
CategorySchema.path('displayName').validate(function (name) {
    return name.length;
}, 'Name cannot be blank');

CategorySchema.path('displayName').validate(function (username, fn) {
    var Category = mongoose.model('Category');

    // Check only when it is a new user or when email field is modified
    if (this.isNew || this.isModified('displayName')) {
        var incoming_slug = slug(this.displayName).toLowerCase();
        Category.find({ slug: incoming_slug }).exec(function (err, users) {
            fn(!err && users.length === 0);
        });
    } else { fn(true); }
}, 'Category already exists.');

CategorySchema.pre('save', function (next) {
    if (this.isNew) { this.published = new Date(); }

    if (this.isNew || this.isModified('displayName')) {
        this.urlCode = slug(this.displayName).toLowerCase();
    }

    this.updated = new Date();
    next();
});

CategorySchema.set('autoIndex', false);

var Category = mongoose.model('Category', CategorySchema);
require('../config/models').model('Category', Category);