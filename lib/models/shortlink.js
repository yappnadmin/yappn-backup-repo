'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    Chance = require('chance');

var chance = new Chance();

var LinkSchema = new Schema({
    author: { type: ObjectId, ref: 'User' },
    displayName: String,
    hash: String,
    url: { type: String, default: '', trim: true }
}, { collection: 'shortlinks' });

LinkSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    this.hash = chance.hash({ length: 10 });
    next();
});

LinkSchema
    .path('url')
    .validate(function(url) {
        return (url && url.length);
    }, 'originalUrl cannot be blank');

LinkSchema.statics = {

    findByHash: function(hash, fn) {
        this.findOne({ hash: hash }).exec(fn);
    }

};

LinkSchema.set('autoIndex', false);

var Shortlink = mongoose.model('Shortlink', LinkSchema);
require('../config/models').model('Shortlink', Shortlink);