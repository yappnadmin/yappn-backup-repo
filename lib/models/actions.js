'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserActionSchema = new Schema({
    user: { type: ObjectId, ref: 'User' },
    action: { type: String },
    counter: { type: Number, default: 0 },
    triggered_at: [Date]
}, { collection: 'users-actions' });

UserActionSchema.pre('save', function(next) {
    if (this.isNew || this.modified('counter')) {
        this.triggered_at.push(new Date().getTime());
        next();
    } else { next(); }
});

UserActionSchema.set('autoIndex', false);

var UserAction = mongoose.model('UserAction', UserActionSchema);
require('../config/models').model('UserAction', UserAction);