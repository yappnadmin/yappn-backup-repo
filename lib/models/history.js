'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    _ = require('lodash');

var HistorySchema = new Schema({
    user: { type: ObjectId, ref: 'User' },
    history: { type: String, default: 'view' },
    url: String,
    comment: { type: ObjectId, ref: 'Comment' },
    room: { type: ObjectId, ref: 'Room' },
    discussion: { type: ObjectId, ref: 'Discussion' },
    person: { type: ObjectId, ref: 'User' },
    counter: { type: Number, default: 0 },
    createdAt: { type: Date },
    updatedAt: { type: Date }
}, { collection: 'users-history' });

HistorySchema.pre('save', function(next) {
    if (this.isNew) {
        this.createdAt = new Date();
    }
    this.updatedAt = new Date();
    next();
});

HistorySchema.statics = {

    capture: function(criteria, done) {
        this.findOneAndUpdate(
            criteria,
            { $inc: { 'counter': 1 }},
            { upsert: true },
            done
        );
    }

};

HistorySchema.set('autoIndex', false);

var History = mongoose.model('History', HistorySchema);
require('../config/models').model('History', History);