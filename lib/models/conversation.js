'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

var PMCommentSchema = new Schema({
    body: String,
    user: { type: String, lowercase: true, trim: true },
    user_avatar: String,
    published: Date,
    lang: String,
    image : { type: { url: String, width: Number, height: Number, duration: Number }, default: null },
    thumbnail : { type: { url: String, width: Number, height: Number, duration: Number }, default: null }
});

var PMComment = mongoose.model('PMComment', PMCommentSchema);
require('../config/models').model('PMComment', PMComment);

var ConversationSchema = new Schema({
    recipients: [String],
    new_comments_for: { type: Array, default: [] },
    last_comment_id: String,
    comments: [PMCommentSchema],
    lang: String,
    published: Date,
    updated: Date
}, { collection: 'conversations', strict: true });

ConversationSchema.pre('save', function(next) {
    if (this.isNew) { this.published = new Date(); }
    this.updated = new Date();
    next();
});

ConversationSchema.methods.addComment = function (user, comment, cb) {
    var pcomment = new PMComment({
        body: _.escape(comment.body),
        user: user.username.toLowerCase(),
        published: new Date(),
        lang: comment.lang,
        thumbnail: comment.thumbnail,
        image: comment.image
    });
    this.comments.push(pcomment.toObject());
    this.last_comment_id = pcomment._id;
    this.new_comments_for = _.union(this.new_comments_for, _.without(this.recipients, user.username.toLowerCase()));
    this.save(function(err, doc) {
        cb(err, doc, pcomment);
    });
};


ConversationSchema.statics.load = function (id, cb) {
    this.findOne({ _id: id }).sort('-comments.published').exec(cb);
};

ConversationSchema.statics.list = function (options, cb) {
    this.find(options.criteria)
        .lean()
        .sort('-comments.published')
        .paginate(options.page, options.limit, cb);
};

ConversationSchema.set('autoIndex', false);

var Conversation = mongoose.model('Conversation', ConversationSchema);
require('../config/models').model('Conversation', Conversation);