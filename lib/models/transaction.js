'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var TransactionSchema = new Schema({
    user            : { type: ObjectId, ref: 'User' },
    coins           : { type: Number },
    points          : { type: Number },
    reason          : { type: String, default: '' },
    status          : { type: String, enum: ['purchase','refund','deposit','reversed'], defualt: 'purchase' },
    purchased_at    : { type: Date, default: Date.now },
    refunded_at     : Date,
    reversed_at     : Date,
    reversed_reason : String,
    data            : {}
}, { collection: 'users-transactions' });

TransactionSchema.set('autoIndex', false);

var Transaction = mongoose.model('Transaction', TransactionSchema);
require('../config/models').model('Transaction', Transaction);