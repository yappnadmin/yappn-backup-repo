'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    _ = require('lodash');

var LocationHash = {
    displayName: {type: String},
    position: {
        latitude: Number,
        longitude: Number
    },
    ipaddress: { type: String }
};

var BadgeSchema = new Schema({
    image: { type: { duration: Number, height: Number, width: Number, url: String }, default: null },
    icon: { type: { duration: Number, height: Number, width: Number, url: String }, default: null },
    displayName: { type: String },
    summary: { type: String },
    content: { type: String },
    url: { type: String },
    published: { type: Date, default: null },
    objectType: { type: String, default: 'Badge' },
    updated: { type: Date, default: null },
    location: LocationHash,
    fullImage: { type: { duration: Number, height: Number, width: Number, url: String }, default: null },
    thumbnail: { type: { duration: Number, height: Number, width: Number, url: String }, default: null },
    author: { type: ObjectId, ref: 'User' },
    'action': { type: String },
    'threshold': { type: Number, default: 1 },
    'badget_set': { type: String, default: 'bronze' }
}, { collection: 'badges' });

BadgeSchema.pre('save', function(next) {
    if (this.isNew) { this.published = new Date(); }
    this.updated = new Date();
    next();
});

BadgeSchema.statics = {

    findByTrigger: function(trigger, done) {
        this.find({ trigger: trigger }, function(err, badges) {
            return done(err, badges);
        });
    }

};

BadgeSchema.set('autoIndex', false);

var Badge = mongoose.model('Badge', BadgeSchema);
require('../config/models').model('Badge', Badge);
