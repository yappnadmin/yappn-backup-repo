'use strict';

var path = require('path');

function render(view) {
    return function (req, res) {
        var data = {
            currentUser: req.user
        };
        
        return res.render(path.join(__dirname, 'views/'+view), data);
    };
}

module.exports = function(app, policies, config, passport) {

    app.get('/admin', [
        policies.enforce('knownDeveloperPage'),
        render('index.jade')
    ]);

};

