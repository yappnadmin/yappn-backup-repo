'use strict';

var path = require('path');

function render(view) {
    return function (req, res) {
        var data = {
            currentUser: req.user
        };
        
        return res.render(path.join(__dirname, 'views/'+view), data);
    };
}

module.exports = function(app, policies, config, passport) {

    app.get('/developer', [
        policies.enforce('knownDeveloperPage'),
        render('index.jade')
    ]);

    app.get('/developer/getting-started', [
        policies.enforce('knownDeveloperPage'),
        render('getting-started.jade')
    ]);

    app.get('/developer/integrated-sdk', [
        policies.enforce('knownDeveloperPage'),
        render('integrated-sdk.jade')
    ]);

    app.get('/developer/public-api', [
        policies.enforce('knownDeveloperPage'),
        render('api.jade')
    ]);

};

