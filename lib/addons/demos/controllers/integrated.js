'use strict';

var path = require('path'),
    config = require('../../../config/config'),
    models = require('../../../config/models'),
    Room = models.model('Room'),
    User = models.model('User');

function render (view, data) {
    if (typeof data === 'undefined') { data = {}; }
    return function (req, res, next) {
        return res.render(path.join(__dirname, '../' + view), data);
    };
}

// exports
exports.transformers = [
    function (req, res, next) {
        User.findOne({ username_idx: 'yasmine' }, function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('No default user'));
            req.profile = user;
            next();
        });
    },
    function(req, res, next) {
        Room.findOne({ displayName: 'Marvel Cinematic Universe' }, function(err, room) {
            if (err) return next(err);
            if (!room) {
                room = new Room();
                room.displayName = 'Marvel Cinematic Universe';
                room.image = { url: 'https://s3.amazonaws.com/cdn.yappn.com/2.0/marvel.jpg' };
                room.content = 'The Marvel Cinematic Universe (MCU) is a media franchise and shared fictional universe that is centered on a series of superhero films, independently produced by Marvel Studios and based on characters that appear in publications by Marvel Comics.';
                room.author = req.profile;
                room.endorsed = true;
                room.lang = 'en';
                room.displayMode = 'integrated';
                room.domain = 'chat.yappn.com';
                room.save(function(err, room) {
                    if (err) return next(err);
                    res.locals.account = room;
                    next();
                });
            } else {
                res.locals.account = room;
                next();
            }
        });
    },
    render('views/integrated/transformers', { rootUrl: config.rootUrl })
];