'use strict';

var integrated = require('./controllers/integrated');

module.exports = function (app, securityPolicy, config, passport) {

    app.get('/demos/integrated/transformers-4-age-of-extinction', securityPolicy.enforce('anonUserPage'), integrated.transformers);

};