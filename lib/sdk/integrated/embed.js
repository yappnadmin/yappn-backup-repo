'use strict';
/*jslint browser:true */
/*global jQuery:false */

var log = function(msg, type) {
    if (typeof console !== 'undefined') {
        if (type === 'warn') {
            console.warn('Yappn Integration Error: ', msg);
        } else {
            console.info(msg);
        }
        
    }
};

function toQueryString(obj) {
    var parts = [];
    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
        }
    }
    return parts.join("&");
}

var YappnIntegrated = function(element) {
    this.accountId = null;
    this.height = 500;
    this.width = 500;
    this.el = null;

    this.title = document.title;
    this.description = jQuery('meta[name=description]').attr('content');

    this.seemless = true;
    this.theme = 'default';

    this.iframeHost = 'http://chat.yappn.com';
    this.el = element;

    if (!this.el) {
        log('Please pass the ID of the element you wish to use for Yappn Integrated Chat. See installation instructions.', 'warn'); return;
    }

};


// SETTERS ----------------------------------------------------
YappnIntegrated.prototype.setAccount = function(id) {
    this.accountId = id;
};

YappnIntegrated.prototype.setTitle = function(title) {
    this.title = title;
};

YappnIntegrated.prototype.setDescription = function(content) {
    this.description = content;
};

YappnIntegrated.prototype.setHost = function(host) {
    this.iframeHost = host;
};

YappnIntegrated.prototype.setDimensions = function(w, h) {
    this.width = w;
    this.height = h;
};

// BUILDERS ---------------------------------------------------
YappnIntegrated.prototype.initialize = function(element) {
    log('initializing chat window');

    if (!this.accountId) { log('Account ID missing. See installation instructions.', 'warn'); return; }
    if (!this.title) { log('Document title missing. See installation instructions.', 'warn'); return; }
    if (!this.description) { log('Document description missing. See installation instructions.', 'warn'); return; }

    var params = toQueryString({
        title: this.title,
        description: this.description,
        theme: this.theme,
        url: location.href,
        width: this.width,
        height: this.height
    });

    var url = this.iframeHost + '/integrated/' + this.accountId;
    var construct = jQuery.post(url, params, function(data) {
        log('done loading.');
    }, "html");

    var self = this;

    construct.fail(function(result) {
        alert(result.responseText);
        return;
    });

    construct.done(function(html) {
        jQuery(self.el).html(html);
    });

};