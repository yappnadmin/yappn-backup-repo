'use strict';

var config = require('../lib/config/config'),
    models = require('../lib/config/models'),
    Comment = models.model('Comment'),
    User = models.model('User'),
    async = require('async');

exports.up = function(next) {
   
    Comment.aggregate({ $group: { _id: '$author', postsCount: { $sum: 1} } }, function(err, results) {
        if (err) { throw err; }
        async.each(results, function(result, cb) {
            User.findByIdAndUpdate(result._id, { post_count: result.postsCount }, function(err) {
                if (err) { throw err; }
                cb();
            });
        }, function(err) {
            if (err) { throw err; }
            next();
        });
    });

};

exports.down = function(next) {
    next();
};