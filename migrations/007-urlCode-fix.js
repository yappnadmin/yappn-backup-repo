'use strict';

var models = require('../lib/config/models'),
    Discussion = models.model('Discussion'),
    async = require('async'),
    slug = require('slug');

exports.up = function(next){

    Discussion.find({}, function(err, discussions) {
        async.each(discussions, function(doc, cb) {
            doc.urlCode = slug(doc.displayName).toLowerCase();
            doc.url = 'http://chat.yappn.com/discussion/' + doc.id;
            doc.save(cb);
        }, function(err) {
            next(err);
        });
    });

};

exports.down = function(next){
    next();
};
