'use strict';

var models = require('../lib/config/models'),
    User = models.model('User'),
    Discussion = models.model('Discussion'),
    async = require('async');

function resetTags (tags) {
    var t = [];
    
    if (!tags) {
        console.log('\t\t-- no tags here');
        return t;
    }

    for(var i = 0; i < tags.length; i++) {
        var inT = tags[i];
        var spIn = inT.split(' ');
        for(var s = 0; s < spIn.length; s++) {
            if (spIn[s].length > 2) {
                t.push(spIn[s]);
            }
        }
    }
    
    return t;

}

exports.up = function(next) {
    // we need to go through all 
    User.find({}).exec(function(err, users) {
        if (err) { return next(err); }
        async.each(users, function(user, cb) {
            user.feed = resetTags(user.feed);
            user.save(cb);
        }, function(err) {
            if (err) { return next(err); }
            // users are done, do discussions.
            Discussion.find({}).exec(function(err, discussions) {
                if (err) { return next(err); }
                async.each(discussions, function(discussion, cb) {
                    discussion.tags = resetTags(discussion.tags);
                    discussion.save(cb);
                }, function(err) {
                    next(err);
                });
            });
        });
    });
};

exports.down = function(next){
    next();
};
