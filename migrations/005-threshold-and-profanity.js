'use strict';

var models = require('../lib/config/models'),
    Threshold = models.model('Threshold'),
    Profanity = models.model('Profanity');

exports.up = function(next) {

    Threshold.remove().exec();
    Threshold.create(
    {
        reference: 'master',
        threshold: 'post-profanity',
        value: 5
    },
    {
        reference: 'master',
        threshold: 'post-flag',
        value: 3
    },
    {
        reference: 'master',
        threshold: 'discussion-flag',
        value: 10
    },
    {
        reference: 'master',
        threshold: 'room-flag',
        value: 10
    },
    {
        reference: 'master',
        threshold: 'user-flag',
        value: 10
    },
    {
        reference: 'master',
        threshold: 'user-flag-permanant',
        value: 20
    },
    {
        reference: 'master',
        threshold: 'popular-days',
        value: 5
    },
    {
        reference: 'master',
        threshold: 'room-tip',
        value: 10
    },
    function (err) {
        if (err) {
            console.log(err);
            next();
        }

        // okay, so lets now go and create the profanity list
        Profanity.remove().exec();
        Profanity.create(
        {
            reference: 'master',
            lang: 'en',
            word: 'skank',
            severity: 1
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'wetback',
            severity: 1
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'bitch',
            severity: 1
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'cunt',
            severity: 9
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'dick',
            severity: 1
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'douchbag',
            severity: 4
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'dyke',
            severity: 3
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'fag',
            severity: 7
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'nigger',
            severity: 5
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'tranny',
            severity: 3
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'trannies',
            severity: 3
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'paki',
            severity: 1
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'pussy',
            severity: 4
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'retard',
            severity: 2
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'slut',
            severity: 4
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'tits',
            severity: 3
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'whore',
            severity: 4
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'chink',
            severity: 5
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'fatass',
            severity: 2
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'twat',
            severity: 7
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'lesbo',
            severity: 3
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'homo',
            severity: 3
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'fuck',
            severity: 8
        },
        {
            reference: 'master',
            lang: 'en',
            word: 'fucking',
            severity: 8
        }, function (err) {
            if (err) { console.log(err); }
            next();
        });
    });
};

exports.down = function(next){
    next();
};
