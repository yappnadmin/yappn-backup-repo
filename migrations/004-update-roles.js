'use strict';

var async = require('async'),
    models = require('../lib/config/models'),
    User = models.model('User');

exports.up = function(next) {
    User.find().exec(function(err, users) {
        if (err) {
            throw err;
        }

        async.each(users, function(user, cb) {
            user.role = 'user';
            user.groups = [];
            user.displayName = user.username;
            user.notifications = {
                mention : true,
                badge : true,
                conversation : true,
                discussion : true,
                follower : true,
                newsletter : true
            };
            delete user.roles;
            user.save(function(err) {
                if (err) { throw err; }
                return cb();
            });
        }, function(err) {
            if (err) { throw err; }
            return next();
        });
    });

};

exports.down = function(next) {
    next();
};