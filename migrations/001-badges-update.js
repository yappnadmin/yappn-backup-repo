'use strict';

var config = require('../lib/config/config'),
    models = require('../lib/config/models'),
    Badge = models.model('Badge');

exports.up = function(next) {
    Badge.remove().exec();
    Badge.create(
    {
        action: 'BADGE.BETA_USER',
        summary: 'badges.beta.pioneer',
        displayName: 'Pioneer',
        badge_set: 'beta',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/pioneer.png' }
    },
    {
        action: 'BADGE.YOU_VISITED_ROOM',
        summary: 'badges.bronze.explorer',
        displayName: 'Explorer',
        badge_set: 'bronze',
        threshold: 3,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/explorer.png' }
    },
    {
        action: 'BADGE.YOU_VISITED_ROOM',
        summary: 'badges.silver.explorer',
        displayName: 'Explorer',
        badge_set: 'silver',
        threshold: 20,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/explorer.png' }
    },
    {
        action: 'BADGE.YOU_VISITED_ROOM',
        summary: 'badges.gold.explorer',
        displayName: 'Explorer',
        badge_set: 'gold',
        threshold: 156,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/explorer.png' }
    },
    {
        action: 'BADGE.YOU_VISITED_ROOM',
        summary: 'badges.unobtainium.explorer',
        displayName: 'Explorer',
        badge_set: 'unobtainium',
        threshold: 750,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/explorer.png' }
    },
    {
        action: 'BADGE.YOU_POSTED',
        summary: 'badges.bronze.posterchild',
        displayName: 'Poster Child',
        badge_set: 'bronze',
        threshold: 6,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/posterchild.png' }
    },
    {
        action: 'BADGE.YOU_POSTED',
        summary: 'badges.silver.posterchild',
        displayName: 'Poster Child',
        badge_set: 'silver',
        threshold: 35,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/posterchild.png' }
    },
    {
        action: 'BADGE.YOU_POSTED',
        summary: 'badges.gold.posterchild',
        displayName: 'Poster Child',
        badge_set: 'gold',
        threshold: 120,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/posterchild.png' }
    },
    {
        action: 'BADGE.YOU_POSTED',
        summary: 'badges.unobtainium.posterchild',
        displayName: 'Poster Child',
        badge_set: 'unobtainium',
        threshold: 1000,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/posterchild.png' }
    },
    {
        action: 'BADGE.YOU_FLAGGED',
        summary: 'badges.bronze.critical',
        displayName: 'Critic-Al',
        badge_set: 'bronze',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/critical.png' }
    },
    {
        action: 'BADGE.YOU_FLAGGED',
        summary: 'badges.silver.critical',
        displayName: 'Critic-Al',
        badge_set: 'silver',
        threshold: 42,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/critical.png' }
    },
    {
        action: 'BADGE.YOU_FLAGGED',
        summary: 'badges.gold.critical',
        displayName: 'Critic-Al',
        badge_set: 'gold',
        threshold: 300,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/critical.png' }
    },
    {
        action: 'BADGE.YOU_FLAGGED',
        summary: 'badges.unobtainium.critical',
        displayName: 'Critic-Al',
        badge_set: 'unobtainium',
        threshold: 1000,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/critical.png' }
    },
    {
        action: 'BADGE.PEOPLE_LIKE_YOUR_POST',
        summary: 'badges.bronze.likable',
        displayName: 'Likable',
        badge_set: 'bronze',
        threshold: 4,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/likable.png' }
    },
    {
        action: 'BADGE.PEOPLE_LIKE_YOUR_POST',
        summary: 'badges.silver.likable',
        displayName: 'Likable',
        badge_set: 'silver',
        threshold: 39,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/likable.png' }
    },
    {
        action: 'BADGE.PEOPLE_LIKE_YOUR_POST',
        summary: 'badges.gold.likable',
        displayName: 'Likable',
        badge_set: 'gold',
        threshold: 100,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/likable.png' }
    },
    {
        action: 'BADGE.PEOPLE_LIKE_YOUR_POST',
        summary: 'badges.unobtainium.likable',
        displayName: 'Likable',
        badge_set: 'unobtainium',
        threshold: 500,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/likable.png' }
    },
    {
        action: 'BADGE.YOU_FOUNDED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.bronze.founder',
        displayName: 'Founder',
        badge_set: 'bronze',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/founder.png' }
    },
    {
        action: 'BADGE.YOU_FOUNDED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.silver.founder',
        displayName: 'Founder',
        badge_set: 'silver',
        threshold: 6,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/founder.png' }
    },
    {
        action: 'BADGE.YOU_FOUNDED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.gold.founder',
        displayName: 'Founder',
        badge_set: 'gold',
        threshold: 14,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/founder.png' }
    },
    {
        action: 'BADGE.YOU_FOUNDED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.unobtainium.founder',
        displayName: 'Founder',
        badge_set: 'unobtainium',
        threshold: 150,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/founder.png' }
    },
    {
        action: 'BADGE.YOU_CREATED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.bronze.creator',
        displayName: 'Creator',
        badge_set: 'bronze',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/creator.png' }
    },
    {
        action: 'BADGE.YOU_CREATED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.silver.creator',
        displayName: 'Creator',
        badge_set: 'silver',
        threshold: 4,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/creator.png' }
    },
    {
        action: 'BADGE.YOU_CREATED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.gold.creator',
        displayName: 'Creator',
        badge_set: 'gold',
        threshold: 18,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/creator.png' }
    },
    {
        action: 'BADGE.YOU_CREATED_A_ROOM_THAT_WENT_LIVE',
        summary: 'badges.unobtainium.creator',
        displayName: 'Creator',
        badge_set: 'unobtainium',
        threshold: 50,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/creator.png' }
    },
    {
        action: 'BADGE.YOU_FOLLOWED_SOMEONE',
        summary: 'badges.bronze.zombie',
        displayName: 'Zombie',
        badge_set: 'bronze',
        threshold: 5,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/zombie.png' }
    },
    {
        action: 'BADGE.YOU_FOLLOWED_SOMEONE',
        summary: 'badges.silver.zombie',
        displayName: 'Zombie',
        badge_set: 'silver',
        threshold: 25,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/zombie.png' }
    },
    {
        action: 'BADGE.YOU_FOLLOWED_SOMEONE',
        summary: 'badges.gold.zombie',
        displayName: 'Zombie',
        badge_set: 'gold',
        threshold: 100,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/zombie.png' }
    },
    {
        action: 'BADGE.YOU_FOLLOWED_SOMEONE',
        summary: 'badges.unobtainium.zombie',
        displayName: 'Zombie',
        badge_set: 'unobtainium',
        threshold: 750,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/zombie.png' }
    },
    {
        action: 'BADGE.YOU_ARE_BEING_FOLLOWED',
        summary: 'badges.bronze.stalkable',
        displayName: 'Stalkable',
        badge_set: 'bronze',
        threshold: 4,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/stalkable.png' }
    },
    {
        action: 'BADGE.YOU_ARE_BEING_FOLLOWED',
        summary: 'badges.silver.stalkable',
        displayName: 'Stalkable',
        badge_set: 'silver',
        threshold: 20,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/stalkable.png' }
    },
    {
        action: 'BADGE.YOU_ARE_BEING_FOLLOWED',
        summary: 'badges.gold.stalkable',
        displayName: 'Stalkable',
        badge_set: 'gold',
        threshold: 90,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/stalkable.png' }
    },
    {
        action: 'BADGE.YOU_ARE_BEING_FOLLOWED',
        summary: 'badges.unobtainium.stalkable',
        displayName: 'Stalkable',
        badge_set: 'unobtainium',
        threshold: 500,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/stalkable.png' }
    },
    {
        action: 'BADGE.YOU_COMPLETED_SURVEY',
        summary: 'badges.bronze.surveyor',
        displayName: 'Surveyor',
        badge_set: 'bronze',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/surveyor.png' }
    },
    {
        action: 'BADGE.YOU_COMPLETED_SURVEY',
        summary: 'badges.silver.surveyor',
        displayName: 'Surveyor',
        badge_set: 'silver',
        threshold: 5,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/surveyor.png' }
    },
    {
        action: 'BADGE.YOU_COMPLETED_SURVEY',
        summary: 'badges.gold.surveyor',
        displayName: 'Surveyor',
        badge_set: 'gold',
        threshold: 20,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/surveyor.png' }
    },
    {
        action: 'BADGE.YOU_COMPLETED_SURVEY',
        summary: 'badges.unobtainium.surveyor',
        displayName: 'Surveyor',
        badge_set: 'unobtainium',
        threshold: 75,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/surveyor.png' }
    },
    {
        action: 'BADGE.SOMEONE_FINDS_YOU_FUNNY',
        summary: 'badges.bronze.comedian',
        displayName: 'Comedian',
        badge_set: 'bronze',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/comedian.png' }
    },
    {
        action: 'BADGE.SOMEONE_FINDS_YOU_FUNNY',
        summary: 'badges.silver.comedian',
        displayName: 'Comedian',
        badge_set: 'silver',
        threshold: 15,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/silver/comedian.png' }
    },
    {
        action: 'BADGE.SOMEONE_FINDS_YOU_FUNNY',
        summary: 'badges.gold.comedian',
        displayName: 'Comedian',
        badge_set: 'gold',
        threshold: 50,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/gold/comedian.png' }
    },
    {
        action: 'BADGE.SOMEONE_FINDS_YOU_FUNNY',
        summary: 'badges.unobtainium.comedian',
        displayName: 'Comedian',
        badge_set: 'unobtainium',
        threshold: 250,
        icon: { height: 287, width: 288, url: 'images/badges/unobtainium/comedian.png' }
    },
    {
        action: 'BADGE.YOU_COMPLETED_PROFILE',
        summary: 'badges.bronze.biohazzard',
        displayName: 'Bio-hazzard',
        badge_set: 'bronze',
        threshold: 1,
        icon: { height: 287, width: 288, url: '//cdn.yappn.com/common-images/badges/bronze/biohazzard.png' }
    }, function (err) {
        if (err) { console.log(err); }
        next();
    });
};

exports.down = function(next){
    next();
};
