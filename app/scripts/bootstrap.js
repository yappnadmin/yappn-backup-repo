'use strict';

(function() {
    var $injector = angular.injector(['ng']);

    $injector.invoke(function($http, $rootScope) {
        $rootScope.$apply(function() {
            $http.get('/services/translator/detect').success(function(response) {
                angular.module('app').constant('BASE_BROWSER_LANG', response.data.lang);
                angular.bootstrap(document, ['app']);
            });
        });
    });

})();