'use strict';

angular.module('app')
    .factory('User', function($resource) {
        return $resource('/api/user/:id', {
            id: '@id'
        }, { //parameters default
            update: {
                method: 'PUT',
                params: {}
            },
            get: {
                method: 'GET',
                params: {
                    id: 'me'
                }
            }
        });
    });