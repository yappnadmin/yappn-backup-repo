'use strict';

angular.module('app')
.factory('FayeService', ['$faye', function($faye) {
    return $faye('/realtime', function(client) {
        client.disable('websocket');
        return client;
    });
}]);