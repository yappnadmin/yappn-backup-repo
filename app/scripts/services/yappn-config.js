'use strict';

angular.module('app')
    .service('YappnConfig', function YappnConfig($window, $http, $rootScope) {
        if ($window.YAPPN_CONFIG) {
            $rootScope.YappnConfig = this;
            var that = this;
            var decoded = JSON.parse($window.YAPPN_CONFIG);

            this.providers = decoded.providers;

            this.hasProvider = function (provider) {
                console.log('looking for: ' + provider + ' in ', this.providers);
                return that.providers.indexOf(provider) !== -1;
            };

            this.getAccount = function() {
                return decoded.account || null;
            };

            this.getDiscussion = function() {
                return decoded.discussion || null;
            };

            this.getRootUrl = function () {
                return decoded.rootUrl || 'http://localhost:9000';
            };

            $http.get('version.json').success(function(version) {
                that.version = version;
            });
        }
    });