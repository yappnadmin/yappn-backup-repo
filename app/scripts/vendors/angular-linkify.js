angular.module('linkify', []);

angular.module('linkify')
    .filter('linkify', function() {
        'use strict';

        function youtube_parser(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                return match[2];
            } else {
                return false;
            }
        }

        function linkify(_str, type) {

            //console.log("string before", _str);

            var _text = _str.replace(/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+(?![^\s]*?")([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/ig, function(url) {

                // ok, so we encountered a URL.
                var wrap = document.createElement('div');

                // ok, i need to detect if it's a youtube video.
                var youtubeID = youtube_parser(url);
                if (youtubeID) {
                    return '<div style="height:400px; margin-top:10px; margin-bottom:10px; display:block;"><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="//www.youtube.com/embed/' + youtubeID + '" frameborder="0" allowfullscreen></iframe></div>';
                }

                // nope. move on.

                var anch = document.createElement('a');
                anch.href = url;
                anch.target = "_blank";

                // now we need to check to see if the link is an image, and insert the image inside the anchor instead of the text or the url
                if (url.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                    anch.innerHTML = '<img src="' + url + '" width="200" class="pull-left post-image-box" style="width: 200px;" />';
                } else {
                    anch.innerHTML = url;
                }

                wrap.appendChild(anch);
                return wrap.innerHTML;
            });

            //console.log("text before: ", _text);

            // Twitter
            if (type === 'twitter') {
                _text = _text.replace(/(|\s)*@(\w+)/g, '$1<a href="https://twitter.com/$2" target="_blank">@$2</a>');
                _text = _text.replace(/(^|\s)*#(\w+)/g, '$1<a href="https://twitter.com/search?q=%23$2" target="_blank">#$2</a>');
            }

            // Github
            if (type === 'github') {
                _text = _text.replace(/(|\s)*@(\w+)/g, '$1<a href="https://github.com/$2" target="_blank">@$2</a>');
            }

            // Yappn
            if (type === 'yappn') {
                _text = _text.replace(/(|\s)*@(\w+)/g, '$1<a href="/people/$2">@$2</a>');
                _text = _text.replace(/(^|\s)*#(\w+)/g, '$1<a href="/?search=$2" target="_self">#$2</a>');
            }

            //console.log("text after", _text);

            return _text;
        }

        //
        return function(text, type) {
            return linkify(text, type);
        };
    })
    .factory('linkify', ['$filter',
        function($filter) {
            'use strict';

            function _linkifyAsType(type) {
                return function(str) {
                    (type, str);
                    return $filter('linkify')(str, type);
                };
            }

            return {
                twitter: _linkifyAsType('twitter'),
                github: _linkifyAsType('github'),
                yappn: _linkifyAsType('yappn'),
                normal: _linkifyAsType()
            };
        }
    ])
    .directive('linkify', ['$filter', '$timeout', 'linkify',
        function($filter, $timeout, linkify) {
            'use strict';
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {

                    scope.$watch(element.html(), function() {
                        var type = attrs.linkify || 'normal';
                        $timeout(function() {
                            element.html(linkify[type](element.html()));
                        });
                    });

                    var type = attrs.linkify || 'normal';
                    $timeout(function() {
                        element.html(linkify[type](element.html()));
                    });
                }
            };
        }
    ]);