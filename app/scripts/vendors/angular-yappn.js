'use strict';

var $yappn = angular.module('ngYappn', []);

$yappn.factory('$yappn', ['$rootScope', '$http', '$cookies', function($rootScope, $http, $cookies) {

    var apiError = function (data) {
        //console.log(data);
        $rootScope.$broadcast('apiErrorFound', data.error);
    };

    return {

        api: function (uri, method, data) {
            if (typeof data === 'undefined') { data = {}; }
            if (typeof method === 'undefined') { method = 'GET'; }

            var config = {
                method:     method,
                url:        uri,
                data:       data,
                headers:    {
                    'X-Yappn-Language': $rootScope.lang
                }
            };

            data.lang = $rootScope.lang;

            if (method === 'GET') {
                delete config.data;
                config.params = data;
                config.params._cached = new Date().getTime();
            } else {
                config.headers['X-XSRF-TOKEN'] = $cookies['XSRF-TOKEN'];
            }



            var endpoint = $http(config);
            endpoint.error(apiError);
            return endpoint;
        },

        bringMyOwnIdentity: function (provider, socialObj) {
            var config = {
                method: 'POST',
                url: '/api/social/'+provider+'/authenticate',
                data: socialObj,
                headers: {
                    'X-Yappn-Language': $rootScope.lang
                }
            };

            config.headers['X-XSRF-TOKEN'] = $cookies['XSRF-TOKEN'];

            var endpoint = $http(config);
            endpoint.error(apiError);
            return endpoint;
        },

        getUser: function() {
            return $rootScope.currentUser;
        },

        getLanguages: function () {
            return [
                { value: 'en', displayName: 'English' },
                { value: 'af', displayName: 'Afrikaans' },
                { value: 'sq', displayName: 'Albanian' },
                { value: 'ar', displayName: 'Arabic' },
                { value: 'hy', displayName: 'Armenian' },
                { value: 'az', displayName: 'Azerbaijani' },
                { value: 'eu', displayName: 'Basque' },
                { value: 'be', displayName: 'Belarusian' },
                { value: 'bn', displayName: 'Bengali' },
                { value: 'bg', displayName: 'Bulgarian' },
                { value: 'ca', displayName: 'Catalan' },
                { value: 'zhs', displayName: 'Chinese (Simplified)' },
                { value: 'zht', displayName: 'Chinese (Traditional)' },
                { value: 'hr', displayName: 'Croatian' },
                { value: 'cs', displayName: 'Czech' },
                { value: 'da', displayName: 'Danish' },
                { value: 'nl', displayName: 'Dutch' },
                { value: 'eo', displayName: 'Esperanto' },
                { value: 'et', displayName: 'Estonian' },
                { value: 'tl', displayName: 'Filipino' },
                { value: 'fi', displayName: 'Finnish' },
                { value: 'fr', displayName: 'French' },
                { value: 'gl', displayName: 'Galician' },
                { value: 'ka', displayName: 'Georgian' },
                { value: 'de', displayName: 'German' },
                { value: 'el', displayName: 'Greek' },
                { value: 'gu', displayName: 'Gujarati' },
                { value: 'ht', displayName: 'Haitian Creole' },
                { value: 'he', displayName: 'Hebrew' },
                { value: 'hi', displayName: 'Hindi' },
                { value: 'mww', displayName: 'Hmong Daw' },
                { value: 'hu', displayName: 'Hungarian' },
                { value: 'is', displayName: 'Icelandic' },
                { value: 'id', displayName: 'Indonesian' },
                { value: 'ga', displayName: 'Irish' },
                { value: 'it', displayName: 'Italian' },
                { value: 'ja', displayName: 'Japanese' },
                { value: 'kn', displayName: 'Kannada' },
                { value: 'ko', displayName: 'Korean' },
                { value: 'lo', displayName: 'Laos' },
                { value: 'la', displayName: 'Latin' },
                { value: 'lv', displayName: 'Latvian' },
                { value: 'lt', displayName: 'Lithuanian' },
                { value: 'mk', displayName: 'Macedonian' },
                { value: 'ms', displayName: 'Malay' },
                { value: 'mt', displayName: 'Maltese' },
                { value: 'no', displayName: 'Norwegian' },
                { value: 'fa', displayName: 'Persian' },
                { value: 'pl', displayName: 'Polish' },
                { value: 'pt', displayName: 'Portuguese' },
                { value: 'ro', displayName: 'Romanian' },
                { value: 'ru', displayName: 'Russian' },
                { value: 'sr', displayName: 'Serbian' },
                { value: 'sk', displayName: 'Slovak' },
                { value: 'sl', displayName: 'Slovenian' },
                { value: 'es', displayName: 'Spanish' },
                { value: 'sw', displayName: 'Swahili' },
                { value: 'sv', displayName: 'Swedish' },
                { value: 'ta', displayName: 'Tamil' },
                { value: 'te', displayName: 'Telugu' },
                { value: 'th', displayName: 'Thai' },
                { value: 'tr', displayName: 'Turkish' },
                { value: 'uk', displayName: 'Ukrainian' },
                { value: 'ur', displayName: 'Urdu' },
                { value: 'vi', displayName: 'Vietnamese' },
                { value: 'cy', displayName: 'Welsh' },
                { value: 'yi', displayName: 'Yiddish' }
            ];
        }

    };

}]);