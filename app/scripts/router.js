'use strict';

angular.module('app')
.config(['$stateProvider', '$urlRouterProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $stateProvider.state('DiscussionView', {
        url: '/discussion/:discussion',
        templateUrl: 'partials/discussions/view.html',
        controller: 'Discussions.Show',
        resolve: {
            Discussion: function ($q, $stateParams, $yappn) {
                var deferred    = $q.defer();
                $yappn.api('/api/forums/discussion/' + $stateParams.discussion)
                .success(function(result) { deferred.resolve(result.data.discussion); })
                .error(function() { deferred.reject('Could not load that discussion'); });
                return deferred.promise;
            }
        }
    });

    $stateProvider.state('DiscussionEdit', {
        url: '/discussion/:discussion/edit',
        templateUrl: 'partials/discussions/edit.html',
        controller: 'Discussions.Edit',
        resolve: {
            Discussion: function ($q, $stateParams, $yappn) {
                var deferred    = $q.defer();
                $yappn.api('/api/forums/discussion/' + $stateParams.discussion)
                .success(function(result) { deferred.resolve(result.data.discussion); })
                .error(function() { deferred.reject('Could not load that discussion'); });
                return deferred.promise;
            }
        }
    });

    $stateProvider.state('Start', {
        url: '/start',
        templateUrl: 'partials/discussions/start.html',
        controller: 'Discussions.Create',
        authenticate: true
    });

    $stateProvider.state('Login', {
        reloadOnSearch: false,
        //reload:false,
        url: '/login',
        templateUrl: 'partials/authentication/login.html',
        controller: 'Auth.Login'
    });

    $stateProvider.state('Register', {
        url: '/register',
        templateUrl: 'partials/authentication/register.html',
        controller: 'Auth.Register'
    });

    $stateProvider.state('Forgot', {
        url: '/forgot',
        templateUrl: 'partials/authentication/forgot.html',
        controller: 'Auth.Forgot'
    });

    $stateProvider.state('Twitter', {
        url: '/connect/twitter',
        templateUrl: 'partials/authentication/login.html',
        controller: 'Auth.Twitter'
    });

    $stateProvider.state('Facebook', {
        url: '/connect/facebook',
        templateUrl: 'partials/authentication/login.html',
        controller: 'Auth.Facebook'
    });

    $stateProvider.state('ProfileView', {
        url: '/people/:person',
        templateUrl: 'partials/profile/view.html',
        controller: 'Profile.View',
        resolve: {
            Person: function ($q, $stateParams, $yappn) {
                var deferred    = $q.defer();
                $yappn.api('/api/user/' + $stateParams.person)
                .success(function(result) {
                    var user = result.data.profile;
                    //console.log(user);
                    deferred.resolve(user);
                })
                .error(function() { deferred.reject('Could not load that user'); });
                return deferred.promise;
            },
            Rank: function ($q, $stateParams, $yappn) {
                var deferred    = $q.defer();
                $yappn.api('/api/ranking/' + $stateParams.person)
                .success(function(result) {
                    //// // console.log('ranked: ' + result.data.ranking);
                    deferred.resolve(result.data);
                });
                return deferred.promise;
            },
            Admin: function ($q, Auth) {
                if (Auth.isLoggedIn()) {
                    var u = Auth.getUser();
                    //console.log(u);
                    if (u.role === 'admin') {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
    }).state('ProfileView.Followers', {
        url: '/followers',
        templateUrl: 'partials/profile/followers.html',
        controller: 'Profile.Followers'
    }).state('ProfileView.Following', {
        url: '/following',
        templateUrl: 'partials/profile/following.html',
        controller: 'Profile.Following'
    }).state('ProfileView.Badges', {
        url: '/badges',
        templateUrl: 'partials/profile/badges.html',
        controller: 'Profile.Badges'
    }).state('ProfileView.Comments', {
        url: '/comments',
        templateUrl: 'partials/profile/history.html',
        controller: 'Profile.Comments'
    });
    
    $stateProvider.state('Index', {
        url: '/',
        templateUrl: 'partials/index.html'
    });

    $stateProvider.state('AccountView', {
        url: '/account',
        templateUrl: 'partials/account/view.html',
        controller: 'AccountView',
        authenticate: true,
        resolve: {
            Account: function ($q, $location, $yappn) {
                var d = $q.defer();
                $yappn.api('/api/me', 'GET', {}).success(function(response) {
                    d.resolve(response);
                });
                return d.promise;
            }
        }
    }).state('AccountView.Password', {
        url: '/password',
        templateUrl: 'partials/account/password.html',
        authenticate: true,
        controller: 'AccountView.Password'
    }).state('AccountView.Settings', {
        url: '/settings',
        templateUrl: 'partials/account/settings.html',
        authenticate: true,
        controller: 'AccountView.Settings'
    }).state('AccountView.Feed', {
        url: '/feed',
        templateUrl: 'partials/account/feed.html',
        authenticate: true,
        controller: 'AccountView.Feed'
    }).state('AccountView.Info', {
        url: '/about',
        templateUrl: 'partials/account/info.html',
        authenticate: true,
        controller: 'AccountView.Info'
    }).state('AccountView.Networks', {
        url: '/networks',
        templateUrl: 'partials/account/networks.html',
        authenticate: true,
        controller: 'AccountView.Networks'
    });

    $stateProvider.state('Inbox', {
        url: '/inbox',
        templateUrl: 'partials/inbox/view.html',
        controller: 'Inbox.View',
        authenticate: true,
        resolve: {
            Conversations: function ($q, $yappn) {
                var d = $q.defer();
                $yappn.api('/api/conversations', 'GET', {}).success(function(response) {
                    d.resolve(response.data.conversations);
                });
                return d.promise;
            }
        }
    })
    .state('Inbox.Compose', {
        url: '/compose/:username',
        templateUrl: 'partials/inbox/compose.html',
        authenticate: true,
        controller: 'Inbox.Compose'
    })
    .state('Inbox.Show', {
        url: '/:conversation',
        templateUrl: 'partials/inbox/conversation.html',
        controller: 'Inbox.Conversation',
        authenticate: true,
        resolve: {
            Conversation: function ($q, $yappn, $stateParams) {
                var d = $q.defer();
                $yappn.api('/api/conversation/' + $stateParams.conversation, 'GET', { limit: 50 }).success(function(response) {
                    d.resolve(response.data.conversation);
                });
                return d.promise;
            }
        }
    });

    $urlRouterProvider.otherwise('/');

}]);
