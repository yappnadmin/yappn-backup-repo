'use strict';

angular.module('app', [
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngAnimate',
    'ngCookies',
    'ngTagsInput',
    'faye',
    'pascalprecht.translate',
    'angularMoment',
    'angulartics',
    'angulartics.google.analytics',
    'infinite-scroll',
    'lrInfiniteScroll',
    'truncate',
    'wu.masonry',
    'xeditable',
    'checklist-model',
    'toaster',
    'angularFileUpload',
    'ui.router',
    'ui.bootstrap',
    'http-throttler',
    'linkify',
    'ngYappn',
    'angular-smilies'
])

.config(['$analyticsProvider', '$translateProvider', '$httpProvider', function ($analyticsProvider, $translateProvider, $httpProvider) {
    $analyticsProvider.firstPageview(false);
    $analyticsProvider.virtualPageviews(true);
    $translateProvider.useLoader('customLoader', {});

    $httpProvider.interceptors.push(['httpThrottler', '$q', '$location',
        function(httpThrottler, $q, $location) {
            return {
                'responseError': function(response) {
                    if (response.status === 401) {
                        $location.path('/login');
                        return $q.reject(response);
                    } else {
                        return $q.reject(response);
                    }
                }
            };
        }
    ]);
}])

// run

.run([function() {
    window.OAuth.initialize('hP63Uivzjb1_CXCiW_JDIxBUETY');
}])

.run(['editableOptions','editableThemes', function(editableOptions, editableThemes) {
    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3';
}])

.run(['$rootScope', function($rootScope) {

    $rootScope.$on('$locationChangeStart', function() {
        $rootScope.page = '';
        $rootScope.flash = '';
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState) {
        $rootScope.pageTitle = '';

        if (toState.url === '/' || toState.url === '/explore') {
            $rootScope.scroll = '';
        } else {
            $rootScope.scroll = 'noscroll';
        }
    });

}])
.run(function (YappnConfig) {/*jshint unused:false*/}) // forcing config service to load
.run(function ($rootScope, $location, Auth, $state) {
    $rootScope.$on('$stateChangeStart', function(event, toState) {
        if (toState.authenticate && !Auth.isLoggedIn()) {
            $location.path('/login');
        }
    });

    $rootScope.$on('$stateChangeError', function(event) {
        event.preventDefault();
        $state.go('404');
    });
})

.run(['$rootScope','$yappn','$window','BASE_BROWSER_LANG','$log','$translate', function ($rootScope, $yappn, $window, BASE_BROWSER_LANG, $log, $translate) {

    $rootScope.$on('UserChangedLanguage', function(event, data) {
        $rootScope.lang = data.lang;
        $log.info('Language changed to: ' + data.lang);
        $translate.uses(data.lang);
        $window.moment.lang(data.lang);
    });

    $rootScope.$broadcast('UserChangedLanguage', { lang: BASE_BROWSER_LANG });
    $translate.uses(BASE_BROWSER_LANG);
    $window.moment.lang(BASE_BROWSER_LANG);

}])

// filters

.filter('langCode', [function () {
    var languages      = {
        'en'    : 'English',
        'af'    : 'Afrikaans',
        'sq'    : 'Albanian',
        'ar'    : 'Arabic',
        'hy'    : 'Armenian',
        'az'    : 'Azerbaijani',
        'eu'    : 'Basque',
        'be'    : 'Belarusian',
        'bn'    : 'Bengali',
        'bg'    : 'Bulgarian',
        'ca'    : 'Catalan',
        'zhs'   : 'Chinese (Simplified)',
        'zht'   : 'Chinese (Traditional)',
        'hr'    : 'Croatian',
        'cs'    : 'Czech',
        'da'    : 'Danish',
        'nl'    : 'Dutch',
        'eo'    : 'Esperanto',
        'et'    : 'Estonian',
        'tl'    : 'Filipino',
        'fi'    : 'Finnish',
        'fr'    : 'French',
        'gl'    : 'Galician',
        'ka'    : 'Georgian',
        'de'    : 'German',
        'el'    : 'Greek',
        'gu'    : 'Gujarati',
        'ht'    : 'Haitian Creole',
        'he'    : 'Hebrew',
        'hi'    : 'Hindi',
        'mww'   : 'Hmong Daw',
        'hu'    : 'Hungarian',
        'is'    : 'Icelandic',
        'id'    : 'Indonesian',
        'ga'    : 'Irish',
        'it'    : 'Italian',
        'ja'    : 'Japanese',
        'kn'    : 'Kannada',
        'ko'    : 'Korean',
        'lo'    : 'Laos',
        'la'    : 'Latin',
        'lv'    : 'Latvian',
        'lt'    : 'Lithuanian',
        'mk'    : 'Macedonian',
        'ms'    : 'Malay',
        'mt'    : 'Maltese',
        'no'    : 'Norwegian',
        'fa'    : 'Persian',
        'pl'    : 'Polish',
        'pt'    : 'Portuguese',
        'ro'    : 'Romanian',
        'ru'    : 'Russian',
        'sr'    : 'Serbian',
        'sk'    : 'Slovak',
        'sl'    : 'Slovenian',
        'es'    : 'Spanish',
        'sw'    : 'Swahili',
        'sv'    : 'Swedish',
        'ta'    : 'Tamil',
        'te'    : 'Telugu',
        'th'    : 'Thai',
        'tr'    : 'Turkish',
        'uk'    : 'Ukrainian',
        'ur'    : 'Urdu',
        'vi'    : 'Vietnamese',
        'cy'    : 'Welsh',
        'yi'    : 'Yiddish'
    };

    return function (iso) {
        return (typeof languages[iso] === 'undefined') ? iso : languages[iso];
    };
}]);