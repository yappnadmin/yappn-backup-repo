'use strict';

angular.module('app')
.controller('Explore', ['$rootScope','$scope','$location','$yappn','$timeout','$interval','FlashService','$routeParams','$translate','$log','$window','onlineStatus','toaster','FayeService', 'Auth', function($rootScope, $scope, $location, $yappn, $timeout, $interval, FlashService, $routeParams, $translate, $log, $window, onlineStatus, toaster, FayeService, Auth) {

    window.$rootScope = $rootScope;

    var user_channel = null;

    $scope.languages            = $yappn.getLanguages();
    $scope.user_selected_lang   = $rootScope.lang;

    $rootScope.scroll   = 'noscroll';

    $rootScope.seo      = {
        title: 'Yappn',
        description: 'An international meeting place where people choose and chat about their favorite topics. Regardless of languages of the people you are chatting with, everything they write is displayed in your language, and what you write is displayed in their language.',
        url: 'http://chat.yappn.com'
    };

    // default scope search input
    $scope.search_text  = $location.search().search || '';

    $scope.people = {
        results: [],
        total: 0,
        more: false,
        load: function() {
            $scope.people.results = [];
            $scope.people.total = 0;
            $scope.people.pages = 0;
            $scope.people.more = false;
            if ($scope.search_text.length) {
                $yappn.api('/api/user/search', 'GET', { search: $scope.search_text, limit: 25 }).success(function(response) {
                    if (response.data.users.length) {

                        if (response.data.pages > 1) {
                            $scope.people.more = true;
                        }

                        $scope.people.total = response.data.total;
                        $scope.people.pages = response.data.pages;
                        $scope.people.results = response.data.users;
                    }
                });
            }
        }
    };

    $scope.clearSearchResults = function(e) {
        e.preventDefault();
        $scope.search_text = '';
        $scope.discussions.search = '';
        $scope.reset();
        
    };

    // module specifics
    $scope.discussions = {
        busy        : false,
        canLoad     : true,
        criteria    : null,
        search      : '',
        page        : 1,
        after       : false,
        before      : false,
        filtered    : 'recent',
        check       : undefined,
        results     : [],
        available   : [],
        load        : function () {
            if ($scope.discussions.busy || !$scope.discussions.canLoad) {
                return;
            }
            $scope.discussions.busy = true;
            cancelDiscussionCheck();
            $timeout(function() {
                switch($scope.discussions.filtered) {
                case 'recent':
                    doGetRecent();
                    break;
                case 'popular':
                    doGetPopular();
                    break;
                case 'trending':
                    doGetTrending();
                    break;
                default:
                    doGetMyFeed();
                }
            }, 500);
        }
    };


    $scope.logout = function() {

        Auth.logout(function() {
            $yappn.api('/logout').success(function() {
                $location.path('/');
            });
        });
    };
    
    // setup the tabs and the page title.
    $scope.tabs = {
        feed        : { active: false, title: 'explore.tabs.feed.title' },
        recent      : { active: true,  title: 'explore.tabs.recent.title' },
        popular     : { active: false, title: 'explore.tabs.trending.title' }
    };

    $scope.$on('$translateChangeSuccess', function () {
        $scope.explore_title        = $translate($scope.tabs[$scope.discussions.filtered].title);
    });

    // change based on filter.
    $scope.changeFilter = function(filter) {
        $scope.discussions.filtered     = filter;
        $scope.tabs.feed.active         = false;
        $scope.tabs.recent.active       = false;
        $scope.tabs.popular.active      = false;
        $scope.tabs[(filter === '') ? 'feed': filter].active      = true;

        $scope.explore_title = $translate($scope.tabs[(filter === '') ? 'feed': filter].title);
        $scope.reset();
    };
    
    // reset everything.
    $scope.reset = function() {
        $scope.discussions.results      = [];
        $scope.discussions.available    = [];
        $scope.discussions.page         = 1;
        $scope.discussions.after        = false;
        $scope.discussions.before       = false;
        //$log.warn('setting busy to false');
        $scope.discussions.busy         = false;
        $scope.discussions.canLoad      = true;
        $scope.people.load();
        $scope.discussions.load();
    };


    $scope.updateUserSelectedLanguage = function () {
        $scope.$emit('UserChangedLanguage', { lang: $scope.user_selected_lang });
        $scope.reset();
    };

    $scope.resetAndStart = function (e) {
        e.preventDefault();
        $scope.search_text = '';
        $location.path('/start');
    };

    var checkForNewDiscussions = function () {
        //$log.debug('checkForNewDiscussions() called.');

        var _thisCheck = _doDiscussionsApi('recent', {
            limit       : 25,
            direction   : 'new',
            after       : ($scope.discussions.results.length) ? $scope.discussions.results[0].last_comment_date : ''
        }, false).success(function(response) {
            $scope.discussions.available = response.data.discussions;
        });
        return _thisCheck; // chainable
    };

    var mergeNewDiscussions = function () {
        //$log.debug('mergeNewDiscussions() called.');

        var docs = $scope.discussions.available;
        docs.reverse();
        for (var i = 0; i < docs.length; i++) {
            $scope.discussions.results.unshift(docs[i]);
        }
        $scope.discussions.available = [];
    };

    $scope.mergeDiscussions = function(e) {
        e.preventDefault();
        //$log.debug('mergeDiscussions() called.');
        mergeNewDiscussions();
    };

    var checkAndMergeDiscussions = function () {
        //$log.debug('checkAndMergeDiscussions() called.');

        _doDiscussionsApi('recent', {
            limit       : 25,
            direction   : 'new',
            after       : ($scope.discussions.results.length) ? $scope.discussions.results[0].last_comment_date : ''
        }, false).success(function(response) {
            var docs = response.data.discussions;
            docs.reverse();
            for (var i = 0; i < docs.length; i++) {
                $scope.discussions.results.unshift(docs[i]);
            }
            $scope.discussions.available = [];
        });

    };

    var cancelDiscussionCheck = function () {
        //$log.debug('attempting to cancel the check for new discussions.');

        if (angular.isDefined($scope.discussions.check)) {
           // $log.debug('check was present; cancelling it.');
            $interval.cancel($scope.discussions.check);
            $scope.discussions.check = undefined;
        }
    };

    var startDiscussionCheck = function () {
        //$log.debug('attempting to start the check for new discussions.');
        if (angular.isDefined($scope.discussions.check)) {
            //$log.debug('check is already running.');
            return;
        }
        $log.debug('started the check.');
        $scope.discussions.check = $interval(checkForNewDiscussions, 120000);
    };

    var _doDiscussionsApi = function(filter, params, populate) {
        if (typeof populate === 'undefined') {
            populate = true;
        }

        if (filter === 'feed') {
            filter = '/feed';
        } else {
            filter = '/' + filter;
        }

        //FlashService.clear();

        var api = $yappn.api('/api/forums/discussions' + filter, 'GET', params);

        if (populate) {
            api.success(function(response) {
                for (var i = 0; i < response.data.discussions.length; i++) {
                    $scope.discussions.results.push(response.data.discussions[i]);
                }
            });
        }
        
        return api;

    };

    var doGetRecent = function() {

        //$log.debug('doGetRecent() called.');

        var params = {
            limit       : 25,
            before        : $scope.discussions.before,
            criteria    : $scope.discussions.search
        };

        _doDiscussionsApi('recent', params).success(function(response) {
            if (response.data.next) {

                $scope.discussions.after = response.data.next;
                $scope.discussions.before = response.data.previous;

                if (!params.criteria && params.criteria.length <= 0) {
                    startDiscussionCheck();
                }

                var count = parseInt(response.data.count, 10);
                //$log.debug('count is: ' + count + ' and the limit is: ' + params.limit);


                if ( count < params.limit ) {
                    // this means that we asked for 25 but only got 10 back, no more must be available, stop loading.
                    $scope.discussions.canLoad = false;
                } else {
                    $scope.discussions.canLoad = true;
                }
            } else {
                $scope.discussions.after = '';
                $scope.discussions.before = '';
            }

            $timeout(function() {
                //$log.info('in doGetRecent() -- resetting busy = false;');
                $scope.discussions.busy = false;
            }, 200);
        });

    };

    var doGetPopular = function() {

        //$log.debug('doGetPopular() called.');

        var params = {
            limit       : 25,
            page        : $scope.discussions.page,
            criteria    : $scope.discussions.search
        };

        _doDiscussionsApi('popular', params).success(function(response) {
            $scope.discussions.page = parseInt(response.data.page, 10) + 1;

            if ($scope.discussions.page < parseInt(response.data.pages, 10)) {
                $scope.discussions.canLoad = true;
            } else {
                $scope.discussions.canLoad = false;
            }

            $timeout(function() {
                //$log.info('in doGetPopular() -- resetting busy = false;');
                $scope.discussions.busy = false;
            }, 200);
        });

    };

    var doGetMyFeed  = function() {
        //$log.debug('doGetMyFeed() called.');

        var params = {
            limit       : 25,
            page        : $scope.discussions.page,
            criteria    : $scope.discussions.search
        };

        _doDiscussionsApi('feed', params).success(function(response) {
            $scope.discussions.page = parseInt(response.data.page, 10) + 1;

            if ($scope.discussions.page < parseInt(response.data.pages, 10)) {
                $scope.discussions.canLoad = true;
            } else {
                $scope.discussions.canLoad = false;
            }

            $timeout(function() {
                //$log.info('in doGetMyFeed() -- resetting busy = false;');
                $scope.discussions.busy = false;
            }, 200);
        });
    };

    var doGetTrending = function () {
        //$log.debug('doGetTrending() called.');

        var params = {
            limit       : 25,
            page        : $scope.discussions.page,
            criteria    : $scope.discussions.search
        };

        _doDiscussionsApi('trending', params).success(function(response) {
            $scope.discussions.page = parseInt(response.data.page, 10) + 1;

            if ($scope.discussions.page < parseInt(response.data.pages, 10)) {
                $scope.discussions.canLoad = true;
            } else {
                $scope.discussions.canLoad = false;
            }

            $timeout(function() {
                //$log.info('in doGetTrending() -- resetting busy = false;');
                $scope.discussions.busy = false;
            }, 200);
        });
    };

    // searcing.

    var filterAction = function ($scope) {
        var search_text             = $scope.search_text.toLowerCase();
        $scope.discussions.search   = search_text;
        $scope.reset();
    };

    var filterDelayed = function($scope) {
        $scope.$apply(function() {
            filterAction($scope);
        });
    };

    var filterThrottled = window._.debounce(filterDelayed, 1000);

    var pickFilter = function($scope) {
        filterThrottled($scope);
    };

    var stinit = false;
    $scope.$watch('search_text', function() {

        if (!stinit) {
            pickFilter($scope);
            stinit = true;
        }

        if ($scope.search_text.length >= 3 && stinit) {
            $scope.discussions.busy = true;
            pickFilter($scope);
        }
    });

    // FAYE
    if (Auth.isLoggedIn()) {
        var watchingUser = '/' + $scope.currentUser.username.toLowerCase();

        //console.log($scope.currentUser);

        $scope.metaFunctions = {
            developer: ($scope.currentUser.groups.indexOf('developer') !== -1) ? true : false,
            admin: ($scope.currentUser.role === 'admin') ? true : false
        };

        $log.info('You are now watching the channel: ' + watchingUser);
        user_channel = FayeService.subscribe(watchingUser, function(incoming) {
            toaster.pop(incoming.method, '', incoming.body, 5000, 'trustedHtml');
        });
    } else {
        $scope.metaFunctions = {
            developer: false,
            admin: false
        };
    }

    // WATCHERS
    
    $scope.$on('event::discussion::created', function() {
        //$log.debug('you created a new discussion, now we need to merge it in.');
        checkAndMergeDiscussions();
    });

    $scope.$on('event::discussion::updated', function() {
        //$log.debug('you just updated a discussion, for now we need to just reset.');
        $scope.reset();
    });

    $scope.$on('event::discussion::removed', function () {
        //$log.debug('you just removed a discussion with the ID of: ' + data.id + ', for now we need to just reset.');
        $scope.reset();
    });

    $rootScope.$on('$stateChangeStart', function () {
        //cancelDiscussionCheck();
        //$log.info('$stateChangeStart');
    });

    $rootScope.$on('apiErrorFound', function(event, data) {
        if (data && data.length > 0) {
            toaster.pop('error', '', $translate(data));
        }
    });

}]);