'use strict';

angular.module('app').factory('customLoader', function ($http, $q) {
    return function (options) {
        var deferred = $q.defer();
      
        $http({
            method:'GET',
            url:'/locales/' + options.key + '.json'
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function () {
            deferred.reject(options.key);
        });
      
        return deferred.promise;
    };
});