'use strict';

angular.module('app')
.controller('Profile.Following', ['$scope', '$stateParams', '$log', '$yappn', function($scope, $stateParams, $log, $yappn) {

    //$log.debug('stateParams from within controller: ', $stateParams);

    $scope.username     = $stateParams.person;
    $scope.following    = {
        people      : [],
        busy        : false,
        page        : 1,
        more        : function () {
            $log.debug('doGetMore() called.');

            if ($scope.following.busy) {
                return;
            }

            $scope.following.busy = true;

            var params = { limit: 20, page: $scope.following.page };
            $yappn.api('/api/user/' + $scope.username + '/following', 'GET', params)
            .success(function(response) {
                var p   = parseInt(response.data.page, 10);
                var ps  = parseInt(response.data.pages, 10);
                $scope.following.page = p + 1;
                if (p < ps) {
                    $scope.following.busy = false;
                } else {
                    $scope.following.busy = true;
                }

                var people = response.data.following;
                for (var i = 0; i < people.length; i++) {
                    $scope.following.people.push(people[i]);
                }

                console.log($scope.following.people);

                $scope.$emit('$profileStateChangeSuccess');
            });
        },
        reset       : function () {
            $scope.following.people = [];
            $scope.following.page   = 1;
            $scope.following.busy   = false;
            $scope.following.more();
        }
    };

    $scope.following.reset();

}]);