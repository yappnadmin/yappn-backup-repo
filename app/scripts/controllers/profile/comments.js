'use strict';

angular.module('app')
.controller('Profile.Comments', ['$scope', '$stateParams', '$log', '$yappn', '$q', '$sanitize', 'Auth', function($scope, $stateParams, $log, $yappn, $q, $sanitize, Auth) {

    //$log.debug('stateParams from within controller: ', $stateParams);

    var can_user_edit = function (comment) {

        // console.log(comment);

        comment.edit      = '';
        comment.editable  = false;

        if (Auth.isLoggedIn() && $scope.currentUser) {
            if (comment.author._id.toString() === $scope.currentUser.id.toString()) {
                comment.edit      = 'author';
                comment.editable  = true;
            } else if ($scope.currentUser.role === 'admin') {
                comment.edit      = 'admin';
                comment.editable  = true;
            } else if (comment.discussion.room.moderators.indexOf($scope.currentUser.id.toString()) !== -1) {
                comment.edit      = 'moderator';
                comment.editable  = true;
            }
        }
        return comment;
    };


    $scope.username  = $stateParams.person;
    $scope.comments = {

        posts   : [],
        busy    : false,
        page    : 1,
        more    : function () {

            if ($scope.comments.busy) {
                return;
            }

            $scope.comments.busy = true;

            $yappn.api('/api/forums/comments/by/' + $stateParams.person, 'GET', { limit: 10, page: $scope.comments.page })
            .success(function(response) {

                var p   = parseInt(response.data.page, 10);
                var ps  = parseInt(response.data.pages, 10);
                $scope.comments.page = p + 1;
                if (p < ps) {
                    $scope.comments.busy = false;
                } else {
                    $scope.comments.busy = true;
                }

                for(var i = 0; i < response.data.comments.length; i++) {
                    var c = can_user_edit(response.data.comments[i]);
                    $scope.comments.posts.push(c);
                }

                $scope.$emit('$profileStateChangeSuccess');


            });
        },
        reset   : function () {
            $scope.comments.posts = [];
            $scope.comments.page = 1;
            $scope.comments.busy = false;
            $scope.comments.more();
        }
    };

    $scope.deletePost   = function(idx, postId) {
        $yappn.api('/api/forums/comment/' + postId, 'delete', {}).success(function() {
            $scope.comments.posts.splice(idx, 1);
        });
    };

    $scope.removeImage = function(idx, postId) {
        $yappn.api('/api/forums/comment/' + postId, 'put', { image: null, thumbnail: null }).success(function() {
            delete $scope.comments.posts[idx].thumbnail;
            delete $scope.comments.posts[idx].image;
        });
    };

    // inline editor
    $scope.updatePost   = function(id, content) {
        var d = $q.defer();
        $yappn.api('/api/forums/comment/' + id, 'put', { content: $sanitize(content) }).success(function(response) {
            d.resolve(response.data.comment);
        }).error(function(e) {
            d.reject(e.message);
        });
        return d.promise;
    };
    
    $scope.comments.reset();


}]);