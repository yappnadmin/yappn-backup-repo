'use strict';

angular.module('app')
.controller('Profile.Badges', ['$scope', '$stateParams', '$log', '$yappn', function($scope, $stateParams, $log, $yappn) {

    //$log.debug('stateParams from within controller: ', $stateParams);

    $scope.badges = [];
    $scope.username  = $stateParams.person;

    $yappn.api('/api/trophyroom/'+$stateParams.person)
    .success(function (response) {
        $scope.badges = response.data.badges;
        $scope.$emit('$profileStateChangeSuccess');
    })
    .error(function (response) {
        $log.error(response.message);
    });

}]);