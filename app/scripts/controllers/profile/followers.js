'use strict';

angular.module('app')
.controller('Profile.Followers', ['$scope', '$stateParams', '$log', '$yappn', function($scope, $stateParams, $log, $yappn) {

    //$log.debug('stateParams from within controller: ', $stateParams);

    $scope.username     = $stateParams.person;

    $scope.followers    = {
        people      : [],
        busy        : false,
        page        : 1,
        more        : function () {
            $log.debug('doGetMore() called.');

            if ($scope.followers.busy) {
                return;
            }

            $scope.followers.busy = true;

            var params = { limit: 20, page: $scope.followers.page };
            $yappn.api('/api/user/' + $scope.username + '/followers', 'GET', params)
            .success(function(response) {
                var p   = parseInt(response.data.page, 10);
                var ps  = parseInt(response.data.pages, 10);
                $scope.followers.page = p + 1;
                if (p < ps) {
                    $scope.followers.busy = false;
                } else {
                    $scope.followers.busy = true;
                }


                var people = response.data.followers;
                for (var i = 0; i < people.length; i++) {
                    $scope.followers.people.push(people[i]);
                }

                //console.log($scope.followers.people);

                $scope.$emit('$profileStateChangeSuccess');
            });
        },
        reset       : function () {
            $scope.followers.people = [];
            $scope.followers.page   = 1;
            $scope.followers.busy   = false;
            $scope.followers.more();
        }
    };

    $scope.followers.reset();

}]);