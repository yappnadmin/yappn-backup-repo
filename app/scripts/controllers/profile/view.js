'use strict';

angular.module('app')
.controller('Profile.View', ['$rootScope','$scope','$yappn','$location','$window','Person','Rank','Admin', '$state','$log','$stateParams','$timeout', function ($rootScope, $scope, $yappn, $location, $window, Person, Rank, Admin, $state, $log, $stateParams, $timeout) {
    $rootScope.page = 'profile';

    $scope.profile  = Person;
    $scope.ranked   = Rank;
    $scope.startedDiscussions = [];
    $scope.admin = Admin;
    //console.log('admin: ' + Admin);
    //console.log(Person);

    $yappn.api('/api/forums/discussions/by/' + Person.username.toLowerCase(), 'GET', {}).success(function(response) {
        $scope.startedDiscussions = response.data.discussions;
    });

    $scope.AccountBan = function (data) {
        // should probably add a growl message here.
        //$log.warn('banning user');
        $yappn.api('/api/user/'+data.person+'/ban', 'GET', {}).success(function() {
            $window.location.reload();
        });
    };

    $scope.AccountRemoveBan = function (data) {
        // should probably add a growl message here.
        //$log.warn('un-banning user');
        $yappn.api('/api/user/'+data.person+'/unban', 'GET', {}).success(function() {
            $window.location.reload();
        });
    };

    
    $scope.$on('$profileStateChangeSuccess', function() {
        $log.warn('calling reload');
        $timeout(function() {
            $scope.$broadcast('masonry.reload');
        }, 500);
        
    });

    $scope.$emit('$profileStateChangeSuccess');

}]);
