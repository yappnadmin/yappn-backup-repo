'use strict';



angular.module('app')

.controller('Auth.Login.Integrated', ['$rootScope', '$scope', 'Auth', 'YappnConfig', '$log', '$yappn', '$modalInstance', function($rootScope, $scope, Auth, YappnConfig, $log, $yappn, $modalInstance) {
    
    $scope.errors = {};
    $scope.credentials = { email: '', password: '' };
    $scope.base = YappnConfig.getRootUrl();

    $scope.login = function(form) {
        $scope.submitted = true;
        
        if (form.$valid) {
            Auth.login($scope.credentials)
            .then(function() {
                $rootScope.$emit('$AuthStateChanged');
                $modalInstance.close();
            })
            .catch(function(err) {
                err = err.data;
                $scope.errors.other = err.message;
            });
        }
    };

    $scope.twitter = function() {
        window.OAuth.popup('twitter', function(err, result) {
            
            if (err) { $log.warn(err); }

            result.get('/1.1/account/verify_credentials.json').done(function(response) {

                $rootScope.flash = '';

                var profile = {};
                profile.id = response.id_str;
                profile.username = response.screen_name;
                profile.displayName = response.name;
                profile.gender = (response.gender || 'male');
                profile.profileUrl = response.link;
                profile.avatar = response.profile_image_url;
                profile.lang = response.lang;
                
                profile.provider = 'twitter';
                profile._raw = response;
                profile._json = response;

                
                $yappn.bringMyOwnIdentity('twitter', profile).success(function(user) {
                    $rootScope.currentUser = user;
                    $rootScope.$emit('$AuthStateChanged');
                    $modalInstance.close();
                });
            
                
            });
        });
    };

    $scope.facebook = function() {
        window.OAuth.popup('facebook', function(err, result) {
    
            if (err) { $log.warn(err); }

            result.get('/me').done(function(response) {

                $rootScope.flash = '';

                var profile = {};
                profile.id = response.id;
                profile.username = response.username || 'FB' + response.id;
                profile.displayName = response.name || 'Facebook User';
                profile.gender = response.gender || 'male';
                profile.profileUrl = response.link || '';
                
                if (response.email) {
                    profile.emails = [{ value: response.email }];
                }

                if (response.picture) {
                    if (typeof response.picture === 'object' && response.picture.data) {
                        // October 2012 Breaking Changes
                        profile.photos = [{ value: response.picture.data.url }];
                    } else {
                        profile.photos = [{ value: response.picture }];
                    }
                }

                profile.provider = 'facebook';
                profile._raw = response;
                profile._json = response;

                
                $yappn.bringMyOwnIdentity('facebook', profile).success(function(user) {
                    $rootScope.currentUser = user;
                    $rootScope.$emit('$AuthStateChanged');
                    $modalInstance.close();
                });
            });
        });
    };
}])

.controller('IntegratedController', ['$scope', '$rootScope', '$yappn', '$location', 'toaster', '$translate', 'Auth', '$modal', function($scope, $rootScope, $yappn, $location, toaster, $translate, Auth, $modal) {

    window.$rootScope = $rootScope;

    $rootScope.$on('apiErrorFound', function(event, data) {
        if (data && data.length > 0) {
            toaster.pop('error', '', $translate(data));
        }
    });

    $scope.logout = function() {
        Auth.logout(function() {
            $yappn.api('/logout').success(function() {
                $rootScope.$emit('$AuthStateChanged');
            });
        });
    };

    $scope.loginForm = function() {

        $modal.open({
            templateUrl: 'partials/authentication/login-integrated.html',
            size: 'sm',
            controller: 'Auth.Login.Integrated'
        });
    };

}]);