'use strict';

angular.module('app')
.controller('Auth.Register', ['$rootScope','$scope','$location','$yappn','FlashService', '$timeout', '$log', 'Auth','$window', function($rootScope, $scope, $location, $yappn, FlashService, $timeout, $log, Auth, $window) {
    $scope.user = {};
    $scope.errors = {};

    if (Auth.isLoggedIn()) {
        $location.path('/');
    }

    $rootScope.page = 'register';

    $scope.newuser = {
        username: '',
        password: '',
        email: '',
        birthdate: '',
        gender: 'male',
        lang: 'en',
        displayName: '',
        location: ''
    };

    $timeout(function() {

        window.geoip2.country(function(geoipResponse) {
            $scope.newuser.location = geoipResponse.country.names.en;
        }, function(err) {
            $log.error(err);
            return;
        });

    }, 1000);

    $scope.register = function (form) {
        $scope.submitted = true;

        if (form.$valid) {
            Auth.createUser($scope.newuser)
            .then(function() {
                $window.location.href = '/?registered=true';
            })
            .catch(function(err) {
                //console.log(err);
                err = err.data.error;
                $scope.errors = {};

                // Update validity of form fields that match the mongoose errors
                angular.forEach(err.errors, function(error, field) {
                    form[field].$setValidity('mongoose', false);
                    $scope.errors[field] = error.message;
                });
            });
        }
    };
}]);
