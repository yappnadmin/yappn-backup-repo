'use strict';

angular.module('app')
.controller('Auth.Forgot', [
    '$scope',
    '$location',
    '$q',
    '$sanitize',
    '$yappn',
    '$translate',
    function($scope, $location, $q, $sanitize, $yappn, $translate) {

    // setup the default var.
        $scope.email = '';
        $scope.success = '';

        // form submitting handler
        $scope.submit = function(form) {
            $scope.submitted = true;

            if (form.$valid) {
                var d = $q.defer();
                $yappn.api('/api/reset', 'POST', { email: $sanitize($scope.email) }).success(function() {
                    d.resolve();
                    $scope.success = $translate('auth.forgot.success');
                }).error(function(e) {
                    d.reject(e.message);
                });

                return d.promise;
            }
        };
    }
]);