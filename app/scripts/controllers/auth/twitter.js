'use strict';

angular.module('app')
.controller('Auth.Twitter', ['$scope','$rootScope','$location','$yappn', '$log', function($scope, $rootScope, $location, $yappn, $log) {

    window.OAuth.popup('twitter', function(err, result) {
        
        if (err) { $log.warn(err); }

        result.get('/1.1/account/verify_credentials.json').done(function(response) {

            $rootScope.flash = '';

            var profile = {};
            profile.id = response.id_str;
            profile.username = response.screen_name;
            profile.displayName = response.name;
            profile.gender = (response.gender || 'male');
            profile.profileUrl = response.link;
            profile.avatar = response.profile_image_url;
            profile.lang = response.lang;
            
            profile.provider = 'twitter';
            profile._raw = response;
            profile._json = response;

            
            $yappn.bringMyOwnIdentity('twitter', profile).success(function() {
                $location.path('/');
            });
        
            
        });
    });

}]);