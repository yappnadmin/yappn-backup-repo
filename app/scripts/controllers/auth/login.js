'use strict';

angular.module('app')
    .controller('Auth.Login', function ($rootScope, $scope, Auth, $location) {

        $scope.user = {};
        $scope.errors = {};

        $rootScope.page = 'login';
        $scope.credentials = { email: '', password: '' };

        $scope.login = function(form) {
            $scope.submitted = true;
            
            if (form.$valid) {
                Auth.login($scope.credentials)
                .then(function() {
                    $location.path('/');
                })
                .catch(function(err) {
                    err = err.data;
                    $scope.errors.other = err.message;
                });
            }
        };

    });