'use strict';

angular.module('app')
.controller('Auth.Facebook', ['$scope','$rootScope','$location','$yappn', '$log', function($scope, $rootScope, $location, $yappn, $log) {

    window.OAuth.popup('facebook', function(err, result) {
        
        if (err) { $log.warn(err); }

        result.get('/me').done(function(response) {

            $rootScope.flash = '';

            var profile = {};
            profile.id = response.id;
            profile.username = response.username || 'FB' + response.id;
            profile.displayName = response.name || 'Facebook User';
            profile.gender = response.gender || 'male';
            profile.profileUrl = response.link || '';
            
            if (response.email) {
                profile.emails = [{ value: response.email }];
            }

            if (response.picture) {
                if (typeof response.picture === 'object' && response.picture.data) {
                    // October 2012 Breaking Changes
                    profile.photos = [{ value: response.picture.data.url }];
                } else {
                    profile.photos = [{ value: response.picture }];
                }
            }

            profile.provider = 'facebook';
            profile._raw = response;
            profile._json = response;

            
            $yappn.bringMyOwnIdentity('facebook', profile).success(function() {
                $location.path('/');
            });
        });
    });

}]);