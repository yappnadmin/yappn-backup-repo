'use strict';
/*jshint unused:false*/

angular.module('app')
.controller('Inbox.Conversation', ['$scope','$yappn','$timeout','Conversations','Conversation','$state','$stateParams','$fileUploader','$log','$sce','$q','$sanitize', 'Auth', function($scope, $yappn, $timeout, Conversations, Conversation, $state, $stateParams, $fileUploader, $log, $sce, $q, $sanitize, Auth) {

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };

        $scope.newpost = { body: '' };
        $scope.canSubmit = false;

        $scope.$watch(function() {
            return $scope.newpost.body;
        }, function() {
            $scope.canSubmit = ($scope.newpost.body && $scope.newpost.body.length > 4);
        });

        var can_user_edit = function (p) {
            p.edit      = '';
            p.editable  = false;

            if (Auth.isLoggedIn() && $scope.currentUser) {
                $log.debug('looking in can_user_edit');
                if (p.user === $scope.currentUser.username.toLowerCase()) {
                    p.edit      = 'author';
                    p.editable  = true;
                }
            } else {
                $log.debug('noop');
            }
            return p;
        };

        // console.log(Conversation.comments[0]);

        $scope.conversation = {
            discussion: Conversation,
            posts: [],
        };

        // iterate over comments.

        for (var i = 0; i < Conversation.comments.length; i++) {
            var p = can_user_edit(Conversation.comments[i]);
            $scope.conversation.posts.unshift(p);
        }

        $scope.openFileDialog = function() {
            // console.log('i am opening the dialog');
            document.getElementById('ref_'+Conversation._id+'_file').click();
        };

        $scope.cbxSubmit = function() {
            $log.info('submitting the comment form in PM.');

            var send = {
                body: $scope.newpost.body
            };

            // if they exist, delete them.
            if ($scope.newpost.thumbnail) {
                send.thumbnail = { url: $scope.newpost.thumbnail.url };
            }

            if ($scope.newpost.image) {
                send.image = { url: $scope.newpost.image.url };
            }

            // console.log('data', send);

            $yappn.api('/api/conversation/'+Conversation._id+'/comment', 'POST', send).success(function() {
                $scope.newpost.body = '';
                // if they exist, delete them.
                if ($scope.newpost.thumbnail) {
                    delete $scope.newpost.thumbnail;
                }

                if ($scope.newpost.thumbnail) {
                    delete $scope.newpost.thumbnail;
                }

                var current = $state.current;
                var params = angular.copy($stateParams);
                $state.transitionTo(current, params, { reload: true, inherit: true, notify: true });

            });
        };

        var btn         = window.jQuery('#ref_' + Conversation._id + '_btn');

        // upload
        var uploader = $scope.uploader = $fileUploader.create({
            scope:      $scope,
            url:        '//media.yappn.com',
            autoUpload: true,
            removeAfterUpload: true
        });

        uploader.bind('beforeupload', function (event, item) {
            //console.info('Before upload', item);
            $timeout(function() { btn.attr('disabled', true); });
        });

        uploader.bind('cancel', function (event, xhr, item) {
            //console.info('Cancel', xhr, item);
            $timeout(function() { btn.removeAttr('disabled'); });
        });

        uploader.bind('error', function (event, xhr, item, response) {
            //console.info('Error', xhr, item, response);
            $timeout(function() { btn.removeAttr('disabled'); });
        });

        uploader.bind('complete', function (event, xhr, item, response) {
            $timeout(function() {
                if (response.files && response.files.length === 1) {
                    // console.log('completed!');
                    $scope.newpost.image = { url: response.files[0].url };
                    $scope.newpost.thumbnail = { url: response.files[0].thumbnailUrl };
                }
            });
        });

        $scope.updateConversationComment   = function(idx, id, content) {
            var d = $q.defer();
            $yappn.api('/api/conversation/'+$scope.conversation.discussion._id+'/comment/' + id, 'put', { content: content }).success(function(response) {
                d.resolve();
                $scope.conversation.posts.splice(idx, 1); // remove the old
                $scope.conversation.posts.splice(idx, 0, can_user_edit(response.data.comment)); // in with the new
            }).error(function(e) {
                d.reject(e.message);
            });
            return d.promise;
        };

        $scope.deleteConversationComment   = function(idx, postId) {
            $yappn.api('/api/conversation/'+$scope.conversation.discussion._id+'/comment/' + postId, 'delete', {}).success(function() {
                $scope.conversation.posts.splice(idx, 1);
            });
        };
    }
]);