'use strict';

/*jshint unused:false*/

angular.module('app')
.controller('Inbox.Compose', ['$scope','$yappn','$timeout','Conversations','$state','$stateParams','$fileUploader','$log', function($scope, $yappn, $timeout, Conversations, $state, $stateParams, $fileUploader, $log) {

    $scope.recipient = $stateParams.username.toLowerCase();

    $scope.newpost = { body: '' };
    $scope.canSubmit = false;

    $scope.$watch(function() {
        return $scope.newpost.body;
    }, function() {
        $scope.canSubmit = ($scope.newpost.body && $scope.newpost.body.length > 4);
    });

    $scope.openFileDialog = function() {
        // console.log('i am opening the dialog');
        document.getElementById('ref_'+$scope.recipient+'_file').click();
    };

    $scope.cbxSubmit = function() {
        $log.info('submitting the comment form in PM.');

        var send = {
            body: $scope.newpost.body,
            recipients: []
        };

        send.recipients.push($stateParams.username.toLowerCase());

        // if they exist, delete them.
        if ($scope.newpost.thumbnail) {
            send.thumbnail = { url: $scope.newpost.thumbnail.url };
        }

        if ($scope.newpost.image) {
            send.image = { url: $scope.newpost.image.url };
        }

        // console.log('data', send);

        $yappn.api('/api/conversations', 'POST', send).success(function(response) {

            $scope.newpost.body = '';
            // if they exist, delete them.
            if ($scope.newpost.thumbnail) {
                delete $scope.newpost.thumbnail;
            }

            if ($scope.newpost.thumbnail) {
                delete $scope.newpost.thumbnail;
            }

            $state.transitionTo('Inbox.Show', { conversation: response.data.conversation._id }, { reload: true, inherit: false, notify: true });

        });
    };

    var btn         = window.jQuery('#ref_' + $scope.recipient + '_btn');

    // upload
    var uploader = $scope.uploader = $fileUploader.create({
        scope:      $scope,
        url:        '//media.yappn.com',
        autoUpload: true,
        removeAfterUpload: true
    });

    uploader.bind('beforeupload', function (event, item) {
        //console.info('Before upload', item);
        $timeout(function() { btn.attr('disabled', true); });
    });

    uploader.bind('cancel', function (event, xhr, item) {
        //console.info('Cancel', xhr, item);
        $timeout(function() { btn.removeAttr('disabled'); });
    });

    uploader.bind('error', function (event, xhr, item, response) {
        //console.info('Error', xhr, item, response);
        $timeout(function() { btn.removeAttr('disabled'); });
    });

    uploader.bind('complete', function (event, xhr, item, response) {
        $timeout(function() {
            if (response.files && response.files.length === 1) {
                // console.log('completed!');
                $scope.newpost.image = { url: response.files[0].url };
                $scope.newpost.thumbnail = { url: response.files[0].thumbnailUrl };
            }
        });
    });

}]);