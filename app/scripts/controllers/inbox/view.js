'use strict';

angular.module('app')
.controller('Inbox.View', [
    '$scope', '$yappn', '$timeout', 'Conversations', '$log',
    function ($scope, $yappn, $timeout, Conversations, $log) {

        $scope.currentPage      = 1;
        $scope.pageSize         = 5;
        $scope.conversations    = [];
        $scope.total            = Conversations.length;

        $scope.$watch('currentPage', function() {
            $log.info('Conversations.slice('+($scope.currentPage - 1) * $scope.pageSize+')');
            $scope.conversations = Conversations.slice( ($scope.currentPage - 1) * $scope.pageSize);
        });

    }
]);