'use strict';

angular.module('app')
.controller('AccountView.Info', ['$rootScope', '$scope','$location','$yappn','$log','Account','toaster','$translate',
function($rootScope, $scope, $location, $yappn, $log, Account, toaster, $translate) {
    $scope.profile = Account;

    $scope.updateInfo = function () {

        $yappn.api('/api/user/' + Account.username_idx, 'PUT', Account)
        .success(function(response) {
            var data = response.data;
            Account = data.user;
            $rootScope.currentUser = response.data.user;
            toaster.pop('success', $translate('global.toaster.success'));
        })
        .error(function(response) {
            toaster.pop('error', $translate('global.toaster.failure'));
            $log.error(response);
        });

    };

}]);