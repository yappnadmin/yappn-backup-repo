'use strict';

angular.module('app')
.controller('AccountView.Password', ['$scope','$location','$yappn','$log','Account', 'Auth', 'toaster', '$translate',
function($scope, $location, $yappn, $log, Account, Auth, toaster, $translate) {
    
    $scope.local = (Account.provider === 'local') ? true : false;

    $scope.passdata = {
        oldPassword:    '',
        newPassword:    '',
        newPassword2:   ''
    };

    $scope.updatePassword = function() {

        $yappn.api('/api/me/password', 'PUT', $scope.passdata)
        .success(function() {
            toaster.pop('info', $translate('account.password.changed.success'));
            $location.path('/account');
        });

    };


}]);