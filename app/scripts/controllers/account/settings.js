'use strict';

angular.module('app')
.controller('AccountView.Settings', ['$scope','$location','$yappn','$log','Account', '$translate', 'toaster',
function($scope, $location, $yappn, $log, Account, $translate, toaster) {
    
    $scope.local = (Account.provider === 'local') ? true : false;
    $scope.profile = Account;

    $scope.notifications = [
        { id: 'mention', text: $translate('account.form.notifications.label.mention') },
        { id: 'badge', text: $translate('account.form.notifications.label.badge') },
        { id: 'conversation', text: $translate('account.form.notifications.label.conversation') },
        { id: 'discussion', text: $translate('account.form.notifications.label.discussion') },
        { id: 'follower', text: $translate('account.form.notifications.label.follower') }
    ];

    $scope.user = {
        notifications: []
    };

    $scope.newsletter = Account.notifications.newsletter || false;

    if (Account.notifications.mention) {
        $scope.user.notifications.push('mention');
    }

    if (Account.notifications.badge) {
        $scope.user.notifications.push('badge');
    }

    if (Account.notifications.conversation) {
        $scope.user.notifications.push('conversation');
    }

    if (Account.notifications.discussion) {
        $scope.user.notifications.push('discussion');
    }

    if (Account.notifications.follower) {
        $scope.user.notifications.push('follower');
    }

    $scope.updateSettings = function () {

        Account.notifications.mention       = ($scope.user.notifications.indexOf('mention') !== -1) ? true : false;
        Account.notifications.badge         = ($scope.user.notifications.indexOf('badge') !== -1) ? true : false;
        Account.notifications.conversation  = ($scope.user.notifications.indexOf('conversation') !== -1) ? true : false;
        Account.notifications.discussion    = ($scope.user.notifications.indexOf('discussion') !== -1) ? true : false;
        Account.notifications.follower      = ($scope.user.notifications.indexOf('follower') !== -1) ? true : false;
        Account.notifications.newsletter    = ($scope.newsletter) ? true : false;

        $yappn.api('/api/user/' + Account.username_idx, 'PUT', Account)
        .success(function() {
            toaster.pop('success', $translate('global.toaster.success'));
        })
        .error(function(response) {
            toaster.pop('error', $translate('global.toaster.failure'));
            $log.error(response);
        });
    };


}]);