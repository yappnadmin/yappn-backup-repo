'use strict';

angular.module('app')
.controller('AccountView.Feed', ['$scope','$location','$yappn','$log','Account', 'toaster', '$translate',
function($scope, $location, $yappn, $log, Account, toaster, $translate) {

    if (!Account.feed) {
        Account.feed = [];
    }

    $scope.profile = Account;

    //console.log('feed: ', Account.feed);

    $scope.updateFeedTags = function() {

        var finalTags = [];

        //console.log($scope.profile.feed);

        $scope.profile.feed.forEach(function(tag) {
            finalTags.push(tag.text.toLowerCase());
        });


        $yappn.api('/api/user/' + Account.username_idx, 'PUT', { feed: finalTags })
        .success(function() {
            toaster.pop('success', $translate('global.toaster.success'));
        })
        .error(function(response) {
            toaster.pop('error', $translate('global.toaster.failure'));
            $log.error(response);
        });
    };

}]);