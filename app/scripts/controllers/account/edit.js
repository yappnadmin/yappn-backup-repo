'use strict';

angular.module('app')
.controller('AccountView', ['$rootScope', '$scope','$location','$yappn','$log','Account', '$translate','$fileUploader', 'toaster', '$timeout',
function($rootScope, $scope, $location, $yappn, $log, Account, $translate, $fileUploader, toaster, $timeout) {
    
    $scope.local = (Account.provider === 'local') ? true : false;
    
    var locateMe = function(event) {

        if (angular.isDefined(event)) {
            event.preventDefault();
        }

        window.geoip2.country(function(geoipResponse) {
            $scope.profile.location.displayName = geoipResponse.country.names.en;
            $scope.profile.location.ipaddress = geoipResponse.traits.ip_address;
        }, function(err) {
            $log.error(err);
            return;
        });

    };

    $scope.locateMe = locateMe;

    //console.log('account', Account);

    if (!Account.location || typeof Account.location.displayName === 'undefined') {
        locateMe();
    }

    $scope.profile = Account;

    $scope.isActive = function(route) {
        return route === $location.path();
    };

    $scope.updateAccount = function () {

        $scope.profile.language = $scope.profile.lang;

        // update!
        $yappn.api('/api/user/' + Account.username_idx, 'PUT', $scope.profile)
        .success(function(response) {
            // ok, user changed their preferred language.
            
            // todo: reset session on the front end?
            $rootScope.currentUser = response.data.user;
            Account = response.data.user;
            toaster.pop('success', $translate('global.toaster.success'));
        })
        .error(function(response) {
            toaster.pop('error', $translate('global.toaster.failure'));
            $log.error(response);
        });
    
    };

    $scope.options = {
        gender: [
            { text: $translate('account.form.label.gender.male'), value: 'male' },
            { text: $translate('account.form.label.gender.female'), value: 'female' }
        ],
        languages: $yappn.getLanguages()
    };

    $scope.openFileDialog = function() {
        document.getElementById('profile_picture_hidden').click();
    };

    // upload
    var uploader = $scope.uploader = $fileUploader.create({
        scope:      $scope,
        url:        '//media.yappn.com',
        autoUpload: true,
        removeAfterUpload: true
    });

    uploader.bind('complete', function (event, xhr, item, response) {
        $timeout(function() {
            if (response.files && response.files.length === 1) {
                $scope.profile.image = { url: response.files[0].url };
            }
        });
    });

}]);