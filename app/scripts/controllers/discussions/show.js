'use strict';

angular.module('app')
.controller('Discussions.Show', ['$rootScope','$scope','$yappn','$route','$timeout','$interval','$sanitize','$q','FlashService','linkify','Discussion','$fileUploader','$log','$location','$sce', 'Auth', function($rootScope, $scope, $yappn, $route, $timeout, $interval, $sanitize, $q, FlashService, linkify, Discussion, $fileUploader, $log, $location, $sce, Auth) {

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    };

    // // console.log('Discussion.Show initialized();');
    
    $scope.newpost = { content: '' };
    $scope.canSubmit = false;

    $scope.$watch(function() {
        return $scope.newpost.content;
    }, function() {
        $scope.canSubmit = ($scope.newpost.content && $scope.newpost.content.length > 4);
    });

    if (Auth.isLoggedIn()) {



        if ($rootScope.currentUser.role === 'admin') {
            Discussion.editable = true;
            Discussion.edit     = 'admin';
        }
        else if ($rootScope.currentUser.id === Discussion.author._id.toString()) {
            Discussion.editable = true;
            Discussion.edit     = 'author';
        }
        else if (Discussion.room.moderators.indexOf($rootScope.currentUser.id) !== -1) {
            Discussion.editable = true;
            Discussion.edit     = 'moderator';
        } else {
            Discussion.editable = false;
        }

    } else {
        Discussion.editable = false;
    }

    var btn         = window.jQuery('#ref_' + Discussion._id + '_btn');

    $scope.discussion = {
        discussion      : Discussion,
        posts           : [],
        busy            : false,
        after           : '',
        canLoad         : true,
        maxItems        : 100,
        newest_id       : '',
        load            : function () {
            if (this.busy || !this.canLoad) {
                return;
            }

            this.busy     = true;
            this.canLoad  = true;

            var that = this;
            $yappn.api('/api/forums/comments/' + this.discussion._id + '?limit=10&after=' + this.after)
            .success(function(response) {

                for (var i = 0; i < response.data.comments.length; i++) {
                    var p = can_user_edit(response.data.comments[i]);
                    that.posts.push(p);
                }

                that.after = response.data.next;
                if (response.data.next === '') {
                    that.after    = '';
                    that.busy     = true;
                    that.canLoad  = false;
                } else {
                    that.after    = response.data.next;
                    that.busy     = false;
                    that.canLoad  = true;
                }
                
            })
            .error(function() {
                that.busy     = true;
                that.canLoad  = false;
                FlashService.show('There was an error loading comments.');
            });
        }
    };

    // inline editor
    $scope.updatePost   = function(idx, id, content) {
        var d = $q.defer();
        $yappn.api('/api/forums/comment/' + id, 'put', { content: content }).success(function(response) {
            d.resolve();
            $scope.discussion.posts.splice(idx, 1); // remove the old
            $scope.discussion.posts.splice(idx, 0, can_user_edit(response.data.comment)); // in with the new
        }).error(function(e) {
            d.reject(e.message);
        });
        return d.promise;
    };

    $scope.reactPost = function(e, kind, idx, postId) {
        e.preventDefault();

        var url = '/api/forums/comment/' + postId + '/' + kind;
        $yappn.api(url, 'post', {})
        .success(function(res) {
            var elem = angular.element(e.srcElement);
            if (parseInt(res.code, 10) === 201) { // reaction took
                // // console.log('need to add one');
                elem.addClass('active');
            } else { // reaction was removed
                // // console.log('need to remove');
                elem.removeClass('active');
            }
        });
    };

    $scope.deletePost   = function(idx, postId) {
        $yappn.api('/api/forums/comment/' + postId, 'delete', {}).success(function() {
            $scope.discussion.posts.splice(idx, 1);
        });
    };

    $scope.removeImage = function(idx, postId) {
        $yappn.api('/api/forums/comment/' + postId, 'put', { image: null, thumbnail: null }).success(function() {
            delete $scope.discussion.posts[idx].thumbnail;
            delete $scope.discussion.posts[idx].image;
        });
    };

    $scope.deleteDiscussion = function(discussionId, e) {
        e.preventDefault();
        $yappn.api('/api/forums/discussion/'+discussionId, 'DELETE', {}).success(function() {
            $scope.$emit('event::discussion::removed', { id: discussionId });
            $location.path('/');
        });
    };

    $scope.reset        = function () {
        $scope.discussion.posts    = [];
        $scope.discussion.canLoad  = true;
        $scope.discussion.after    = '';
        $scope.discussion.load();
    };

    var can_user_edit = function (p) {
        p.edit      = '';
        p.editable  = false;

        if (Auth.isLoggedIn() && $scope.currentUser) {
            if (p.author._id.toString() === $rootScope.currentUser.id.toString()) {
                p.edit      = 'author';
                p.editable  = true;
            } else if ($rootScope.currentUser.role === 'admin') {
                p.edit      = 'admin';
                p.editable  = true;
            } else if ($scope.discussion.discussion.room.moderators.indexOf($rootScope.currentUser.id.toString()) !== -1) {
                p.edit      = 'moderator';
                p.editable  = true;
            }
        }
        return p;
    };

    // ----------------------------------------------------------------
    // Check for new messages, interval-based until realtime is ready

    var check;

    var checkForNewMessages = function() {

        var last_known_id = ($scope.discussion.posts.length) ? $scope.discussion.posts[0]._id : '';
        $yappn.api('/api/forums/comments/' + $scope.discussion.discussion._id + '?limit=25&direction=new&after=' + last_known_id).success(function (response) {
            $scope.discussion.available = response.data.comments;
        });
    };

    $scope.mergeNewMessages = function() {
        var messages = $scope.discussion.available;
        messages.reverse();
        for (var i = 0; i < messages.length; i++) {
            var p = can_user_edit(messages[i]);
            $scope.discussion.posts.unshift(p);
        }
        $scope.discussion.available = [];
    };

    var checkAndMerge = function () {
        var last_known_id = ($scope.discussion.posts.length) ? $scope.discussion.posts[0]._id : '';
        $yappn.api('/api/forums/comments/' + $scope.discussion.discussion._id + '?limit=25&direction=new&after=' + last_known_id).success(function (response) {
            $scope.discussion.available = response.data.comments;
            $scope.mergeNewMessages();
        });
    };

    $scope.startChecking = function () {
        // dont check if we are already checking.
        if (angular.isDefined(check)) { return; }
        check = $interval(checkForNewMessages, 45000);
    };

    $scope.stopChecking = function () {
        if (angular.isDefined(check)) {
            $interval.cancel(check);
            check = undefined;
        }
    };

    $scope.$on('$destroy', function() {
        // Make sure that the interval is destroyed too
        $scope.stopChecking();
    });

    $scope.startChecking();

    $scope.submit = function () {
        checkAndMerge();
    };

    $scope.reset();

    // --- uploader
    $scope.cbxSubmit = function() {
                
        // console.log('i am submitting the form.');

        var send = {
            referenceTo: Discussion._id,
            reference: 'discussions',
            content: $scope.newpost.content,
            lang: $scope.lang
        };

        // if they exist, delete them.
        if ($scope.newpost.thumbnail) {
            send.thumbnail = { url: $scope.newpost.thumbnail.url };
        }

        if ($scope.newpost.thumbnail) {
            send.image = { url: $scope.newpost.image.url };
        }

        $yappn.api('/api/forums/comments', 'POST', send).success(function() {
            // console.log('i am back from submitting.');
            $scope.newpost.content = '';

            // if they exist, delete them.
            if ($scope.newpost.thumbnail) {
                delete $scope.newpost.thumbnail;
            }

            if ($scope.newpost.image) {
                delete $scope.newpost.image;
            }

            $timeout(function() {
                $scope.submit();
            });
        });

    };

    $scope.openFileDialog = function() {
        // console.log('i am opening the dialog');
        document.getElementById('ref_'+Discussion._id+'_file').click();
    };

    // upload
    var uploader = $scope.uploader = $fileUploader.create({
        scope:      $scope,
        url:        '//media.yappn.com',
        autoUpload: true,
        removeAfterUpload: true
    });

    uploader.bind('beforeupload', function (/*event, item*/) {
        //console.info('Before upload', item);
        $timeout(function() { btn.attr('disabled', true); });
    });

    uploader.bind('cancel', function (/*event, xhr, item*/) {
        //console.info('Cancel', xhr, item);
        $timeout(function() { btn.removeAttr('disabled'); });
    });

    uploader.bind('error', function (/*event, xhr, item, response*/) {
        //console.info('Error', xhr, item, response);
        $timeout(function() { btn.removeAttr('disabled'); });
    });

    uploader.bind('complete', function (event, xhr, item, response) {
        $timeout(function() {
            if (response.files && response.files.length === 1) {
                // console.log('completed!');
                $scope.newpost.image = { url: response.files[0].url };
                $scope.newpost.thumbnail = { url: response.files[0].thumbnailUrl };
            }
        });
    });

}]);