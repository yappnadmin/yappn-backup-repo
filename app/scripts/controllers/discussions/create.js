'use strict';

angular.module('app')
.controller('Discussions.Create', ['$rootScope','$scope','$timeout','$yappn','$location','$sanitize','$fileUploader','$log','$state', function($rootScope, $scope, $timeout, $yappn, $location, $sanitize, $fileUploader, $log, $state) {

    $rootScope.page = 'discussions';

    $log.debug('Discussion.Create controller initialized.');

    $scope.id = Math.floor((Math.random()*1000000)+1);

    // variables
    $scope.canSubmit = false;

    $scope.discussion = {
        displayName: '',
        content: '',
        image: { url: '' },
        tags: [],
        lang: $rootScope.lang
    };

    $scope.$watch(function() {
        return $scope.discussion.content;
    }, function() {
        $scope.canSubmit = ($scope.discussion.content && $scope.discussion.content.length > 4);
    });

    $scope.openFileDialog = function() {
        // // console.log('i am opening the dialog');
        document.getElementById('ref_'+$scope.id+'_file').click();
    };

    // upload
    var uploader = $scope.uploader = $fileUploader.create({
        scope:      $scope,
        url:        '//media.yappn.com',
        autoUpload: true,
        removeAfterUpload: true,
        filters:    [
            function () {
                return true; // return false to cancel.
            }
        ]
    });
    
    var btn         = window.jQuery('#ref_' + $scope.id + '_btn');

    uploader.bind('beforeupload', function () {
        btn.attr('disabled', true);
    });

    uploader.bind('cancel', function () {
        btn.removeAttr('disabled');
    });

    uploader.bind('error', function () {
        btn.removeAttr('disabled');
    });

    uploader.bind('complete', function (event, xhr, item, response) {
        
        if (response.files && response.files.length === 1) {
            // // console.log('here!');
            $scope.discussion.image = { url: response.files[0].url };
            $scope.discussion.thumbnail = { url: response.files[0].thumbnailUrl };
        }

    });

    $scope.submit = function() {
        
        // // console.log('im submitting now.');

        var finalTags = [];
        $scope.discussion.tags.forEach(function(tag) {
            finalTags.push(tag.text.toLowerCase());
        });

        var output = $scope.discussion;

        output.tags = finalTags;
        
        $yappn.api('/api/forums/discussions', 'POST', output).success(function(response) {
            // // console.log('i am back from submitting.');
            // // console.log('emit a checkAndMerge on discussions');
            $scope.$emit('event::discussion::created');
            // finally, lets get out of here.
            //$location.path('/');
            $state.go('DiscussionView', { discussion: response.data.discussion._id });
        });

    };

}]);