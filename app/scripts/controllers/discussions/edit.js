'use strict';

angular.module('app')
.controller('Discussions.Edit', ['$rootScope','$scope','$location','$timeout','$yappn','FlashService','Discussion','$sanitize','$fileUploader', function($rootScope, $scope, $location, $timeout, $yappn, FlashService, Discussion, $sanitize, $fileUploader) {

    $rootScope.page = 'discussions';

    $scope.discussion = Discussion;

    //console.log('tags: ', Discussion.tags);

    $scope.id = Discussion._id.toString();

    $scope.canSubmit = false;

    $scope.$watch(function() {
        return $scope.discussion.content;
    }, function() {
        $scope.canSubmit = ($scope.discussion.content && $scope.discussion.content.length > 4);
    });

    $scope.openFileDialog = function() {
        // // console.log('i am opening the dialog');
        document.getElementById('ref_'+$scope.id+'_file').click();
    };

    // upload
    var uploader = $scope.uploader = $fileUploader.create({
        scope:      $scope,
        url:        '//media.yappn.com',
        autoUpload: true,
        removeAfterUpload: true,
        filters:    [
            function () {
                return true; // return false to cancel.
            }
        ]
    });
    
    var btn         = window.jQuery('#ref_' + $scope.id + '_btn');

    uploader.bind('beforeupload', function () {
        btn.attr('disabled', true);
    });

    uploader.bind('cancel', function () {
        btn.removeAttr('disabled');
    });

    uploader.bind('error', function () {
        btn.removeAttr('disabled');
    });

    uploader.bind('complete', function (event, xhr, item, response) {
        
        if (response.files && response.files.length === 1) {
            $scope.discussion.image = { url: response.files[0].url };
            $scope.discussion.thumbnail = { url: response.files[0].thumbnailUrl };
        }

    });

    $scope.update = function() {

        var finalTags = [];

        $scope.discussion.tags.forEach(function(tag) {
            finalTags.push(tag.text.toLowerCase());
        });

        var output = $scope.discussion;

        output.tags = finalTags;
        
        $yappn.api('/api/forums/discussion/'+Discussion._id, 'PUT', output).success(function() {
            $scope.$emit('event::discussion::updated', { id: Discussion._id });
            // finally, lets get out of here.
            $location.path('/discussion/' + Discussion._id);
        });

    };

}]);