'use strict';

angular.module('app').directive('reaction', [function() {

    var directive = {
        restrict: 'E',
        transclude: false,
        replace: true,
        scope: {
            icon: '@',
            table: '@',
            collection: '=',
            reference: '=',
            kind: '@'
        },

        link: function (scope, elem) {
            elem.on('click', function (e) {
                e.preventDefault();
            });
        },

        controller: ['$rootScope', '$scope', '$yappn', 'Auth', function ($rootScope, $scope, $yappn, Auth) {

            $scope.active = (Auth.isLoggedIn() && $scope.collection.indexOf($rootScope.currentUser.id) !== -1) ? 'active' : '';
            $scope.count  = $scope.collection.length;

            $scope.react = function () {
                // // console.log('icon: ', $scope.icon);
                // // console.log('table: ', $scope.table);
                // // console.log('collection: ', $scope.collection);
                // // console.log('reference: ', $scope.reference);
                // // console.log('kind: ', $scope.kind);
                
                $yappn.api('/api/'+$scope.table+'/'+$scope.reference+'/'+$scope.kind, 'POST', {}).success(function(response) {
                    $scope.count = response.data.count;
                    //// // console.log(response);
                    if (response.code === 201) {
                        $scope.active = 'active';
                    } else {
                        $scope.active = '';
                    }
                });

            };

        }],

        template:   '<button ng-click="react()" class="btn btn-default btn-sm {{ active }}"><i class="fa {{icon}}"></i> <span ng-show="count">{{count}}</span></button>'
    };

    return directive;
}]);