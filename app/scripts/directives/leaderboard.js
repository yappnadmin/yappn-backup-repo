'use strict';

angular.module('app')
.directive('leaderboard', [
    function() {

        var directive = {

            restrict: 'E',
            transclude: false,
            replace: false,

            link: function(scope, elem, attrs) {
                scope.rank      = 0;
                scope.limit     = attrs.limit;
                scope.username  = attrs.username;
                scope.leaders   = [];
            },

            controller: ['$scope','$yappn','$log','$timeout', 'FayeService', function($scope, $yappn, $log, $timeout, FayeService) {

                $scope.base_url = '/';


                $timeout(function() {

                    //$log.debug("limit: " + $scope.limit);


                    // ok, now lets get the data - initial data
                    $yappn.api('/api/ranking', 'GET', { limit: $scope.limit }).success(function(response) {
                        var members = response.data.leaderboard;
                        window.async.each(members, function(item, cb) {
                            item.image = '/avatar/' + item.member;
                            cb();
                        }, function() {
                            $scope.leaders = members;
                        });
                    });

                    FayeService.subscribe('/leaderboard', function(message) {
                        var members = message.leaderboard;
                        window.async.each(members, function(item, cb) {
                            item.image = '/avatar/' + item.member;
                            cb();
                        }, function() {
                            $scope.leaders = members;
                        });
                    });
                    

                });
            }]

        };

        return directive;

    }
]);