'use strict';

angular.module('app')
.directive(
    'comYappnUiCommonAddthis',
    function() {

        var sharebox = {
            restrict: 'A',
            transclude: true,
            replace: true,
            scope: {
                shareTitle: '=',
                shareDescription: '=',
                shareLang: '=',
                shareGoogleAnalytics: '='
            },
            template: '<div ng-transclude></div>',
            link: function ($scope, element) {

                var addthis_config = {
                    'data_track_addressbar':true,
                    'ui_language': $scope.shareLang,
                    'data_ga_property': $scope.shareGoogleAnalytics
                };

                var addthis_share = {
                    'title': $scope.shareTitle,
                    'url': document.location.href,
                    'description': $scope.shareDescription,
                    'templates': {
                        twitter: 'check out {{url}} (from @yappnCorp)'
                    }
                };

                window.addthis.init();
                window.addthis.toolbox(window.jQuery(element).get(), addthis_config, addthis_share);

                //addthis.toolbox($(element).get());
            }
        };

        return sharebox;
    }
);

/*
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-531627e76c79c99b"></script>
<!-- AddThis Button END -->

 */