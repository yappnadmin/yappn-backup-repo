'use strict';

angular.module('app').directive('userimage', [function () {

    var directive = {
        restrict: 'E',
        transclude: false,
        replace: true,
        scope: {
            imageurl: '@',
            imagebox: '@'
        },
        controller: ['$scope', function($scope) {
            if (!$scope.imageurl || $scope.imageurl.length === 0) {
                $scope.imageurl = 'https://s3.amazonaws.com/cdn.yappn.com/2.0/default-user-image-male.jpeg';
            }

            if ($scope.imagebox < 64) {
                $scope.bordersize = 4;
            }
            else if ($scope.imagebox >= 120) {
                $scope.bordersize = 8;
            }
            else {
                $scope.bordersize = 4;
            }

        }],
        template: '<img class="media-object dp img-circle" ng-cloak src="https://s3.amazonaws.com/cdn.yappn.com/2.0/blank.png" height="{{imagebox}}" width="{{imagebox}}" style="background-color:#fff; border-width: {{bordersize}}px; border-color: rgba(55,174,37,1); width: {{imagebox}}px;height:{{imagebox}}px; background-image: url(\'{{imageurl}}\'); background-repeat: no-repeat; background-position: center; background-size:cover; -webkit-background-size:cover">'

    };

    return directive;

}]);