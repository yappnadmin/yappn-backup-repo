'use strict';

angular.module('app').directive('infiniteDiv', [function() {
    return {
        link: function (scope, element, attrs) {
            var offset = parseInt(attrs.threshold, 0) || 0;

            var e = element[0];
            if (attrs.container) {
                //console.log('using ' + attrs.container);
                e = document.getElementById(attrs.container);
            }

            angular.element(e).bind('scroll', function () {

                var equation = scope.$eval(attrs.canLoad) && e.scrollTop + e.offsetHeight >= e.scrollHeight - offset;
                //console.log("canLoad: " + scope.$eval(attrs.canLoad) + " and " + (e.scrollTop + e.offsetHeight).toString() + " >= " + (e.scrollHeight - offset).toString() + " | equation: " + equation);

                if (equation) {
                    scope.$apply(attrs.infiniteDiv);
                }
            });
        }
    };
}]);