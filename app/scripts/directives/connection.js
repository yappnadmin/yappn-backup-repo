'use strict';

angular.module('app').directive('connection', [function() {

    return {

        restrict: 'E',
        transclude: false,
        replace: true,

        scope: {
            username: '='
        },

        controller: ['$rootScope', '$scope','$timeout','$yappn', '$log', '$translate', 'Auth', function($rootScope, $scope, $timeout, $yappn, $log, $translate, Auth) {
            
            $scope.followable   = true;
            $scope.btn          = 'btn-default';
            $scope.label        = $translate('connection.btn.follow');
            $scope.icon         = '';
            $scope.connection   = null;

            if ( !Auth.isLoggedIn() || $scope.username === $rootScope.currentUser.username ) {
                $scope.followable = false;
            }

            var checkConnection = function() {
                if ($scope.followable) {
                    $yappn.api('/api/user/'+$scope.username+'/connection', 'GET', {}).success(function(response) {
                        var connection = response.data.connection;
                        $scope.connection = connection;

                        switch(connection) {
                            case 'following':
                                $scope.label    = $translate('connection.btn.following');
                                $scope.btn      = 'btn-warning';
                                $scope.icon     = 'fa-check-square';
                                break;
                            case 'mutual':
                                $scope.label    = $translate('connection.btn.mutual');
                                $scope.btn      = 'btn-success';
                                $scope.icon     = 'fa-check-square';
                                break;
                            default:
                                $scope.label    = $translate('connection.btn.follow');
                                $scope.btn      = 'btn-default';
                                $scope.icon     = 'fa-circle-o';
                                break;
                        }
                    });
                }
            };

            checkConnection();
            

            // use this to connect
            $scope.connect = function () {

                var action = 'follow';
                if ($scope.connection === 'mutual' || $scope.connection === 'following') {
                    action = 'unfollow';
                }

                $yappn.api('/api/user/'+$scope.username+'/'+action, 'POST', {}).success(function() {
                    // if they are following, you will get a 201 response, if unfollow you get 200
                    checkConnection();
                });

            };

        }],

        template: '<button class="btn btn-sm {{ btn }}" ng-click="connect()" ng-disabled="!followable" style="text-transform:uppercase;">{{ label }}</button>'

    };

}]);