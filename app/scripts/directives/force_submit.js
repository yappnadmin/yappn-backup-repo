'use strict';

angular.module('app').directive('forceSubmitOnEnter', [function () {
    return function (scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
            if(event.which === 13) {
                if (element.context.value.length > 3) {
                    scope.$apply(function (){
                        scope.$eval(attrs.forceSubmitOnEnter);
                    });
                }
                event.preventDefault();
            }
        });
    };
}]);