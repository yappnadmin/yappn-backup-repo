'use strict';

module.exports = function(grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        env: {
            options: {},
            test: {
                NODE_ENV: 'test',
                NODE_MONGOOSE_MIGRATIONS_CONFIG: process.env.PWD + '/lib/config/migrations.js'
            },
            development: {
                NODE_ENV: 'development',
                NODE_MONGOOSE_MIGRATIONS_CONFIG: process.env.PWD + '/lib/config/migrations.js',
                AUTH_FACEBOOK: '683619055030979:eda038151547056009f7b8573c21f3a3',
                AUTH_TWITTER: '8J2fyCJRqJWxUBLvSzSeNyvAZ:Zg5ciKVI0SfEkqURY8dn7xHS5aaLOO9lZ6duzlmklIxpPAJzlC'
            },
            staging: {
                NODE_ENV: 'staging',
                NODE_MONGOOSE_MIGRATIONS_CONFIG: process.env.PWD + '/lib/config/migrations.js'
            },
            production: {
                NODE_ENV: 'production',
                NODE_MONGOOSE_MIGRATIONS_CONFIG: process.env.PWD + '/lib/config/migrations.js'
            }
        },

        // Project settings
        yeoman: {
            // configurable paths
            app: require('./bower.json').appPath || 'app',
            dist: 'dist'
        },

        express: {
            options: {
                port: process.env.PORT || 9000
            },
            dev: {
                options: {
                    script: 'server.js',
                    debug: true
                }
            },
            prod: {
                options: {
                    script: 'dist/server.js',
                    debug: true
                }
            }
        },

        open: {
            server: {
                url: 'http://localhost:<%= express.options.port %>'
            }
        },

        watch: {
            js: {
                files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
                tasks: ['newer:jshint:all'],
                options: {
                    livereload: true
                }
            },
            jsTest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['newer:jshint:testMocha', 'newer:jshint:testJasmine']
            },
            less: {
                files: ['<%= yeoman.app %>/styles/{,*/}*.less'],
                tasks: ['less:dist']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                files: [
                    '<%= yeoman.app %>/views/{,*//*}*.{html,jade,ejs}',
                    '{.tmp,<%= yeoman.app %>}/styles/{,*//*}*.css',
                    '{.tmp,<%= yeoman.app %>}/scripts/{,*//*}*.js',
                    '<%= yeoman.app %>/images/{,*//*}*.{png,jpg,jpeg,gif,webp,svg}',
                ],
                options: {
                    livereload: true
                }
            },
            express: {
                files: [
                    'server.js',
                    'lib/**/*.{js,json}'
                ],
                tasks: ['newer:jshint:server', 'express:dev', 'wait'],
                options: {
                    livereload: true,
                    nospawn: true //Without this option specified express won't be reloaded
                }
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            server: {
                options: {
                    jshintrc: 'lib/.jshintrc'
                },
                src: ['lib/{,*/}*.js']
            },
            all: [
                '<%= yeoman.app %>/scripts/{,*/}*.js', '!<%= yeoman.app %>/scripts/vendors/{,*/}*.js'
            ],
            testMocha: {
                options: {
                    jshintrc: 'test/server/.jshintrc'
                },
                src: ['test/server/**/*.js']
            },
            testJasmine: {
                options: {
                    jshintrc: 'test/.jshintrc'
                },
                src: ['test/spec/**/*.js']
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/*',
                        '!<%= yeoman.dist %>/Procfile',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            server: ['.tmp']
        },

        less: {
            dist: {
                options: {
                    dumpLineNumbers: 'comments' // TODO: source-maps https://github.com/gruntjs/grunt-contrib-less/issues/60
                },
                files: {
                    '.tmp/styles/main.css': '<%= yeoman.app %>/styles/main.less',
                    '.tmp/styles/vendor.css': '<%= yeoman.app %>/styles/vendor.less',
                }
            }
        },

        // Automatically inject Bower components into the app
        'wiredep': {
            app: {
                src: ['<%= yeoman.app %>/views/index.html','<%= yeoman.app %>/views/integrated.html'],
                ignorePath: '../',
                exclude: [
                    'bootstrap',
                    'bootswatch'
                ]
            }
        },

        // Capture the git commit for reporting
        'git-describe': {
            me: {}
        },

        // Renames files for browser caching purposes
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= yeoman.dist %>/public/scripts/{,*/}*.js',
                        '<%= yeoman.dist %>/public/styles/{,*/}*.css',
                        '<%= yeoman.dist %>/public/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                        '<%= yeoman.dist %>/public/styles/fonts/*'
                    ]
                }
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: [
                '<%= yeoman.app %>/views/index.html',
                '<%= yeoman.app %>/views/integrated.html',
            ],
            options: {
                dest: '<%= yeoman.dist %>/public'
            }
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            html: [
                '<%= yeoman.dist %>/views/{,*/}*.html',
                '<%= yeoman.dist %>/views/{,*/}*.ejs',
                '<%= yeoman.dist %>/views/{,*/}*.jade'
            ],
            css: ['<%= yeoman.dist %>/public/styles/{,*/}*.css'],
            options: {
                assetsDirs: ['<%= yeoman.dist %>/public']
            }
        },

        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%= yeoman.dist %>/public/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>/public/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    //collapseWhitespace: true,
                    //collapseBooleanAttributes: true,
                    //removeCommentsFromCDATA: true,
                    //removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/views',
                    src: ['*.{html,ejs}', 'partials/*.html'],
                    dest: '<%= yeoman.dist %>/views'
                }]
            }
        },

        // Allow the use of non-minsafe AngularJS files. Automatically makes it
        // minsafe compatible so Uglify does not destroy the ng references
        ngmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: '*.js',
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        uglify: {
            options: {
                mangle: false
            }
        },

        // Replace Google CDN references
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/views/*.html']
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>/public',
                    src: [
                        'version.json',
                        '*.{ico,png,txt}',
                        '.htaccess',
                        'bower_components/**/*',
                        '!bower_components/bootstrap/**/*',
                        '!bower_components/bootswatch/**/*',
                        'images/{,*/}*.{webp}',
                        'fonts/**/*'
                    ]
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>/locales',
                    dest: '<%= yeoman.dist %>/public/locales',
                    src: '**/*.json'
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>/views',
                    dest: '<%= yeoman.dist %>/views',
                    src: '**/*.jade'
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>/views/partials',
                    dest: '<%= yeoman.dist %>/views/partials',
                    src: '**/*.html'
                }, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/public/images',
                    src: ['generated/*']
                }, {
                    expand: true,
                    dest: '<%= yeoman.dist %>',
                    src: [
                        'package.json',
                        'server.js',
                        'lib/**/*'
                    ]
                }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'less:dist' // 'copy:styles'
            ],
            test: [
                'less:dist' // 'copy:styles'
            ],
            dist: [
                'less:dist', // 'copy:styles'
                'imagemin',
                'svgmin',
                'htmlmin'
            ]
        },

        // Test settings
        karma: {
            options: {
                configFile: 'karma.conf.js',
                singleRun: true
            },
            unit: {
                browsers: ['Chrome']
            }
        },

        mochaTest: {
            unit: {
                options: {
                    reporter: 'spec'
                },
                src: ['test/server/spec/**/*.js']
            },
            integration: {
                options: {
                    reporter: 'spec',
                    require: './server'
                },
                src: ['test/server/integration/*.js']
            }
        },

        'modulus-deploy': {
            staging: {
                options: {
                    project: 'yappn-staging-server ./dist'
                }
            },
            production: {
                options: {
                    project: 'yappn-rescue-server ./dist'
                }
            }
        },

        'merge-json': {
            'en': {
                src: ['locales/*.json'],
                dest: '<%= yeoman.app %>/locales/en.json'
            }
        },

        ortsbo_process: {
            options: {
                srcFolder: '<%= yeoman.app %>/locales',
                distFolder: '<%= yeoman.app %>/locales',
                locales: ['en', 'af', 'sq', 'ar', 'hy', 'az', 'eu', 'be', 'bn', 'bg', 'ca', 'zhs', 'zht', 'hr', 'cs', 'da', 'nl', 'eo', 'et', 'tl', 'fi', 'fr', 'gl', 'ka', 'de', 'el', 'gu', 'ht', 'he', 'hi', 'mww', 'hu', 'is', 'id', 'ga', 'it', 'ja', 'kn', 'ko', 'lo', 'la', 'lv', 'lt', 'mk', 'ms', 'mt', 'no', 'fa', 'pl', 'pt', 'ro', 'ru', 'sr', 'sk', 'sl', 'es', 'sw', 'sv', 'ta', 'te', 'th', 'tr', 'uk', 'ur', 'vi', 'cy', 'yi'],
                key: 'A3E1-5C7A-8EBB-AB3F-0170-E67A-FA00-3D4D',
                regenerate: false
            }
        },

        // Configuration to be run (and then tested).
        migrate: {
            up: {},
            down: {},
            create: {},
            options: {
                directory: './migrations',
                binaryPath: process.env.PWD + '/node_modules/.bin/mongoose-migrate'
            },
            target: '---'
        },

        prompt: {
            locales: {
                options: {
                    questions: [{
                        config: 'ortsbo_process.options.regenerate',
                        type: 'confirm',
                        message: 'Do you need to re-generate the language files?',
                        default: 'No'
                    }]
                }
            }
        }
    });

    // Used for delaying livereload until after server has restarted
    grunt.registerTask('wait', function() {
        grunt.log.ok('Waiting for server reload...');
        var done = this.async();

        setTimeout(function() {
            grunt.log.writeln('Done waiting!');
            done();
        }, 500);
    });

    grunt.registerTask('version', 'Tag the current build revision', function() {
        grunt.event.once('git-describe', function(rev) {
            grunt.file.write(grunt.config('yeoman.app') + '/version.json', JSON.stringify({
                revision: rev[0],
                date: grunt.template.today('isoDateTime')
            }));
        });
        grunt.task.run('git-describe');
    });

    grunt.registerTask('express-keepalive', 'Keep grunt running', function() {
        this.async();
    });

    grunt.registerTask('serve', function(target) {
        if (target === 'dist') {
            return grunt.task.run(['express:prod', 'open', 'express-keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'version',
            'concurrent:server',
            'express:dev',
            'watch'
        ]);
    });

    grunt.registerTask('server', function() {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve']);
    });

    grunt.registerTask('testServer', function(target) {
        target = target || 'unit';
        grunt.task.run([
            'clean:server',
            'version',
            'concurrent:test',
            'env:test',
            'mochaTest:' + target
        ]);
    });

    grunt.registerTask('testClient', function(target) {
        target = target || 'unit';
        grunt.task.run([
            'clean:server',
            'version',
            'concurrent:test',
            'env:test',
            'karma:' + target
        ]);
    });

    grunt.registerTask('deploy', function(target) {
        target = target || 'staging';
        grunt.task.run([
            'modulus-deploy:' + target
        ]);
    });



    grunt.registerTask('locales', function() {
        grunt.task.run(['merge-json', 'ortsbo_process']);
    });

    grunt.registerTask('dev', function() {
        grunt.task.run([
            'env:development',
            'migrate:up',
            'merge-json',
            'serve'
        ]);
    });

    grunt.registerTask('prod', function() {
        grunt.task.run([
            'env:staging',
            'prompt:locales',
            'locales',
            'newer:jshint',
            'clean:dist',
            'version',
            'wiredep',
            'useminPrepare',
            'concurrent:dist',
            'concat',
            'ngmin',
            'copy:dist',
            'cdnify',
            'cssmin',
            'uglify',
            'rev',
            'usemin',
            'migrate:up',
            'clean:server',
            'version',
            'concurrent:server',
            'express:prod',
            'watch'
        ]);
    });

    grunt.registerTask('test', function() {
        grunt.task.run([
            'env:test',
            'merge-json',
            'testServer:unit',
            'testServer:integration',
            //'testClient'
        ]);
    });

    grunt.registerTask('stage', function() {
        grunt.task.run([
            'env:staging',
            'prompt:locales',
            'locales',
            'newer:jshint',
            'clean:dist',
            'version',
            'wiredep',
            'useminPrepare',
            'concurrent:dist',
            'concat',
            'ngmin',
            'copy:dist',
            'cdnify',
            'cssmin',
            'uglify',
            'rev',
            'usemin',
            'migrate:up',
            'deploy:staging'
        ]);
    });

    grunt.registerTask('live', function() {
        grunt.task.run([
            'env:production',
            'prompt:locales',
            'locales',
            'newer:jshint',
            'clean:dist',
            'version',
            'wiredep',
            'useminPrepare',
            'concurrent:dist',
            'concat',
            'ngmin',
            'copy:dist',
            'cdnify',
            'cssmin',
            'uglify',
            'rev',
            'usemin',
            'migrate:up',
            'deploy:production'
        ]);
    });

    grunt.registerTask('default', function() {
        grunt.log.warn('The `default` task has been deprecated. Use `grunt dev|test|stage|live` to start a server.');
    });
};