'use strict';

var express = require('express'),
    http = require('http'),
    pkg = require('./package.json');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// load the config
var config = require('./lib/config/config');

// Bootstrap Models
require('./lib/config/models');

// Populate empty DB with sample data
//console.log('process.env.NODE_ENV === ' + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    require('./lib/config/testdata');
}

// Passport Config
require('./lib/config/passport')();

// OAuth2 Server Config
require('./lib/config/oauth2');

var app = express();

// Express Settings
require('./lib/config/express')(app);

// Routing
require('./lib/routes')(app);


// Start server
var server = http.createServer(app),
    realtime = require('./lib/helpers/realtime').realtime;

realtime.server.attach(server);
server.listen(config.port, function () {
    if (process.env.NODE_ENV !== 'test') {
        console.log(pkg.name + ' | Ver: ' + pkg.version + ' | Port: '+config.port+' | Mongo: '+config.mongo.uri+' | Redis: '+config.redis.uri + ' | Env: ' + process.env.NODE_ENV);
        //console.log('ENV', process.env);
    }
});
// Expose server for testing
app.server = server;

// Expose app
exports = module.exports = app;